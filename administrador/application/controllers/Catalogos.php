<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Catalogos extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('hoteles_model');
        $this->load->library('session');
	}
        
	public function index(){
            $log = $this->session->userdata('logued_in');
            if($log){
                /*$data["hoteles_result"] = $this->hoteles_model->get_hoteles();
                $data["regiones"] = $this->hoteles_model->get_regiones();
                $data["destinos"] = $this->hoteles_model->get_destinos();*/

                $data_header["seccion_catalogos"] = 1;

                $this->load->view('view_header',$data_header);
                //$this->load->view('view_hoteles',$data);
                $this->load->view('view_footer');

               
            }else{
                 redirect('home/index'); 
            }

            
	}

}
