<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contacto extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('contacto_model');
        $this->load->library('session');
        require_once APPPATH.'third_party/PHPMailer-master/PHPMailerAutoload.php';
	}
        
	public function index(){
            $log = $this->session->userdata('logued_in');
            if($log){
                $data["contacto_result"] = $this->contacto_model->get_contactos();
                $data["status_solicitud"] = $this->contacto_model->get_status_contacto();
                $data_header["seccion_contacto"] = 1;
                $this->load->view('view_header',$data_header);
                $this->load->view('view_contacto',$data);
                $this->load->view('view_footer');
               
            }else{
                 redirect('home/index'); 
            }

            
	}


    public function recuperarContacto(){
        $user = $this->contacto_model->get_contacto($this->input->post('id_contacto'));
        echo json_encode($user);
    }


    public function actualizarContacto(){
        $data = $this->input->post();
        $datos = array(
            'fecha_modificacion' => date('Y-m-d H:i:s'),
            'nombre_contacto' => $data['nombre_contacto_edit'],
            'nacimiento_contacto' => $data['nacimiento_contacto_edit'],
            'telefono_contacto' => $data['telefono_contacto_edit'],
            'correo_contacto' => $data['correo_contacto_edit'],
            'genero_contacto' => $data['genero_contacto_edit'],
            'comentarios_contacto' => $data['comentarios_contacto_edit'],
            'respuesta_contacto' => $data['respuesta_contacto_edit'],
            'id_status_contacto' => $data['id_status_contacto_edit']
        );

        echo '<div id="div_resultado">'.$this->contacto_model->actualiza_contacto( $data['id_contacto_edit'],$datos ).'</div>';
    }

    public function enviarMailContacto(){

       $contacto = $this->contacto_model->get_contacto($this->input->post('id_contacto'));

        $data = $this->input->post();
        $datos = array(
            'fecha_modificacion' => date('Y-m-d H:i:s'),
            'correo_contacto' => $data['correo_contacto'],
            'respuesta_contacto' => $data['respuesta_contacto']
        );
        $this->contacto_model->actualiza_contacto( $data['id_contacto'],$datos );



        $correo = $data['correo_contacto'];
        $mensaje = $data['respuesta_contacto'];

        if( $correo!="" ){

            $mail_p = new PHPMailer();
            $mail_p->IsSMTP();
            $mail_p->SMTPAuth   = true;
            $mail_p->SMTPSecure = "tls"; 
            $mail_p->Host       = "smtp.mandrillapp.com";
            $mail_p->Port       = 587; 
            $mail_p->Username   = "aquintana@estrategica.com";
            $mail_p->Password   = "9C6kelU75JH5kwhS_BUFFQ";
            $mail_p->From       = "contacto@crmpeugeot.com.mx";
            $mail_p->FromName   = "Joyas de Mexico.";
            $mail_p->Subject    = "Contacto";
            $mail_p->AltBody    = "Contacto";
            $mail_p->AddReplyTo("contacto@crmpeugeot.com.mx","Joyas de Mexico");
            $mail_p->WordWrap   = 50;

            $mail_p->AddAddress($correo, '');
            $mail_p->IsHTML(true);
            $mail_p->CharSet = 'UTF-8';
            $mail_p->Body = $mensaje;
           
            if($mail_p->Send()) {
                echo "1";
            }else{
                echo "2";
                //echo $mail->ErrorInfo;
            }

        }else{
            echo "0";
        }


    }


}
