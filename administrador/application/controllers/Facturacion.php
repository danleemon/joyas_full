<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Facturacion extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('facturacion_model');
        $this->load->library('session');
        require_once APPPATH.'third_party/PHPMailer-master/PHPMailerAutoload.php';
        require_once APPPATH.'third_party/phpexcel/Classes/PHPExcel.php';
	}
        
	public function index(){
            $log = $this->session->userdata('logued_in');
            if($log){
                $data["facturacion_result"] = $this->facturacion_model->get_facturaciones();
                $data["status_solicitud"] = $this->facturacion_model->get_status_solicitudes();
                $data_header["seccion_facturacion"] = 1;
                $this->load->view('view_header',$data_header);
                $this->load->view('view_facturacion',$data);
                $this->load->view('view_footer');
               
            }else{
                 redirect('home/index'); 
            }

            
	}


    public function recuperarFacturacion(){
        $user = $this->facturacion_model->get_facturacion($this->input->post('id_facturacion'));
        echo json_encode($user);
    }


    public function actualizarFacturacion(){
        $data = $this->input->post();
        $datos = array(
            'fecha_modificacion' => date('Y-m-d H:i:s'),
            'clave_reservacion' => $data['clave_reservacion_edit'],
            'nombre_factura' => $data['nombre_factura_edit'],
            'rfc_factura' => $data['rfc_factura_edit'],
            'direccion_factura' => $data['direccion_factura_edit'],
            'correo_factura' => $data['correo_factura_edit'],
            'id_status_factura' => $data['id_status_factura_edit']
        );

        
        $config['upload_path'] = 'assets/imagenes/facturacion';
        $config['allowed_types'] = 'xml|png|jpeg';
        $this->load->library('upload');
        $this->upload->initialize($config);
        $data_upload = array();
        if( !$this->upload->do_upload("input_factura_xml_edit") ){
            $data_upload[0] = $this->upload->display_errors();
        }else{
            $img_data = $this->upload->data();
            $datos['factura_xml'] = $img_data["file_name"];
        }

        $config['upload_path'] = 'assets/imagenes/facturacion';
        $config['allowed_types'] = 'pdf|jpg|png|jpeg';
        $this->load->library('upload');
        $this->upload->initialize($config);
        $data_upload = array();
        if( !$this->upload->do_upload("input_factura_pdf_edit") ){
            $data_upload[0] = $this->upload->display_errors();
        }else{
            $img_data = $this->upload->data();
            $datos['factura_pdf'] = $img_data["file_name"];
        }


        echo '<div id="div_resultado">'.$this->facturacion_model->actualiza_facturacion( $data['id_facturacion_edit'],$datos ).'</div>';
    }

    public function enviarMailFactura(){

       $factura = $this->facturacion_model->get_facturacion($this->input->post('id_facturacion'));

        $correo = $factura['correo_factura'];
        $archivo_pdf = $factura['factura_pdf'];
        $archivo_xml = $factura['factura_xml'];

        if( $archivo_pdf!="" && $archivo_xml!="" ){

            $mail_p = new PHPMailer();
            $mail_p->IsSMTP();
            $mail_p->SMTPAuth   = true;
            $mail_p->SMTPSecure = "tls"; 
            $mail_p->Host       = "smtp.mandrillapp.com";
            $mail_p->Port       = 587; 
            $mail_p->Username   = "aquintana@estrategica.com";
            $mail_p->Password   = "9C6kelU75JH5kwhS_BUFFQ";
            $mail_p->From       = "contacto@crmpeugeot.com.mx";
            $mail_p->FromName   = "Joyas de Mexico.";
            $mail_p->Subject    = "Facturacion";
            $mail_p->AltBody    = "Facturacion";
            $mail_p->AddReplyTo("contacto@crmpeugeot.com.mx","Joyas de Mexico");
            $mail_p->WordWrap   = 50;
            $mail_p->AddAddress($correo, '');
            $mail_p->AddAddress("dlimon@estrategica.com",'');
            $mail_p->AddAddress("daniel.limon@hotmail.com",'');
            $mail_p->IsHTML(true);
            $mail_p->CharSet = 'UTF-8';
            $mail_p->Body = "Esta es una prueba de email desde Joyas de mexico";
            $mail_p->addAttachment("assets/imagenes/facturacion/".$archivo_pdf);
            $mail_p->addAttachment("assets/imagenes/facturacion/".$archivo_xml);
            if($mail_p->Send()) {
                echo "1";
            }else{
                echo "2";
                //echo $mail->ErrorInfo;
            }

        }else{
            echo "0";
        }


    }





    public function descarga_csv( ){

            $data = $this->input->post();
            $valores = $this->facturacion_model->get_facturaciones();
            
            // Create new PHPExcel object
            $objPHPExcel = new PHPExcel();
            
            // Pestaña del webservice
            $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Arial');
            $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(11);
            $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('2283c5');
            $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getFont()->getColor()->setARGB('FFFFFF');
            $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            for($col = 'A'; $col !== 'F'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
            $objPHPExcel->getActiveSheet()->setAutoFilter("A1:E1");

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A1', 'Clave de reservacion')
                        ->setCellValue('B1', 'Nombre del huesped')
                        ->setCellValue('C1', 'RFC')
                        ->setCellValue('E1', 'Fecha de registro')
                        ->setCellValue('E1', 'Estatus');
            $c_ws = 1;
            foreach ($valores as $val) {
                $c_ws++;
                $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$c_ws, $val->clave_reservacion)
                            ->setCellValue('B'.$c_ws, $val->nombre_factura)
                            ->setCellValue('C'.$c_ws, $val->rfc_factura)
                            ->setCellValue('D'.$c_ws, $val->fecha_registro)
                            ->setCellValue('E'.$c_ws, $val->status_factura);
            } 
            
            $objPHPExcel->setActiveSheetIndex(0)->setTitle('Concentrado');
            $objPHPExcel->setActiveSheetIndex(0);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="Facturacion_'.date('Y_m_d_H_i_s').'.xls"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
            header ('Cache-Control: cache, must-revalidate');
            header ('Pragma: public');
            
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save('php://output');
            exit;
    }



}
