<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Habitaciones extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('habitaciones_model');
        $this->load->model('paquetes_model');
        $this->load->library('session');
	}
        
	public function index(){
            $log = $this->session->userdata('logued_in');
            if($log){
                $data["habitaciones_result"] = $this->habitaciones_model->get_habitaciones();
                $data["hoteles"] = $this->habitaciones_model->get_hoteles();
                $data["monedas"] = $this->paquetes_model->get_monedas();

                $data_header["seccion_habitaciones"] = 1;

                $this->load->view('view_header',$data_header);
                $this->load->view('view_habitaciones',$data);
                $this->load->view('view_footer');

               
            }else{
                 redirect('home/index'); 
            }
 
	}
	
    public function recuperarHabitacion(){
        $user = $this->habitaciones_model->get_habitacion($this->input->post('id_habitacion'));
        echo json_encode($user);
    }

    public function habilitarHabitacion(){
        $data = $this->input->post();
        $datos = array(
            'id_status_general' => $data['status']
        );
            
        echo $this->habitaciones_model->actualiza_habitacion( $data['id_habitacion'],$datos );
    }
    
    public function actualizarHabitacion(){
        $data = $this->input->post();
        $datos = array(
            'id_hotel' => $data['id_hotel_edit'],
            'nombre_habitacion' => $data['nombre_habitacion_edit'],
            'ocupacion_maxima' => $data['ocupacion_maxima_edit'],
            'descripcion' => $data['descripcion_edit'],
            'tarifa_base' => $data['tarifa_base_edit'],
            'id_moneda' => $data['id_moneda_edit']
        );


        echo '<div id="div_resultado">'.$this->habitaciones_model->actualiza_habitacion( $data['id_habitacion_edit'],$datos ).'</div>';
    }
    
    
    public function guardarHabitacion(){
        $data = $this->input->post();
        $datos = array(
            'id_hotel' => $data['id_hotel_add'],
            'nombre_habitacion' => $data['nombre_habitacion_add'],
            'ocupacion_maxima' => $data['ocupacion_maxima_add'],
            'descripcion' => $data['descripcion_add'],
            'id_status_general' => '1',
            'tarifa_base' => $data['tarifa_base_add'],
            'id_moneda' => $data['id_moneda_add']
        );

        echo '<div id="div_resultado">'.$this->habitaciones_model->guardar_habitacion( $datos ).'</div>';
    }
    
    
    public function eliminarHabitacion(){
		$data = $this->input->post();
		echo $this->habitaciones_model->eliminarHabitacion($data['id_habitacion']);
    }

}
