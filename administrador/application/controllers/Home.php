<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('home_model');
        $this->load->library('session');
	}
        
	public function index(){
            $log = $this->session->userdata('logued_in');
            if($log){
                redirect('home/welcome'); 
            }else{
                $this->load->view('view_login');
            }
	}
        
    public function log(){
        $user = $this->input->post('username');
        $pass = md5($this->input->post('password'));
		$usuario = $this->home_model->login($user,$pass);
		if($usuario){
        	$user_data = $array = array('nombre'=> $usuario['nombre_administrador'],'logued_in'=>TRUE, 'perfil'=>$usuario['perfil']); 
            $this->session->set_userdata($user_data);
            echo 1;
		}else{
            echo 0;
		}
	}

	public function welcome(){
		if($this->session->userdata('logued_in') ) {
            $this->load->view('view_header');
			$this->load->view('view_footer');
        }else {
            $this->load->view('view_login');

        }
	}
	public function logout(){
            $this->session->sess_destroy(); 
            redirect(); 
    }
}
