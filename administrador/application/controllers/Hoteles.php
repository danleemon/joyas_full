<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hoteles extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('hoteles_model');
        $this->load->library('session');
	}
        
	public function index(){
            $log = $this->session->userdata('logued_in');
            if($log){
                $data["hoteles_result"] = $this->hoteles_model->get_hoteles();
                $data["regiones"] = $this->hoteles_model->get_regiones();
                $data["destinos"] = $this->hoteles_model->get_destinos();

                $data_header["seccion_hoteles"] = 1;

                $this->load->view('view_header',$data_header);
                $this->load->view('view_hoteles',$data);
                $this->load->view('view_footer');

               
            }else{
                 redirect('home/index'); 
            }

            
	}

    public function recuperarHotel(){
        $user = $this->hoteles_model->get_hotel($this->input->post('id_hotel'));
        echo json_encode($user);
    }

    public function habilitarHotel(){
        $data = $this->input->post();
        $datos = array(
            'id_status_general' => $data['status']
        );
            
        echo $this->hoteles_model->actualiza_hotel( $data['id_hotel'],$datos );
    }

    public function habilitarPaquetes(){
        $data = $this->input->post();
        $datos = array(
            'visible_promociones' => $data['status']
        );
            
        echo $this->hoteles_model->actualiza_hotel( $data['id_hotel'],$datos );
    }

    public function actualizarHotel(){
        $data = $this->input->post();
        $datos = array(
            'id_region' => $data['region_edit'],
            'nombre_hotel' => $data['nombre_hotel_edit'],
            'url_slug' => $data['url_slug_edit'],
            'id_destino' => $data['destino_edit'],
            'ciudad' => $data['ciudad_edit'],
            'fecha_modificacion' => date('Y-m-d H:i:s'),
            'direccion_hotel' => $data['direccion_edit'],
            'leyenda' => $data['leyenda_edit'],
            'declaracion' => $data['declaracion_edit'],
            'codigo_postal' => $data['codigo_postal_edit'],
            'email' => '',
            'coordenadas_maps' => ''
        );
        
        $config['upload_path'] = 'assets/imagenes/hoteles';
        $config['allowed_types'] = 'jpg|png|jpeg|svg';

        $this->load->library('upload');
        $this->upload->initialize($config);

        $data_upload = array();

        //Slider Home
        if( !$this->upload->do_upload("input_slider_home_edit") ){
            $data_upload[0] = $this->upload->display_errors();
        }else{
            $img_data = $this->upload->data();
            $datos['imagen_slider_home'] = $img_data["file_name"];
        }

		//Home mini
        if( !$this->upload->do_upload("input_home_mini_edit") ){
            $data_upload[0] = $this->upload->display_errors();
        }else{
            $img_data = $this->upload->data();
            $datos['imagen_mini_home'] = $img_data["file_name"];
        }
		
		//Slider interno A
        if( !$this->upload->do_upload("input_slider_interno_a_edit") ){
            $data_upload[0] = $this->upload->display_errors();
        }else{
            $img_data = $this->upload->data();
            $datos['imagen_interior_a'] = $img_data["file_name"];
        }
		//Slider interno B
        if( !$this->upload->do_upload("input_slider_interno_b_edit") ){
            $data_upload[0] = $this->upload->display_errors();
        }else{
            $img_data = $this->upload->data();
            $datos['imagen_interior_b'] = $img_data["file_name"];
        }
        //Slider interno C
        if( !$this->upload->do_upload("input_slider_interno_c_edit") ){
            $data_upload[0] = $this->upload->display_errors();
        }else{
            $img_data = $this->upload->data();
            $datos['imagen_interior_c'] = $img_data["file_name"];
        }
        //Slider interno D
        if( !$this->upload->do_upload("input_slider_interno_d_edit") ){
            $data_upload[0] = $this->upload->display_errors();
        }else{
            $img_data = $this->upload->data();
            $datos['imagen_interior_d'] = $img_data["file_name"];
        }
        //Slider interno E
        if( !$this->upload->do_upload("input_slider_interno_e_edit") ){
            $data_upload[0] = $this->upload->display_errors();
        }else{
            $img_data = $this->upload->data();
            $datos['imagen_interior_e'] = $img_data["file_name"];
        }
        //Slider interno F
        if( !$this->upload->do_upload("input_slider_interno_f_edit") ){
            $data_upload[0] = $this->upload->display_errors();
        }else{
            $img_data = $this->upload->data();
            $datos['imagen_interior_f'] = $img_data["file_name"];
        }
        
        //Logo
        if( !$this->upload->do_upload("input_logo_edit") ){
            $data_upload[0] = $this->upload->display_errors();
        }else{
            $img_data = $this->upload->data();
            $datos['imagen_logo'] = $img_data["file_name"];
        }
        //Mapa confirmacion
        if( !$this->upload->do_upload("input_mapa_edit") ){
            $data_upload[0] = $this->upload->display_errors();
        }else{
            $img_data = $this->upload->data();
            $datos['imagen_mapa'] = $img_data["file_name"];
        }

        echo '<div id="div_resultado">'.$this->hoteles_model->actualiza_hotel( $data['id_hotel_edit'],$datos ).'</div>';
    }

    public function agregarHotel(){
        $data = $this->input->post();
        $datos = array(
            'id_region' => $data['region_add'],
            'nombre_hotel' => $data['nombre_hotel_add'],
            'url_slug' => $data['url_slug_add'],
            'id_destino' => $data['destino_add'],
            'ciudad' => $data['ciudad_add'],
            'fecha_modificacion' => date('Y-m-d H:i:s'),
            'direccion_hotel' => $data['direccion_add'],
            'leyenda' => $data['leyenda_add'],
            'declaracion' => $data['declaracion_add'],
            'codigo_postal' => $data['codigo_postal_add'],
            'email' => '',
            'coordenadas_maps' => '',
            'id_status_general' => '1',
            'visible_promociones' => '1'
        );
        
        $config['upload_path'] = 'assets/imagenes/hoteles';
        $config['allowed_types'] = 'jpg|png|jpeg';

        $this->load->library('upload');
        $this->upload->initialize($config);

        $data_upload = array();

        //Slider Home
        if( !$this->upload->do_upload("input_slider_home_add") ){
            $data_upload[0] = $this->upload->display_errors();
        }else{
            $img_data = $this->upload->data();
            $datos['imagen_slider_home'] = $img_data["file_name"];
        }

		//Home mini
        if( !$this->upload->do_upload("input_home_mini_add") ){
            $data_upload[0] = $this->upload->display_errors();
        }else{
            $img_data = $this->upload->data();
            $datos['imagen_mini_home'] = $img_data["file_name"];
        }
		
		//Slider interno A
        if( !$this->upload->do_upload("input_slider_interno_a_add") ){
            $data_upload[0] = $this->upload->display_errors();
        }else{
            $img_data = $this->upload->data();
            $datos['imagen_interior_a'] = $img_data["file_name"];
        }
		//Slider interno B
        if( !$this->upload->do_upload("input_slider_interno_b_add") ){
            $data_upload[0] = $this->upload->display_errors();
        }else{
            $img_data = $this->upload->data();
            $datos['imagen_interior_b'] = $img_data["file_name"];
        }
        //Slider interno C
        if( !$this->upload->do_upload("input_slider_interno_c_add") ){
            $data_upload[0] = $this->upload->display_errors();
        }else{
            $img_data = $this->upload->data();
            $datos['imagen_interior_c'] = $img_data["file_name"];
        }
        //Slider interno D
        if( !$this->upload->do_upload("input_slider_interno_d_add") ){
            $data_upload[0] = $this->upload->display_errors();
        }else{
            $img_data = $this->upload->data();
            $datos['imagen_interior_d'] = $img_data["file_name"];
        }
        //Slider interno E
        if( !$this->upload->do_upload("input_slider_interno_e_add") ){
            $data_upload[0] = $this->upload->display_errors();
        }else{
            $img_data = $this->upload->data();
            $datos['imagen_interior_e'] = $img_data["file_name"];
        }
        //Slider interno F
        if( !$this->upload->do_upload("input_slider_interno_f_add") ){
            $data_upload[0] = $this->upload->display_errors();
        }else{
            $img_data = $this->upload->data();
            $datos['imagen_interior_f'] = $img_data["file_name"];
        }

        //Logo
        if( !$this->upload->do_upload("input_logo_add") ){
            $data_upload[0] = $this->upload->display_errors();
        }else{
            $img_data = $this->upload->data();
            $datos['imagen_logo'] = $img_data["file_name"];
        }
        //Mapa confirmacion
        if( !$this->upload->do_upload("input_mapa_add") ){
            $data_upload[0] = $this->upload->display_errors();
        }else{
            $img_data = $this->upload->data();
            $datos['imagen_mapa'] = $img_data["file_name"];
        }



         
        echo '<div id="div_resultado">'.$this->hoteles_model->guardar_hotel( $datos ).'</div>';

    }

    public function eliminarHotel(){
                $data = $this->input->post();
                echo $this->hoteles_model->eliminarHotel($data['id_hotel']);
            }
        
   
}
