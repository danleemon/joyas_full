<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paquetes extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('paquetes_model');
        $this->load->model('habitaciones_model');
        $this->load->library('session');
	}
        
	public function index(){
            $log = $this->session->userdata('logued_in');
            if($log){
                $data["paquetes_result"] = $this->paquetes_model->get_paquetes();
                $data["hoteles"] = $this->paquetes_model->get_hoteles();
                $data["monedas"] = $this->paquetes_model->get_monedas();

                $data_header["seccion_paquetes"] = 1;

                $this->load->view('view_header',$data_header);
                $this->load->view('view_paquetes',$data);
                $this->load->view('view_footer');

               
            }else{
                 redirect('home/index'); 
            }
 
	}
	
    public function recuperarPaquete(){
        $user = $this->paquetes_model->get_paquete($this->input->post('id_paquete'));
        echo json_encode($user);
    }

    public function recuperarHabitaciones(){
         $habitaciones = $this->habitaciones_model->get_habitacion_hotel($this->input->post('id_hotel'));
         foreach($habitaciones as $habitacion){
            echo '<option value="'.$habitacion->id_habitacion.'">'.$habitacion->nombre_habitacion.'</option>';
         }

    }


    public function habilitarPaquete(){
        $data = $this->input->post();
        $datos = array(
            'id_status_general' => $data['status']
        );
            
        echo $this->paquetes_model->actualiza_paquete( $data['id_paquete'],$datos );
    }
    
    public function actualizarPaquete(){
        $data = $this->input->post();
        $datos = array(
            'id_hotel' => $data['id_hotel_edit'],
            'id_habitacion' => $data['id_habitacion_edit'],
            'id_disponibilidad' => $data['id_disponibilidad_edit'],
            'destino' => $data['destino_edit'],
            'nombre_paquete' => $data['nombre_paquete_edit'],
            'id_moneda' => $data['id_moneda_edit'],
            'cargo_moneda' => $data['cargo_moneda_edit'],
            'precio' => $data['precio_edit'],
            'fecha_modificacion' => date('Y-m-d H:i:s'),
            'iva' => $data['iva_edit'],
            'ish' => $data['ish_edit'],
            'cargo_servicio' => $data['cargo_servicio_edit'],
            'tipo_cargo' => $data['tipo_cargo_edit'],
            'incluye_impuestos' => $data['incluye_impuestos_edit'],
            'vigencia_inicio' => $data['vigencia_inicio_edit'],
            'vigencia_fin' => $data['vigencia_fin_edit'],
            'descripcion' => $data['descripcion_edit'],
            'numero_habitaciones' => $data['numero_habitaciones_edit'],
            //'contempla_menores' => $data['contempla_menores_edit'],

            'dias' => $data['dias_edit'],
            'personas' => $data['personas_edit'],
            'menores' => $data['menores_edit'],

            'tipo_tarifa' => $data['tipo_tarifa_edit'],
            'ocupacion' => $data['ocupacion_edit'],
            'orden' => $data['orden_edit']
        );

        
        $config['upload_path'] = 'assets/imagenes/paquetes';
        $config['allowed_types'] = 'jpg|png|jpeg';

        $this->load->library('upload');
        $this->upload->initialize($config);

        $data_upload = array();
		//Home mini
        if( !$this->upload->do_upload("input_imagen_edit") ){
            $data_upload[0] = $this->upload->display_errors();
        }else{
            $img_data = $this->upload->data();
            $datos['imagen_detalle'] = $img_data["file_name"];
        }

        echo '<div id="div_resultado">'.$this->paquetes_model->actualiza_paquete( $data['id_paquete_edit'],$datos ).'</div>';
    }
    
    
    public function guardarPaquete(){
        $data = $this->input->post();
        $datos = array(
            'nombre_paquete' => $data['nombre_paquete_add'],
            'id_hotel' => $data['id_hotel_add'],
            'id_habitacion' => $data['id_habitacion_add'],
            'id_disponibilidad' => $data['id_disponibilidad_add'],
            'destino' => $data['destino_add'],
            'id_moneda' => $data['id_moneda_add'],
            'cargo_moneda' => $data['cargo_moneda_add'],
            'precio' => $data['precio_add'],
            'iva' => $data['iva_add'],
            'ish' => $data['ish_add'],
            'cargo_servicio' => $data['cargo_servicio_add'],
            'tipo_cargo' => $data['tipo_cargo_add'],
            'incluye_impuestos' => $data['incluye_impuestos_add'],
            'vigencia_inicio' => $data['vigencia_inicio_add'],
            'vigencia_fin' => $data['vigencia_fin_add'],
            'id_status_general' => '1',
            'descripcion' => $data['descripcion_add'],
            'numero_habitaciones' => $data['numero_habitaciones_add'],
            //'contempla_menores' => $data['contempla_menores_add'],
            'dias' => $data['dias_add'],
            'personas' => $data['personas_add'],
            'menores' => $data['menores_add'],
            
            'tipo_tarifa' => $data['tipo_tarifa_add'],
            'ocupacion' => $data['ocupacion_add'],
            'orden' => $data['orden_add']
        );

        $config['upload_path'] = 'assets/imagenes/paquetes';
        $config['allowed_types'] = 'jpg|png|jpeg';

        $this->load->library('upload');
        $this->upload->initialize($config);

        $data_upload = array();
		//Home mini
        if( !$this->upload->do_upload("input_imagen_add") ){
            $data_upload[0] = $this->upload->display_errors();
        }else{
            $img_data = $this->upload->data();
            $datos['imagen_detalle'] = $img_data["file_name"];
        }
		
		
        
        echo '<div id="div_resultado">'.$this->paquetes_model->guardar_paquete( $datos ).'</div>';
    }
    
    
    public function eliminarPaquete(){
		$data = $this->input->post();
		echo $this->paquetes_model->eliminarPaquete($data['id_paquete']);
    }

}
