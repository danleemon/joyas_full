<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promo_exp_manager extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('paquetes_model');
        $this->load->model('promociones_model');
        $this->load->library('session');
	}
        
	public function especiales(){
            $log = $this->session->userdata('logued_in');
            if($log){
                $data["promociones_result"] = $this->promociones_model->get_promos_especiales();
                $data["paquetes"] = $this->promociones_model->get_paquetes();

                $data_header["seccion_especiales"] = 1;

                $this->load->view('view_header',$data_header);
                $this->load->view('view_promos_especiales',$data);
                $this->load->view('view_footer');

               
            }else{
                 redirect('home/index'); 
            }
 
	}


    public function temporada(){
            $log = $this->session->userdata('logued_in');
            if($log){
                $data["paquetes_result"] = $this->paquetes_model->get_paquetes();
                $data["hoteles"] = $this->paquetes_model->get_hoteles();
                $data["monedas"] = $this->paquetes_model->get_monedas();

                $data_header["seccion_paquetes"] = 1;

                $this->load->view('view_header',$data_header);
                $this->load->view('view_promos_temporada',$data);
                $this->load->view('view_footer');

               
            }else{
                 redirect('home/index'); 
            }
 
    }


	
    public function recuperarPromocion(){
        $user = $this->promociones_model->get_promocion($this->input->post('id_promo_especial'));
        echo json_encode($user);
    }

    public function recuperarHabitaciones(){
         $habitaciones = $this->habitaciones_model->get_habitacion_hotel($this->input->post('id_hotel'));
         foreach($habitaciones as $habitacion){
            echo '<option value="'.$habitacion->id_habitacion.'">'.$habitacion->nombre_habitacion.'</option>';
         }

    }

    public function habilitarPromocionEspecial(){
        $data = $this->input->post();
        $datos = array(
            'publicado' => $data['status']
        );
        echo $this->promociones_model->actualiza_promocion_especial( $data['id_promo_especial'],$datos );
    }

    
    public function actualizarPaquete(){
        $data = $this->input->post();
        $datos = array(
            'id_hotel' => $data['id_hotel_edit'],
            'id_habitacion' => $data['id_habitacion_edit'],
            'id_disponibilidad' => $data['id_disponibilidad_edit'],
            'destino' => $data['destino_edit'],
            'nombre_paquete' => $data['nombre_paquete_edit'],
            'id_moneda' => $data['id_moneda_edit'],
            'precio' => $data['precio_edit'],
            'fecha_modificacion' => date('Y-m-d H:i:s'),
            'iva' => $data['iva_edit'],
            'ish' => $data['ish_edit'],
            'cargo_servicio' => $data['cargo_servicio_edit'],

            'tipo_cargo' => $data['tipo_cargo_edit'],
            'incluye_impuestos' => $data['incluye_impuestos_edit'],

            'vigencia_inicio' => $data['vigencia_inicio_edit'],
            'vigencia_fin' => $data['vigencia_fin_edit'],
            'descripcion' => $data['descripcion_edit']
        );

        
        $config['upload_path'] = 'assets/imagenes/paquetes';
        $config['allowed_types'] = 'jpg|png|jpeg';

        $this->load->library('upload');
        $this->upload->initialize($config);

        $data_upload = array();
		//Home mini
        if( !$this->upload->do_upload("input_imagen_edit") ){
            $data_upload[0] = $this->upload->display_errors();
        }else{
            $img_data = $this->upload->data();
            $datos['imagen_detalle'] = $img_data["file_name"];
        }

        echo '<div id="div_resultado">'.$this->paquetes_model->actualiza_paquete( $data['id_paquete_edit'],$datos ).'</div>';
    }
    
    
    public function guardarPaquete(){
        $data = $this->input->post();
        $datos = array(
            'nombre_paquete' => $data['nombre_paquete_add'],
            'id_hotel' => $data['id_hotel_add'],
            'id_habitacion' => $data['id_habitacion_add'],
            'id_disponibilidad' => $data['id_disponibilidad_add'],
            'destino' => $data['destino_add'],
            'id_moneda' => $data['id_moneda_add'],
            'precio' => $data['precio_add'],
            'iva' => $data['iva_add'],
            'ish' => $data['ish_add'],
            'cargo_servicio' => $data['cargo_servicio_add'],
            'tipo_cargo' => $data['tipo_cargo_add'],
            'incluye_impuestos' => $data['incluye_impuestos_add'],
            'vigencia_inicio' => $data['vigencia_inicio_add'],
            'vigencia_fin' => $data['vigencia_fin_add'],
            'id_status_general' => '1',
            'descripcion' => $data['descripcion_add']
        );

        $config['upload_path'] = 'assets/imagenes/paquetes';
        $config['allowed_types'] = 'jpg|png|jpeg';

        $this->load->library('upload');
        $this->upload->initialize($config);

        $data_upload = array();
		//Home mini
        if( !$this->upload->do_upload("input_imagen_add") ){
            $data_upload[0] = $this->upload->display_errors();
        }else{
            $img_data = $this->upload->data();
            $datos['imagen_detalle'] = $img_data["file_name"];
        }
		
		
        
        echo '<div id="div_resultado">'.$this->paquetes_model->guardar_paquete( $datos ).'</div>';
    }
    
    
    public function eliminarPaquete(){
		$data = $this->input->post();
		echo $this->paquetes_model->eliminarPaquete($data['id_paquete']);
    }

}
