<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promo_hoteles extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('paquetes_model');
        $this->load->model('promociones_hoteles_model');
        $this->load->library('session');
	}
        
	public function index(){
            $log = $this->session->userdata('logued_in');
            if($log){
                $data["promociones_result"] = $this->promociones_hoteles_model->get_promos_especiales();
                $data["paquetes"] = $this->promociones_hoteles_model->get_paquetes();

                $data_header["seccion_banners_hoteles"] = 1;

                $this->load->view('view_header',$data_header);
                $this->load->view('view_promos_hoteles',$data);
                $this->load->view('view_footer');

               
            }else{
                 redirect('home/index'); 
            }
 
	}


    public function temporada(){
            $log = $this->session->userdata('logued_in');
            if($log){
                $data["paquetes_result"] = $this->paquetes_model->get_paquetes();
                $data["hoteles"] = $this->paquetes_model->get_hoteles();
                $data["monedas"] = $this->paquetes_model->get_monedas();

                $data_header["seccion_paquetes"] = 1;

                $this->load->view('view_header',$data_header);
                $this->load->view('view_promos_temporada',$data);
                $this->load->view('view_footer');

               
            }else{
                 redirect('home/index'); 
            }
 
    }


	
    public function recuperarPromocion(){
        $user = $this->promociones_hoteles_model->get_promocion($this->input->post('id_promo_especial'));
        echo json_encode($user);
    }

    public function recuperarHabitaciones(){
         $habitaciones = $this->habitaciones_model->get_habitacion_hotel($this->input->post('id_hotel'));
         foreach($habitaciones as $habitacion){
            echo '<option value="'.$habitacion->id_habitacion.'">'.$habitacion->nombre_habitacion.'</option>';
         }

    }

    public function habilitarPromocionEspecial(){
        $data = $this->input->post();
        $datos = array(
            'publicado' => $data['status']
        );
        echo $this->promociones_hoteles_model->actualiza_promocion_especial( $data['id_promo_especial'],$datos );
    }

    
    public function actualizarPromoEspecial(){

        $data = $this->input->post();
        $datos = array(
            'encabezado' => $data['titulo_experiencia_edit'],
            'tipo_layout' => $data['id_layout_edit'],
            'id_paquete' => $data['id_paquete_edit'],
            'texto_banner' => $data['texto_banner_edit'],
            'orden' => $data['orden_edit']
        );

        
        $config['upload_path'] = 'assets/imagenes/paquetes';
        $config['allowed_types'] = 'jpg|png|jpeg';

        $this->load->library('upload');
        $this->upload->initialize($config);

        

        $data_upload = array();
		if( !$this->upload->do_upload("input_imagen_completo_edit") ){
            $data_upload[0] = $this->upload->display_errors();
        }else{
            $img_data = $this->upload->data();
            $datos['imagen_banner'] = $img_data["file_name"];
        }

        echo '<div id="div_resultado">'.$this->promociones_hoteles_model->actualiza_promocion_especial( $data['id_promo_especial_edit'],$datos ).'</div>';
    }
    
    
    public function guardarPaquete(){
        $data = $this->input->post();
        $datos = array(
            'encabezado' => $data['titulo_experiencia_add'],
            'tipo_layout' => $data['id_layout_add'],
            'id_paquete' => $data['id_paquete_add'],
            'fecha_registro' => date('Y-m-d H:i:s'),
            'texto_banner' => $data['texto_banner_add'],
            'orden' => $data['orden_add']
        );

        $config['upload_path'] = 'assets/imagenes/paquetes';
        $config['allowed_types'] = 'jpg|png|jpeg';

        $this->load->library('upload');
        $this->upload->initialize($config);

        $data_upload = array();
		//Home mini
        if( !$this->upload->do_upload("input_imagen_completo_add") ){
            $data_upload[0] = $this->upload->display_errors();
        }else{
            $img_data = $this->upload->data();
            $datos['imagen_banner'] = $img_data["file_name"];
        }
		
		
        
        echo '<div id="div_resultado">'.$this->promociones_hoteles_model->guardar_promo( $datos ).'</div>';
    }
    
    
    public function eliminarPaquete(){
		$data = $this->input->post();
		echo $this->paquetes_model->eliminarPaquete($data['id_paquete']);
    }

}
