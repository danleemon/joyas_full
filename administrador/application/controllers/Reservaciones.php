<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reservaciones extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('reservaciones_model');
        $this->load->library('session');

        require_once APPPATH.'third_party/phpexcel/Classes/PHPExcel.php';
    }
        
    public function index(){
            $log = $this->session->userdata('logued_in');
            if($log){
                $data["reservaciones_result"] = $this->reservaciones_model->get_reservaciones();

                $data["hoteles"] = $this->reservaciones_model->get_hoteles();
                $data["monedas"] = $this->reservaciones_model->get_monedas();
                $data["tipos_habitacion"] = $this->reservaciones_model->get_tipos_habitacion();
                $data["paquetes"] = $this->reservaciones_model->get_paquetes();
                $data["tipos_tarjeta"] = $this->reservaciones_model->get_tipos_tarjeta();
                $data["status_reservaciones"] = $this->reservaciones_model->get_status_reservaciones();

                $data_header["seccion_reservaciones"] = 1;

                $this->load->view('view_header',$data_header);
                $this->load->view('view_reservaciones',$data);
                $this->load->view('view_footer');

               
            }else{
                 redirect('home/index'); 
            }
 
    }
    
    public function recuperarReservacion(){
        $user = $this->reservaciones_model->get_reservacion($this->input->post('id_reservacion'));
        echo json_encode($user);
    }

    public function habilitarPaquete(){
        $data = $this->input->post();
        $datos = array(
            'id_status_general' => $data['status']
        );
            
        echo $this->paquetes_model->actualiza_paquete( $data['id_paquete'],$datos );
    }
    
    public function actualizarReservacion(){
        $data = $this->input->post();
        $datos = array(
            'id_status_reservacion' => $data['id_status_reservacion_edit'],
            'otros_detalles' => $data['otros_detalles_edit']
        );


        echo '<div id="div_resultado">'.$this->reservaciones_model->actualiza_reservacion( $data['id_reservacion_edit'],$datos ).'</div>';
    }
    
    
    public function guardarPaquete(){
        $data = $this->input->post();
        $datos = array(
            'nombre_paquete' => $data['nombre_paquete_add'],
            'id_hotel' => $data['id_hotel_add'],
            'id_moneda' => $data['id_moneda_add'],
            'precio' => $data['precio_add'],
            'iva' => $data['iva_add'],
            'ish' => $data['ish_add'],
            'cargo_servicio' => $data['cargo_servicio_add'],
            'vigencia_inicio' => $data['vigencia_inicio_add'],
            'vigencia_fin' => $data['vigencia_fin_add'],
            'id_status_general' => '1',
            'descripcion' => $data['descripcion_add']
        );

        $config['upload_path'] = 'assets/imagenes/paquetes';
        $config['allowed_types'] = 'jpg|png|jpeg';

        $this->load->library('upload');
        $this->upload->initialize($config);

        $data_upload = array();
        //Home mini
        if( !$this->upload->do_upload("input_imagen_add") ){
            $data_upload[0] = $this->upload->display_errors();
        }else{
            $img_data = $this->upload->data();
            $datos['imagen_detalle'] = $img_data["file_name"];
        }
        
        
        
        echo '<div id="div_resultado">'.$this->paquetes_model->guardar_paquete( $datos ).'</div>';
    }
    
    
    public function eliminarPaquete(){
        $data = $this->input->post();
        echo $this->paquetes_model->eliminarPaquete($data['id_paquete']);
    }




    public function descarga_csv( ){

            $data = $this->input->post();
            $valores = $this->reservaciones_model->get_reservaciones();
            
            // Create new PHPExcel object
            $objPHPExcel = new PHPExcel();
            
            // Pestaña del webservice
            $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Arial');
            $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(11);
            $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('2283c5');
            $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFont()->getColor()->setARGB('FFFFFF');
            $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            for($col = 'A'; $col !== 'G'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
            $objPHPExcel->getActiveSheet()->setAutoFilter("A1:F1");

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A1', 'Clave de reservacion')
                        ->setCellValue('B1', 'Cliente')
                        ->setCellValue('C1', 'Hotel')
                        ->setCellValue('D1', 'Paquete')
                        ->setCellValue('E1', 'Fecha de registro')
                        ->setCellValue('F1', 'Estatus');
            $c_ws = 1;
            foreach ($valores as $val) {
                $c_ws++;
                $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$c_ws, $val->clave_reservacion)
                            ->setCellValue('B'.$c_ws, $val->nombre_cliente.' '.$val->apellidos_cliente)
                            ->setCellValue('C'.$c_ws, $val->nombre_hotel)
                            ->setCellValue('D'.$c_ws, $val->nombre_paquete)
                            ->setCellValue('E'.$c_ws, $val->fecha_registro)
                            ->setCellValue('F'.$c_ws, $val->status_reservacion);
            } 
            
            $objPHPExcel->setActiveSheetIndex(0)->setTitle('Concentrado');
            $objPHPExcel->setActiveSheetIndex(0);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="Reservaciones_'.date('Y_m_d_H_i_s').'.xls"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
            header ('Cache-Control: cache, must-revalidate');
            header ('Pragma: public');
            
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save('php://output');
            exit;
    }


}
