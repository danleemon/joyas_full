<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Solicitudes extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('solicitudes_model');
        $this->load->library('session');
        //$this->load->library('curl');
        require_once APPPATH.'third_party/PHPMailer-master/PHPMailerAutoload.php';
        require_once APPPATH.'third_party/phpexcel/Classes/PHPExcel.php';
	}
        
	public function index(){
            $log = $this->session->userdata('logued_in');
            if($log){
                $data["solicitudes_result"] = $this->solicitudes_model->get_solicitudes();
                $data["tipos_habitacion"] = $this->solicitudes_model->get_tipos_habitacion();
                $data["paquetes"] = $this->solicitudes_model->get_paquetes();
                $data["tipos_tarjeta"] = $this->solicitudes_model->get_tipos_tarjeta();
                $data["status_solicitud"] = $this->solicitudes_model->get_status_solicitudes();

                $data_header["seccion_solicitudes"] = 1;

                $this->load->view('view_header',$data_header);
                $this->load->view('view_solicitudes',$data);
                $this->load->view('view_footer');

               
            }else{
                 redirect('home/index'); 
            }
        
	}
	public function recuperarSolicitud(){
        $user = $this->solicitudes_model->get_solicitud($this->input->post('id_solicitud'));
        echo json_encode($user);
    }

    public function habilitarSolicitud(){
        $data = $this->input->post();
        $datos = array(
            'id_status_solicitud' => $data['status']
        );
            
        echo $this->solicitudes_model->actualiza_solicitud( $data['id_solicitud'],$datos );
    }


     public function actualizarSolicitud(){
        $data = $this->input->post();
        $datos = array(
            'id_solicitud' => $data['id_solicitud_edit'],
            'id_status_solicitud' => $data['id_status_solicitud_edit'],
            'id_paquete' => $data['id_paquete_edit'],
            'titulo_cliente' => $data['titulo_cliente_edit'],
            'nombre_cliente' => $data['nombre_cliente_edit'],
            'apellidos_cliente' => $data['apellidos_cliente_edit'],
            'fecha_registro' => $data['fecha_registro_edit'],
            'clave_reservacion' => $data['clave_reservacion_edit'],
            'direccion_cliente' => $data['direccion_cliente_edit'],
            'ciudad_cliente' => $data['ciudad_cliente_edit'],
            'estado_cliente' => $data['estado_cliente_edit'],
            'pais_cliente' => $data['pais_cliente_edit'],
            'delegacion_cliente' => $data['delegacion_cliente_edit'],
            'codigo_postal_cliente' => $data['codigo_postal_cliente_edit'],
            'fecha_nacimiento_cliente' => $data['fecha_nacimiento_cliente_edit'],
            'telefono_cliente' => $data['telefono_cliente_edit'],
            'movil_cliente' => $data['movil_cliente_edit'],
            'correo_cliente' => $data['correo_cliente_edit'],
            'id_tipo_tarjeta' => $data['id_tipo_tarjeta_edit'],
            'titular_tarjeta' => $data['titular_tarjeta_edit'],
            'numero_tarjeta' => $data['numero_tarjeta_edit'],
            'codigo_seguridad' => $data['codigo_seguridad_edit'],
            'fecha_vencimiento_mes' => $data['fecha_vencimiento_mes_edit'],
            'fecha_vencimiento_anio' => $data['fecha_vencimiento_anio_edit'],
            'fecha_llegada' => $data['fecha_llegada_edit'],
            'hora_llegada' => $data['hora_llegada_edit'],
            'fecha_salida' => $data['fecha_salida_edit'],
            'hora_salida' => $data['hora_salida_edit'],
            'observaciones' => $data['observaciones_edit'],
            'id_tipo_habitacion' => $data['id_tipo_habitacion_edit'],
            'numero_menores' => $data['numero_menores_edit'],
            'numero_adultos' => $data['numero_adultos_edit']
        );

        echo '<div id="div_resultado">'.$this->solicitudes_model->actualiza_solicitud( $data['id_solicitud_edit'],$datos ).'</div>';
    }



    public function confirmarSolicitud(){

    	$data = $this->input->post();

        //Actualiza estatus de solicitud
        $datos = array(
            'id_status_solicitud' => $data['id_status_solicitud']
        );
        $this->solicitudes_model->actualiza_solicitud( $data['id_solicitud'],$datos );

        //Se crea la reservacion
        $datos = array(
            'clave_reservacion' => $data['clave_reservacion'],
            'id_status_reservacion' => '1'
        );

        $solicitudes_ = $this->solicitudes_model->guardar_reservacion( $datos );
        $correo = $data['correo_cliente'];



        $reserva_ = $this->solicitudes_model->get_reservacion_clave($data['clave_reservacion']);
        $personas = intval($reserva_['numero_adultos'])+intval($reserva_['numero_menores']);
        $subtotal = 0;

        $id_hotel_ = intval($reserva_['id_hotel']);
        $id_paquete_ = intval($reserva_['id_paquete']);

        if($id_hotel_==4 || $id_hotel_==6  || $id_hotel_==10  || $id_hotel_==11 || $id_hotel_==8  || $id_hotel_==9 || $id_hotel_==7  || $id_hotel_==12 || $id_paquete_==39){
            $subtotal = intval($reserva_['precio']);
        }else{
            $subtotal = $personas*intval($reserva_['precio']);
        }

        $iva = 0;
        $ish = 0;
        $cargo = 0;
        if($reserva_['incluye_impuestos']==2){ //no incluye impuestos, calcular
            $iva = intval($reserva_['iva']);
            $ish = intval($reserva_['ish']);
            $iva = $subtotal * ($iva/100);
            $ish = $subtotal * ($ish/100);
            if( $reserva_['tipo_cargo']==1){ //cargo fijo
                $cargo = intval($reserva_['iva']);

            }else if( $reserva_['tipo_cargo']==2 ){//porcentual
                $cargo = intval($reserva_['iva']);
                $cargo = $subtotal * ($cargo/100);

            }
        }

        $total = $subtotal + $iva +$ish + $cargo;





        $mail_p = new PHPMailer();
        $mail_p->IsSMTP();
        $mail_p->SMTPAuth   = true;
        $mail_p->SMTPSecure = "tls"; 
        $mail_p->Host       = "smtp.mandrillapp.com";
        $mail_p->Port       = 587; 
        $mail_p->Username   = "aquintana@estrategica.com";
        $mail_p->Password   = "9C6kelU75JH5kwhS_BUFFQ";
        $mail_p->From       = "hola@joyasdemexico.mx";
        $mail_p->FromName   = "Joyas de Mexico.";
        $mail_p->Subject    = "Reservacion confirmada";
        $mail_p->AltBody    = "Reservacion confirmada";
        $mail_p->AddReplyTo("hola@joyasdemexico.mx","Joyas de Mexico");
        $mail_p->WordWrap   = 50;
        $mail_p->AddAddress($correo, '');
        $mail_p->AddBCC('adrian.hibert@joyasdemexico.mx');
        $mail_p->IsHTML(true);
        $mail_p->CharSet = 'UTF-8';

         $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
                        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                        <html xmlns="http://www.w3.org/1999/xhtml">
                        <head>
                        <title>Joyas de Mexico - Recibo de compra</title>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
						<link href=\'https://fonts.googleapis.com/css?family=Questrial\' rel=\'stylesheet\' type=\'text/css\'>

                        <style>

                        </style>
                        </head>
                            
                            
                        <body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
                        
                        <table width="1024" border="0" cellspacing="0" style="font-size:12px;background-repeat: no-repeat;" cellpadding="20" background="http://198.199.104.194/administrador/assets/images/email_recibo_cleanv3.jpg">

                            <tr>
                                <td colspan="5" width="1024" height="410" style="text-align:left"></td>
                            </tr>
                            

                            <tr>
                                <td width="150" height="30" style="text-align:left"></td>
                                 <td width="250" height="30" style="text-align:left"></td>
                                <td colspan="2" width="500"height="30" style="font-size:20px;text-align:left;color:#8C8C8C;font-family: \'Questrial\',sans-serif;">
                                    
                                    <p style="color:#AA9048;font-family:\'Questrial\',sans-serif;">CLAVE DE RESERVACIÓN '.$reserva_['clave_reservacion'].'</p>
                                    RESERVADO A '.strtoupper($reserva_['nombre_cliente']).' '.strtoupper($reserva_['apellidos_cliente']).'
                                </td>

                              
                                <td width="124" height="30" style="text-align:left" coalspan="2"></td>
                            </tr>

                            <tr>
                                <td width="150" height="10" style="text-align:left;"></td>
                                <td colspan="2" width="550" height="10" style="border-top:thin dotted ;border-color:#8C8C8C;text-align:left;color:#58585b;font-family: \'Questrial\',sans-serif;">
                                DETALLES DE TU RESERVACIÓN
                                 </td>
                                 
                                <td  width="200" height="10" style="text-align:left;color:#58585b;border-top:thin dotted ;border-color:#8C8C8C;font-family: \'Questrial\',sans-serif;">RESÚMEN</td>
                                <td width="124" height="10" style="text-align:left" coalspan="2"></td>
                            </tr>

                             <tr>
                                <td width="150" height="30" style="text-align:left"></td>
                                <td width="250" height="30" style="text-align:left;color:#8C8C8C;font-family: \'Questrial\',sans-serif;">
                                HOTEL
                                 </td>
                                 <td width="300" height="30" style="text-align:left;color:#8C8C8C">
                                  <p style="color:#8C8C8C;font-family: \'Questrial\',sans-serif;">'.strtoupper($reserva_['nombre_hotel']).'</p>
                                 </td>
                                <td  width="200" height="30" style="text-align:left;color:#8C8C8C;font-family: \'Questrial\',sans-serif;">
                                     Subtotal: $'.number_format($subtotal,2).'<br/>';

                                if($reserva_['incluye_impuestos']==2){
                                    $message .= 'IVA ('.$reserva_['iva'].'%): $'.number_format(  $iva,2).'</br>';
                                    $message .= 'ISH ('.$reserva_['ish'].'%): $'.number_format(  $ish,2).'</br>';
                                    if( $reserva_['tipo_cargo']==1){ //cargo fijo
                                       $message .= 'Cargo: $'.number_format(  $cargo,2).'</p>';

                                    }else if( $reserva_['tipo_cargo']==2 ){//porcentual
                                         $message .= 'Cargo ('.$reserva_['cargo_servicio'].'%): $'.number_format(  $cargo,2).'</br>';

                                    }

                                }else{
                                    $message .= 'IVA ('.$reserva_['iva'].'%): Incluido</br>';
                                    $message .= 'ISH ('.$reserva_['ish'].'%): Incluido</br>';
                                }

                                    $message .= ' Total: $'.number_format($total,2).'</br>';
                                if($reserva_['id_moneda']==1){
                                    $message .= ' PESOS MEXICANOS</br>';
                                }else{
                                    $message .= ' DÓLARES AMERICANOS</br>';

                                }
                               $message .= ' </td>
                                <td width="124" height="30" style="text-align:left" coalspan="2"></td>
                            </tr>';

                            $imagen_mapa = $reserva_['imagen_mapa'];


        $message .= '<tr>
                                <td width="150" height="50" style="text-align:left"></td>
                                
                                <td  width="250" height="50" style="text-align:left;color:#8C8C8C;font-family: \'Questrial\',sans-serif;border-top:thin dotted ;border-color:#8C8C8C;">
                                TIPO DE HABITACIÓN<br/>
                                NÚMERO DE PERSONAS<br/>
                                <br/>
                               LLEGADA<br/>
                               SALIDA<br/>
                               HORA DE ENTRADA<br/>
                               HORA DE SALIDA<br/>  
                                </td>
                                <td width="300" height="50" style="text-align:left;color:#8C8C8C;font-family: \'Questrial\',sans-serif;border-top:thin dotted ;border-color:#8C8C8C;">
                               '.strtoupper($reserva_['nombre_habitacion']).'<br/>
                               '.$personas.'<br/>
                                <br/>
                               '.$reserva_['fecha_llegada'].'<br/>
                               '.$reserva_['fecha_salida'].'<br/>
                               '.$reserva_['hora_llegada'].'<br/>
                               '.$reserva_['hora_salida'].'<br/>
                                </td>
                                <td  colspan="2" width="324" height="50" style="text-align:left">

                                    <img width="250" height="120" src="http://198.199.104.194/administrador/assets/imagenes/hoteles/'.$imagen_mapa.'" />
                               

                                </td>
                            </tr>



                            <tr>
                                <td colspan="5" width="1024" height="300" style="text-align:left"></td>
                            </tr>

                        </table>
                        </body>
                        </html>';
        //$mail_p->addAttachment("assets/pdf/recibo_de_compra.pdf");


        $mail_p->Body = $message;

        if($mail_p->Send()) {
            echo "1";
        }else{
            echo "2";
        }



    	/*$htmlFile = base_url()."/assets/pdf/Pronostico_general.pdf"; 

        require(APPPATH.'libraries/html2pdf/html2pdf.class.php');
        $html2pdf = new HTML2PDF('L','LETTER','es');
        $html2pdf->setDefaultFont('arial');
        $html = " <page backimg='".base_url()."/assets/pdf/Pronostico_general.jpg'>";
        $html .= "</page>";
        $html2pdf->writeHTML($html);
        $html2pdf->Output('assets/pdf/confirmacion.pdf', 'F');*/


        $url = 'https://api.sendgrid.com/';
        $user = 'danleemon';
        $pass = 'solstice123';

        $params = array(
            'api_user'  => $user,
            'api_key'   => $pass,
            'to'        => 'hola@joyasdemexico.mx',
            'bcc'       => 'dlimon@estrategica.com',
            'bcc'       => 'adominguez@estrategica.com',
            'subject'   => 'Reservacion confirmada',
            'html'      => $message,
            'text'      => 'Reservacion confirmada',
            'from'      => 'hola@joyasdemexico.mx',
        );

        
        $request =  $url.'api/mail.send.json';


        $session = curl_init($request);
        curl_setopt ($session, CURLOPT_POST, true);
        curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
        curl_setopt($session, CURLOPT_HEADER, false);
        //curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($session);
        curl_close($session);
			
			
    }



    public function showvars(){

        $reserva_ = $this->solicitudes_model->get_reservacion_clave('3456345634563');
        echo "<br/>precio ".$reserva_['precio'];
       

        $personas = intval($reserva_['numero_adultos'])+intval($reserva_['numero_menores']);
        echo "<br/>personas ".$personas;

        $subtotal = $personas*intval($reserva_['precio']);
        echo "<br/>subtotal ".$subtotal;


        echo "<br/>Incluye ".$reserva_['incluye_impuestos'];
        echo "<br/>Tipo cargo ".$reserva_['tipo_cargo'];

        $iva = 0;
        $ish = 0;
        $cargo = 0;
        //if($reserva_['incluye_impuestos']==2){ //no incluye impuestos, calcular
            $iva = intval($reserva_['iva']);
            $ish = intval($reserva_['ish']);
            $iva = $subtotal * ($iva/100);
            $ish = $subtotal * ($ish/100);
            if( $reserva_['tipo_cargo']==1){ //cargo fijo
                $cargo = intval($reserva_['iva']);

            }else if( $reserva_['tipo_cargo']==2 ){//porcentual
                $cargo = intval($reserva_['iva']);
                $cargo = $subtotal * ($cargo/100);

            }
       // }

        echo "<br/>IVA ".$iva;
        echo "<br/>ISH ".$ish;
        echo "<br/>Cargo ".$cargo;

        $total = $subtotal + $iva +$ish + $cargo;

        echo "<br/>total ".$total;

    }

    public function pruebapdf(){


         $reserva_ = $this->solicitudes_model->get_reservacion_clave('3456345634563');


        $personas = intval($reserva_['numero_adultos'])+intval($reserva_['numero_menores']);
        $subtotal = $personas*intval($reserva_['precio']);

        $iva = 0;
        $ish = 0;
        $cargo = 0;
        if($reserva_['incluye_impuestos']==2){ //no incluye impuestos, calcular
            $iva = intval($reserva_['iva']);
            $ish = intval($reserva_['ish']);
            $iva = $subtotal * ($iva/100);
            $ish = $subtotal * ($ish/100);
            if( $reserva_['tipo_cargo']==1){ //cargo fijo
                $cargo = intval($reserva_['iva']);

            }else if( $reserva_['tipo_cargo']==2 ){//porcentual
                $cargo = intval($reserva_['iva']);
                $cargo = $subtotal * ($cargo/100);

            }
        }

        $total = $subtotal + $iva +$ish + $cargo;

        $htmlFile = base_url()."/assets/images/recibo de compra.pdf"; 

        require(APPPATH.'libraries/html2pdf/html2pdf.class.php');
        $html2pdf = new HTML2PDF('L','LETTER','es');
        $html2pdf->setDefaultFont('arial');
        $html = " <page backimg='".base_url()."/assets/images/recibo-de-compra.jpg'>";
        /*$html .= '<table width="1024px" height="1359px" border="0" cellspacing="0" style="background-repeat: no-repeat;" cellpadding="20" background="http://198.199.104.194/administrador/assets/images/email_recibo_clean.jpg">

                            
                            <tr>
                                <td width="150" height="200" style="text-align:left"></td>
                                <td colspan="3" width="750" height="200" style="text-align:left;color:#8C8C8C" colspan="3">
                                    Cd. de México a '.date("Y-m-d").'
                                    Att. '.$reserva_['nombre_cliente'].' '.$reserva_['apellidos_cliente'].'
                                    Presente
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                </td>
                                <td width="124" height="200" style="text-align:left"></td>
                            </tr>


                            <tr>
                                <td width="150" style="text-align:left"></td>
                                 <td width="250"  style="text-align:left"></td>
                                <td width="300" style="text-align:left;color:#8C8C8C">
                                    
                                    <p style="color:#AA9048;font-size:24px">CLAVE DE RESERVACIÓN</p>
                                     RESERVADO A
                                </td>
                                <td  width="200" style="text-align:left;color:#8C8C8C">
                                    <p style="color:#AA9048;font-size:24px">'.$reserva_['clave_reservacion'].'</p>
                                    '.$reserva_['nombre_cliente'].' '.$reserva_['apellidos_cliente'].'
                                </td>
                                <td width="124" style="text-align:left" coalspan="2"></td>
                            </tr>

                         

                             <tr>
                                <td width="150" height="70" style="text-align:left"></td>
                                <td width="250" height="70" style="text-align:left;color:#8C8C8C">
                                Hotel
                                 </td>
                                 <td width="300" height="70" style="text-align:left;color:#8C8C8C">
                                 '.$reserva_['nombre_hotel'].'
                                 </td>
                                <td  width="200" height="70" style="text-align:left;color:#8C8C8C">
                                     Subtotal: $'.number_format($subtotal,2).'<br/>';

                                if($reserva_['incluye_impuestos']==2){
                                     $html .= 'IVA ('.$reserva_['iva'].'%): $'.number_format(  $iva,2).'';
                                     $html .= 'ISH ('.$reserva_['ish'].'%): $'.number_format(  $ish,2).'';
                                    if( $reserva_['tipo_cargo']==1){ //cargo fijo
                                        $html .= 'Cargo: $'.number_format(  $cargo,2).'</p>';

                                    }else if( $reserva_['tipo_cargo']==2 ){//porcentual
                                          $html .= 'Cargo ('.$reserva_['cargo_servicio'].'%): $'.number_format(  $ $cargo,2).'';

                                    }

                                }else{
                                    $html .= 'IVA ('.$reserva_['iva'].'%): Incluido';
                                     $html .= 'ISH ('.$reserva_['ish'].'%): Incluido';
                                }

       $html .= ' Total: $'.number_format($total,2).'

                                </td>
                                <td width="124" height="70" style="text-align:left" coalspan="2"></td>
                            </tr>';



        $html .= '<tr>
                                <td width="150" height="50" style="text-align:left"></td>
                                
                                <td  width="250" height="50" style="text-align:left">
                                TIPO DE HABITACIÓN
                                NÚMERO DE PERSONAS
                                
                               LLEGADA
                               SALIDA
                               HORA DE ENTRADA
                               HORA DE SALIDA
                                </td>
                                <td width="300" height="50" style="text-align:left;color:#8C8C8C">
                               '.$reserva_['nombre_habitacion'].'
                               '.$personas.'
                               
                               '.$reserva_['fecha_llegada'].'
                               '.$reserva_['fecha_salida'].'
                               '.$reserva_['hora_llegada'].'
                               '.$reserva_['hora_salida'].'
                                </td>
                                <td  width="200" height="50" style="text-align:left"></td>
                                <td width="124" height="50" style="text-align:left" coalspan="2"></td>
                            </tr>



                            <tr>
                                <td colspan="5" width="1024" height="500" style="text-align:left"></td>
                            </tr>

                        </table>';*/
        $html .= "</page>";
        $html2pdf->writeHTML($html);
        $html2pdf->Output('assets/pdf/confirmacion.pdf', 'F');
        //$html2pdf->Output('confirmacion.pdf', 'F');



      
            
           
    }

    public function pruebaConfirmarSolicitud(){
        $reserva_ = $this->solicitudes_model->get_reservacion_clave('3456345634563');


        $personas = intval($reserva_['numero_adultos'])+intval($reserva_['numero_menores']);
        $subtotal = 0;

        $id_hotel_ = intval($reserva_['id_hotel']);
        $id_paquete_ = intval($reserva_['id_paquete']);

        if($id_hotel_==4 || $id_hotel_==6  || $id_hotel_==10  || $id_hotel_==11 || $id_hotel_==8  || $id_hotel_==9 || $id_hotel_==7  || $id_hotel_==12 || $id_paquete_==39){
            $subtotal = intval($reserva_['precio']);
        }else{
            $subtotal = $personas*intval($reserva_['precio']);
        }

        $iva = 0;
        $ish = 0;
        $cargo = 0;
        if($reserva_['incluye_impuestos']==2){ //no incluye impuestos, calcular
            $iva = intval($reserva_['iva']);
            $ish = intval($reserva_['ish']);
            $iva = $subtotal * ($iva/100);
            $ish = $subtotal * ($ish/100);
            if( $reserva_['tipo_cargo']==1){ //cargo fijo
                $cargo = intval($reserva_['iva']);

            }else if( $reserva_['tipo_cargo']==2 ){//porcentual
                $cargo = intval($reserva_['iva']);
                $cargo = $subtotal * ($cargo/100);

            }
        }

        $total = $subtotal + $iva +$ish + $cargo;

 
        $mail_p = new PHPMailer();
        $mail_p->IsSMTP();
        $mail_p->SMTPAuth   = true;
        $mail_p->SMTPSecure = "tls"; 
        $mail_p->Host       = "smtp.mandrillapp.com";
        $mail_p->Port       = 587; 
        $mail_p->Username   = "aquintana@estrategica.com";
        $mail_p->Password   = "9C6kelU75JH5kwhS_BUFFQ";
        $mail_p->From       = "hola@joyasdemexico.mx";
        $mail_p->FromName   = "Joyas de Mexico.";
        $mail_p->Subject    = "Reservacion confirmada";
        $mail_p->AltBody    = "Reservacion confirmada";
        $mail_p->AddReplyTo("hola@joyasdemexico.mx","Joyas de Mexico");
        $mail_p->WordWrap   = 50;
        $mail_p->AddAddress('daniel.limon@hotmail.com', '');
        $mail_p->IsHTML(true);
        $mail_p->CharSet = 'UTF-8';
        $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
                        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                        <html xmlns="http://www.w3.org/1999/xhtml">
                        <head>
                        <title>Joyas de Mexico - Recibo de compra</title>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
						<link href=\'https://fonts.googleapis.com/css?family=Questrial\' rel=\'stylesheet\' type=\'text/css\'>

                        <style>

                        </style>
                        </head>
                            
                            
                        <body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
                        
                        <table width="1024" border="0" cellspacing="0" style="font-size:12px;background-repeat: no-repeat;" cellpadding="20" background="http://198.199.104.194/administrador/assets/images/email_recibo_cleanv3.jpg">

                            <tr>
                                <td colspan="5" width="1024" height="410" style="text-align:left"></td>
                            </tr>
                            

                            <tr>
                                <td width="150" height="30" style="text-align:left"></td>
                                 <td width="250" height="30" style="text-align:left"></td>
                                <td colspan="2" width="500"height="30" style="font-size:20px;text-align:left;color:#8C8C8C;font-family: \'Questrial\',sans-serif;">
                                    
                                    <p style="color:#AA9048;font-family:\'Questrial\',sans-serif;">CLAVE DE RESERVACIÓN '.$reserva_['clave_reservacion'].'</p>
                                    RESERVADO A '.strtoupper($reserva_['nombre_cliente']).' '.strtoupper($reserva_['apellidos_cliente']).'
                                </td>

                              
                                <td width="124" height="30" style="text-align:left" coalspan="2"></td>
                            </tr>

                            <tr>
                                <td width="150" height="10" style="text-align:left;"></td>
                                <td colspan="2" width="550" height="10" style="border-top:thin dotted ;border-color:#8C8C8C;text-align:left;color:#58585b;font-family: \'Questrial\',sans-serif;">
                                DETALLES DE TU RESERVACIÓN
                                 </td>
                                 
                                <td  width="200" height="10" style="text-align:left;color:#58585b;border-top:thin dotted ;border-color:#8C8C8C;font-family: \'Questrial\',sans-serif;">RESÚMEN</td>
                                <td width="124" height="10" style="text-align:left" coalspan="2"></td>
                            </tr>

                             <tr>
                                <td width="150" height="30" style="text-align:left"></td>
                                <td width="250" height="30" style="text-align:left;color:#8C8C8C;font-family: \'Questrial\',sans-serif;">
                                HOTEL
                                 </td>
                                 <td width="300" height="30" style="text-align:left;color:#8C8C8C">
                                  <p style="color:#8C8C8C;font-family: \'Questrial\',sans-serif;">'.strtoupper($reserva_['nombre_hotel']).'</p>
                                 </td>
                                <td  width="200" height="30" style="text-align:left;color:#8C8C8C;font-family: \'Questrial\',sans-serif;">
                                     Subtotal: $'.number_format($subtotal,2).'<br/>';

                                if($reserva_['incluye_impuestos']==2){
                                    $message .= 'IVA ('.$reserva_['iva'].'%): $'.number_format(  $iva,2).'</br>';
                                    $message .= 'ISH ('.$reserva_['ish'].'%): $'.number_format(  $ish,2).'</br>';
                                    if( $reserva_['tipo_cargo']==1){ //cargo fijo
                                       $message .= 'Cargo: $'.number_format(  $cargo,2).'</p>';

                                    }else if( $reserva_['tipo_cargo']==2 ){//porcentual
                                         $message .= 'Cargo ('.$reserva_['cargo_servicio'].'%): $'.number_format(  $cargo,2).'</br>';

                                    }

                                }else{
                                    $message .= 'IVA ('.$reserva_['iva'].'%): Incluido</br>';
                                    $message .= 'ISH ('.$reserva_['ish'].'%): Incluido</br>';
                                }

                                    $message .= ' Total: $'.number_format($total,2).'</br>';
                                if($reserva_['id_moneda']==1){
                                    $message .= ' PESOS MEXICANOS</br>';
                                }else{
                                    $message .= ' DÓLARES AMERICANOS</br>';

                                }
                               $message .= ' </td>
                                <td width="124" height="30" style="text-align:left" coalspan="2"></td>
                            </tr>';

                            $imagen_mapa = $reserva_['imagen_mapa'];


        $message .= '<tr>
                                <td width="150" height="50" style="text-align:left"></td>
                                
                                <td  width="250" height="50" style="text-align:left;color:#8C8C8C;font-family: \'Questrial\',sans-serif;border-top:thin dotted ;border-color:#8C8C8C;">
                                TIPO DE HABITACIÓN<br/>
                                NÚMERO DE PERSONAS<br/>
                                <br/>
                               LLEGADA<br/>
                               SALIDA<br/>
                               HORA DE ENTRADA<br/>
                               HORA DE SALIDA<br/>  
                                </td>
                                <td width="300" height="50" style="text-align:left;color:#8C8C8C;font-family: \'Questrial\',sans-serif;border-top:thin dotted ;border-color:#8C8C8C;">
                               '.strtoupper($reserva_['nombre_habitacion']).'<br/>
                               '.$personas.'<br/>
                                <br/>
                               '.$reserva_['fecha_llegada'].'<br/>
                               '.$reserva_['fecha_salida'].'<br/>
                               '.$reserva_['hora_llegada'].'<br/>
                               '.$reserva_['hora_salida'].'<br/>
                                </td>
                                <td  colspan="2" width="324" height="50" style="text-align:left">

                                    <img width="250" height="120" src="http://198.199.104.194/administrador/assets/imagenes/hoteles/'.$imagen_mapa.'" />
                               

                                </td>
                            </tr>



                            <tr>
                                <td colspan="5" width="1024" height="300" style="text-align:left"></td>
                            </tr>

                        </table>
                        </body>
                        </html>';
        //$mail_p->addAttachment("assets/pdf/recibo_de_compra.pdf");


        $mail_p->Body = $message;

        if($mail_p->Send()) {
            echo "1";
        }else{
            echo "2";
        }



        /*$htmlFile = base_url()."/assets/pdf/Pronostico_general.pdf"; 

        require(APPPATH.'libraries/html2pdf/html2pdf.class.php');
        $html2pdf = new HTML2PDF('L','LETTER','es');
        $html2pdf->setDefaultFont('arial');
        $html = " <page backimg='".base_url()."/assets/pdf/Pronostico_general.jpg'>";
        $html .= "</page>";
        $html2pdf->writeHTML($html);
        $html2pdf->Output('assets/pdf/confirmacion.pdf', 'F');*/



        
        $url = 'https://api.sendgrid.com/';
        $user = 'danleemon';
        $pass = 'solstice123';

        $params = array(
            'api_user'  => $user,
            'api_key'   => $pass,
            'to'        => 'daniel.limon@hushmail.me',
            'bcc'       => 'dlimon@estrategica.com',
            'subject'   => 'Reservacion confirmada (prueba)',
            'html'      => $message,
            'text'      => 'Reservacion confirmada (prueba)',
            'from'      => 'hola@joyasdemexico.mx',
        );

        /*$params = array(
            'api_user'  => $user,
            'api_key'   => $pass,
            'to'        => 'dlimon@estrategica.com',
            'subject'   => 'Programa de Incentivos Canon - Buzon de Quejas y Sugerencias',
            'html'      => $message,
            'text'      => $message,
            'from'      => 'comunicados@canon.com',
        );*/
        
        
        $request =  $url.'api/mail.send.json';


        $session = curl_init($request);
        curl_setopt ($session, CURLOPT_POST, true);
        curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
        curl_setopt($session, CURLOPT_HEADER, false);
        //curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($session);
        curl_close($session);

        echo "sendgrid ok";
            


            
    }



        public function descarga_csv( ){

            $data = $this->input->post();
            $valores = $this->solicitudes_model->get_solicitudes();
            
            // Create new PHPExcel object
            $objPHPExcel = new PHPExcel();
            
            // Pestaña del webservice
            $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Arial');
            $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(11);
            $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('2283c5');
            $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFont()->getColor()->setARGB('FFFFFF');
            $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            for($col = 'A'; $col !== 'G'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }
            $objPHPExcel->getActiveSheet()->setAutoFilter("A1:F1");

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A1', 'Clave de reservacion')
                        ->setCellValue('B1', 'Cliente')
                        ->setCellValue('C1', 'Hotel')
                        ->setCellValue('D1', 'Paquete')
                        ->setCellValue('E1', 'Fecha de registro')
                        ->setCellValue('F1', 'Estatus');
            $c_ws = 1;
            foreach ($valores as $val) {
                $c_ws++;
                $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$c_ws, $val->clave_reservacion)
                            ->setCellValue('B'.$c_ws, $val->nombre_cliente.' '.$val->apellidos_cliente)
                            ->setCellValue('C'.$c_ws, $val->nombre_hotel)
                            ->setCellValue('D'.$c_ws, $val->nombre_paquete)
                            ->setCellValue('E'.$c_ws, $val->fecha_registro)
                            ->setCellValue('F'.$c_ws, $val->status_solicitud);
            } 
            
            $objPHPExcel->setActiveSheetIndex(0)->setTitle('Concentrado');
            $objPHPExcel->setActiveSheetIndex(0);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="Solicitudes_'.date('Y_m_d_H_i_s').'.xls"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
            header ('Cache-Control: cache, must-revalidate');
            header ('Pragma: public');
            
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save('php://output');
            exit;
    }





}
