<?php
class contacto_model extends CI_Model {
    
    function __construct(){
        $this->load->database();
    }
    
    function get_contacto($id_contacto){
		if ($id_contacto === FALSE){
			return FALSE;
		}
		$query = $this->db->get_where('tbl_contacto', array('id_contacto' => $id_contacto));
		return $query->row_array();
    }

    function actualiza_contacto($id_contacto,$datos){
        $this->db->where("id_contacto",$id_contacto);
        $this->db->update("tbl_contacto",$datos);
		return $this->db->affected_rows();
    }

    function guardar_contacto($datos){
        $this->db->insert('tbl_contacto',$datos);
        return $this->db->affected_rows();
    }

     function eliminarContacto($id_contacto){
        $this->db->where("id_contacto",$id_contacto);
        $this->db->delete("tbl_contacto");
		return $this->db->affected_rows();
    }

    function get_contactos(){
        	$query_l = "SELECT 
                        c.*, s.descripcion as status_contacto 
                        FROM  tbl_contacto c
                        INNER JOIN tbl_status_contacto s ON s.id_status_contacto = c.id_status_contacto";
        	$query = $this->db->query($query_l);
			return $query->result();
    }

    function get_status_contacto(){
            $query_l = "SELECT 
                        s.*
                        FROM  tbl_status_contacto s";
            $query = $this->db->query($query_l);
            return $query->result();
    }

}
?>
