<?php
class facturacion_model extends CI_Model {
    
    function __construct(){
        $this->load->database();
    }
    
    function get_facturacion($id_facturacion){
		if ($id_facturacion === FALSE){
			return FALSE;
		}
		$query = $this->db->get_where('tbl_facturacion', array('id_facturacion' => $id_facturacion));
		return $query->row_array();
    }

    function actualiza_facturacion($id_facturacion,$datos){
        $this->db->where("id_facturacion",$id_facturacion);
        $this->db->update("tbl_facturacion",$datos);
		return $this->db->affected_rows();
    }

    function guardar_facturacion($datos){
        $this->db->insert('tbl_facturacion',$datos);
        return $this->db->affected_rows();
    }

     function eliminarFacturacion($id_facturacion){
        $this->db->where("id_facturacion",$id_facturacion);
        $this->db->delete("tbl_facturacion");
		return $this->db->affected_rows();
    }

    function get_facturaciones(){
        	$query_l = "SELECT 
                        f.*, s.descripcion as status_factura 
                        FROM  tbl_facturacion f
                        INNER JOIN tbl_status_factura s ON s.id_status_factura = f.id_status_factura";
        	$query = $this->db->query($query_l);
			return $query->result();
    }

    function get_status_solicitudes(){
            $query_l = "SELECT 
                        s.*
                        FROM  tbl_status_factura s";
            $query = $this->db->query($query_l);
            return $query->result();
    }

}
?>
