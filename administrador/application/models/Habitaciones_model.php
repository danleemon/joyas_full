<?php
class habitaciones_model extends CI_Model {
    
    function __construct(){
        $this->load->database();
    }
    
    function get_habitacion($id_habitacion){
		if ($id_habitacion === FALSE){
			return FALSE;
		}
		$query = $this->db->get_where('tbl_habitaciones', array('id_habitacion' => $id_habitacion));
		return $query->row_array();
    }

    function get_habitacion_hotel($id_hotel){
        $query = $this->db->get_where('tbl_habitaciones', array('id_hotel' => $id_hotel, 'id_status_general' => 1));
        return $query->result();
    }
    function actualiza_habitacion($id_habitacion,$datos){
        $this->db->where("id_habitacion",$id_habitacion);
        $this->db->update("tbl_habitaciones",$datos);
		return $this->db->affected_rows();
    }

    function guardar_habitacion($datos){
        $this->db->insert('tbl_habitaciones',$datos);
        return $this->db->affected_rows();
    }

     function eliminarHabitacion($id_habitacion){
        $this->db->where("id_habitacion",$id_habitacion);
        $this->db->delete("tbl_habitaciones");
		return $this->db->affected_rows();
    }



    function get_hoteles(){
        	$query_l = "SELECT 
                     	h.*
                     	FROM  tbl_hoteles h";
        	$query = $this->db->query($query_l);
			return $query->result();
    }
   
    function get_habitaciones(){
        	$query_l = "SELECT 
                     	p.*, h.nombre_hotel 
                     	FROM  tbl_habitaciones p
                       	INNER JOIN tbl_hoteles h ON p.id_hotel = h.id_hotel";
        	$query = $this->db->query($query_l);
			return $query->result();
    }

}
?>
