<?php
class hoteles_model extends CI_Model {
    
    function __construct(){
        $this->load->database();
    }
    
    function get_hotel($id_hotel){
		if ($id_hotel === FALSE){
			return FALSE;
		}
		$query = $this->db->get_where('tbl_hoteles', array('id_hotel' => $id_hotel));
		return $query->row_array();
    }

    function get_regiones(){
		$query = $this->db->get('tbl_regiones');
		return $query->result();
    }

    function get_destinos(){
		$query = $this->db->get('tbl_destinos');
		return $query->result();
    }

    function actualiza_hotel($id_hotel,$datos){
        $this->db->where("id_hotel",$id_hotel);
        $this->db->update("tbl_hoteles",$datos);
		return $this->db->affected_rows();
    }

    function guardar_hotel($datos){
        $this->db->insert('tbl_hoteles',$datos);
        return $this->db->affected_rows();
    }

     function eliminarHotel($id_hotel){
        $this->db->where("id_hotel",$id_hotel);
        $this->db->delete("tbl_hoteles");
		return $this->db->affected_rows();
    }

    function get_hoteles(){
		//if ($id_hotel === FALSE){
        	$query_l = "SELECT 
                     	h.*, r.descripcion as region, d.descripcion as destino 
                     	FROM  tbl_hoteles h
                       	INNER JOIN tbl_regiones r ON r.id_region = h.id_region
                       	INNER JOIN tbl_destinos d ON d.id_destino = h.id_destino
                       	INNER JOIN tbl_status_general s ON s.id_status_general = h.id_status_general";
        	$query = $this->db->query($query_l);
			//if ($query->num_rows > 0){
				return $query->result();
			/*}else{
				return FALSE;
			}*/
			
		//}

		/*$query_l = "SELECT 
                 	h.*, d.descripcion as region
                 	FROM  tbl_hoteles h
                   	INNER JOIN tbl_destinos d ON d.id_destino = h.id_destino
                   	INNER JOIN tbl_status_general s ON s.id_status_general = h.id_status_general
                   	WHERE h.id_hotel=".$id_hotel;
    	$query = $this->db->query($query_l);
		if ($query->num_rows > 0){
			return $query->row_array();
		}else{
			return FALSE;
		}*/
		
    }


    /*function insert_user($data){
        $this->db->insert('usuarios',$data);
        return $this->db->affected_rows();//return $this->db->insert_id();
    }
	
	function update_user($id_usuario,$datos){
        $this->db->where("id_usuario",$id_usuario);
        $this->db->update("usuarios",$datos);
		return $this->db->affected_rows();
    }	
	
	function get_users($id_usuario = FALSE){
		if ($id_usuario === FALSE){
        	 $query_l = "SELECT 
                     	u.*, ut.tipo, c.category, p.patron
                     FROM 
                       usuarios  u
                       INNER JOIN usuario_tipos ut ON ut.usuario_tipo = u.usuario_tipo
                       LEFT JOIN categorys c ON c.category_id = u.category_id
                       LEFT JOIN patrones p ON p.patron_id = u.patron_id";
        $query = $this->db->query($query_l);
			if ($query->num_rows > 0){
				return $query->result();
			}else{
				return FALSE;
			}
			
		}
		$query = $this->db->get_where('usuarios', array('id_usuario' => $id_usuario));
		return $query->row_array();
    }
	
	function get_tipos(){
		$query = $this->db->get_where('usuario_tipos', array('status' => 1));
		return $query->result();
    }
	
	function get_categorias(){
		$query = $this->db->get_where('categorys', array('status' => 1));
		return $query->result();
    }
	
	function get_patrones(){
		$query = $this->db->get_where('patrones', array('status' => 1));
		return $query->result();
    }
    
    function eliminar_user($id_usuario){
        $this->db->where("id_usuario",$id_usuario);
        $this->db->delete("usuarios");
		return $this->db->affected_rows();
    }*/
	
		
	
	
    
    
}
?>
