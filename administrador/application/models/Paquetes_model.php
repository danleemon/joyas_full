<?php
class paquetes_model extends CI_Model {
    
    function __construct(){
        $this->load->database();
    }
    
    function get_paquete($id_paquete){
		if ($id_paquete === FALSE){
			return FALSE;
		}
		$query = $this->db->get_where('tbl_paquetes', array('id_paquete' => $id_paquete));
		return $query->row_array();
    }

    function actualiza_paquete($id_paquete,$datos){
        $this->db->where("id_paquete",$id_paquete);
        $this->db->update("tbl_paquetes",$datos);
		return $this->db->affected_rows();
    }

    function guardar_paquete($datos){
        $this->db->insert('tbl_paquetes',$datos);
        return $this->db->affected_rows();
    }

     function eliminarPaquete($id_paquete){
        $this->db->where("id_paquete",$id_paquete);
        $this->db->delete("tbl_paquetes");
		return $this->db->affected_rows();
    }

    function get_hoteles(){
        	$query_l = "SELECT 
                     	h.*
                     	FROM  tbl_hoteles h";
        	$query = $this->db->query($query_l);
			return $query->result();
    }
    function get_monedas(){
        	$query_l = "SELECT 
                     	m.*
                     	FROM  tbl_monedas m";
        	$query = $this->db->query($query_l);
			return $query->result();
    }


    function get_paquetes(){
        	$query_l = "SELECT 
                     	p.*, h.nombre_hotel, m.descripcion as nombre_moneda 
                     	FROM  tbl_paquetes p
                       	INNER JOIN tbl_hoteles h ON p.id_hotel = h.id_hotel
                       	INNER JOIN tbl_monedas m ON m.id_moneda = p.id_moneda";
        	$query = $this->db->query($query_l);
			return $query->result();
    }

}
?>
