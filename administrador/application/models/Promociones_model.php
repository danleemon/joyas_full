<?php
class promociones_model extends CI_Model {
    
    function __construct(){
        $this->load->database();
    }


    function get_promos_especiales(){
            $query_l = "SELECT 
                        p.*, a.nombre_paquete
                        FROM  tbl_promos_home p
                        INNER JOIN tbl_paquetes a 
                        ON p.id_paquete=a.id_paquete";
            $query = $this->db->query($query_l);
            return $query->result();
    }



    
    function get_promocion($id_promo_especial){
		if ($id_promo_especial === FALSE){
			return FALSE;
		}
		$query = $this->db->get_where('tbl_promos_home', array('id_promo_especial' => $id_promo_especial));
		return $query->row_array();
    }

    function actualiza_promocion_especial($id_paquete,$datos){
        $this->db->where("id_promo_especial",$id_paquete);
        $this->db->update("tbl_promos_home",$datos);
		return $this->db->affected_rows();
    }

    function guardar_paquete($datos){
        $this->db->insert('tbl_paquetes',$datos);
        return $this->db->affected_rows();
    }

    function guardar_promo($datos){
        $this->db->insert('tbl_promos_home',$datos);
        return $this->db->affected_rows();
    }

     function eliminarPaquete($id_paquete){
        $this->db->where("id_paquete",$id_paquete);
        $this->db->delete("tbl_paquetes");
		return $this->db->affected_rows();
    }

    function get_hoteles(){
        	$query_l = "SELECT 
                     	h.*
                     	FROM  tbl_hoteles h";
        	$query = $this->db->query($query_l);
			return $query->result();
    }
    function get_monedas(){
        	$query_l = "SELECT 
                     	m.*
                     	FROM  tbl_monedas m";
        	$query = $this->db->query($query_l);
			return $query->result();
    }


    function get_paquetes(){
        	$query_l = "SELECT 
                     	p.*, h.nombre_hotel, m.descripcion as nombre_moneda 
                     	FROM  tbl_paquetes p
                       	INNER JOIN tbl_hoteles h ON p.id_hotel = h.id_hotel
                       	INNER JOIN tbl_monedas m ON m.id_moneda = p.id_moneda";
        	$query = $this->db->query($query_l);
			return $query->result();
    }

}
?>
