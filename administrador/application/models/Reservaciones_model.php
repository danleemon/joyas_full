<?php
class reservaciones_model extends CI_Model {
    
    function __construct(){
        $this->load->database();
    }
    
    function get_reservacion($id_reservacion){
		if ($id_reservacion === FALSE){
			return FALSE;
		}
		$query_l = "SELECT 
                        r.*, s.*, h.nombre_hotel, p.id_paquete, p.nombre_paquete, sr.descripcion as status_reservacion
                        FROM  tbl_reservaciones r
                        INNER JOIN tbl_solicitudes s ON s.clave_reservacion = r.clave_reservacion
                        INNER JOIN tbl_status_reservaciones sr ON sr.id_status_reservacion = r.id_status_reservacion
                        INNER JOIN tbl_paquetes p ON s.id_paquete = p.id_paquete
                        INNER JOIN tbl_hoteles h ON p.id_hotel = h.id_hotel
                        WHERE r.id_reservacion=".$id_reservacion;
        $query = $this->db->query($query_l);
		return $query->row_array();
    }

    function actualiza_reservacion($id_reservacion,$datos){
        $this->db->where("id_reservacion",$id_reservacion);
        $this->db->update("tbl_reservaciones",$datos);
		return $this->db->affected_rows();
    }

    function guardar_paquete($datos){
        $this->db->insert('tbl_paquetes',$datos);
        return $this->db->affected_rows();
    }

     function eliminarPaquete($id_paquete){
        $this->db->where("id_paquete",$id_paquete);
        $this->db->delete("tbl_paquetes");
		return $this->db->affected_rows();
    }

    function get_hoteles(){
        	$query_l = "SELECT 
                     	h.*
                     	FROM  tbl_hoteles h";
        	$query = $this->db->query($query_l);
			return $query->result();
    }
    function get_monedas(){
        	$query_l = "SELECT 
                     	m.*
                     	FROM  tbl_monedas m";
        	$query = $this->db->query($query_l);
			return $query->result();
    }
     function get_tipos_habitacion(){
            $query_l = "SELECT 
                        h.*
                        FROM  tbl_habitaciones h";
            $query = $this->db->query($query_l);
            return $query->result();
    }
    function get_tipos_tarjeta(){
            $query_l = "SELECT 
                        t.*
                        FROM  tbl_tipos_tarjeta t";
            $query = $this->db->query($query_l);
            return $query->result();
    }
    function get_paquetes(){
            $query_l = "SELECT 
                        p.*
                        FROM  tbl_paquetes p";
            $query = $this->db->query($query_l);
            return $query->result();
    }
    function get_status_reservaciones(){
            $query_l = "SELECT 
                        s.*
                        FROM  tbl_status_reservaciones s";
            $query = $this->db->query($query_l);
            return $query->result();
    }
    


    function get_reservaciones(){
        	$query_l = "SELECT 
                     	r.*, s.*, h.nombre_hotel, p.id_paquete, p.nombre_paquete, sr.descripcion as status_reservacion
                     	FROM  tbl_reservaciones r
                        INNER JOIN tbl_solicitudes s ON s.clave_reservacion = r.clave_reservacion
                        INNER JOIN tbl_status_reservaciones sr ON sr.id_status_reservacion = r.id_status_reservacion
                        INNER JOIN tbl_paquetes p ON s.id_paquete = p.id_paquete
                       	INNER JOIN tbl_hoteles h ON p.id_hotel = h.id_hotel";
        	$query = $this->db->query($query_l);
			return $query->result();
    }

}
?>
