<?php
class solicitudes_model extends CI_Model {
    
    function __construct(){
        $this->load->database();
    }
    
    function get_solicitud($id_solicitud){
		if ($id_solicitud === FALSE){
			return FALSE;
		}
		$query = $this->db->get_where('tbl_solicitudes', array('id_solicitud' => $id_solicitud));
		return $query->row_array();
    }

    function actualiza_solicitud($id_solicitud,$datos){
        $this->db->where("id_solicitud",$id_solicitud);
        $this->db->update("tbl_solicitudes",$datos);
		return $this->db->affected_rows();
    }

    function guardar_solicitud($datos){
        $this->db->insert('tbl_solicitud',$datos);
        return $this->db->affected_rows();
    }

    function guardar_reservacion($datos){
        $this->db->insert('tbl_reservaciones',$datos);
        return $this->db->affected_rows();
    }

     function eliminarSolicitud($id_solicitud){
        $this->db->where("id_solicitud",$id_solicitud);
        $this->db->delete("tbl_solicitudes");
		return $this->db->affected_rows();
    }

	function get_paquetes(){
        	$query_l = "SELECT 
                     	p.*
                     	FROM  tbl_paquetes p";
        	$query = $this->db->query($query_l);
			return $query->result();
    }
    function get_tipos_habitacion(){
        	$query_l = "SELECT 
                     	h.*
                     	FROM  tbl_habitaciones h";
        	$query = $this->db->query($query_l);
			return $query->result();
    }
    function get_tipos_tarjeta(){
        	$query_l = "SELECT 
                     	t.*
                     	FROM  tbl_tipos_tarjeta t";
        	$query = $this->db->query($query_l);
			return $query->result();
    }
    function get_status_solicitudes(){
        	$query_l = "SELECT 
                     	s.*
                     	FROM  tbl_status_solicitudes s";
        	$query = $this->db->query($query_l);
			return $query->result();
    }



    function get_solicitudes(){
        	$query_l = "SELECT 
                     	so.*, p.nombre_paquete, h.nombre_habitacion as tipo_habitacion, t.descripcion as tipo_tarjeta, s.descripcion as status_solicitud, ho.nombre_hotel 
                     	FROM  tbl_solicitudes so
                       	INNER JOIN tbl_paquetes p ON p.id_paquete = so.id_paquete
                       	INNER JOIN tbl_hoteles ho ON p.id_hotel = ho.id_hotel
                       	INNER JOIN tbl_habitaciones h ON h.id_habitacion = so.id_tipo_habitacion
                       	INNER JOIN tbl_tipos_tarjeta t ON t.id_tipo_tarjeta = so.id_tipo_tarjeta
                       	INNER JOIN tbl_status_solicitudes s ON s.id_status_solicitud = so.id_status_solicitud";
        	$query = $this->db->query($query_l);
			return $query->result();
    }


    function get_reservacion_clave($clave_reservacion){
        if ($clave_reservacion === FALSE){
            return FALSE;
        }
        $query_l = "SELECT 
                        r.*, s.*, h.nombre_hotel, h.id_hotel, h.imagen_mapa, h.direccion_hotel, p.precio, p.id_moneda, p.incluye_impuestos, p.tipo_cargo, p.iva, p.ish, p.cargo_servicio, p.id_paquete, p.nombre_paquete, sr.descripcion as status_reservacion, hab.nombre_habitacion 
                        FROM  tbl_reservaciones r
                        INNER JOIN tbl_solicitudes s ON s.clave_reservacion = r.clave_reservacion
                        INNER JOIN tbl_status_reservaciones sr ON sr.id_status_reservacion = r.id_status_reservacion
                        INNER JOIN tbl_paquetes p ON s.id_paquete = p.id_paquete
                        INNER JOIN tbl_hoteles h ON p.id_hotel = h.id_hotel
                        INNER JOIN tbl_habitaciones hab ON p.id_habitacion = hab.id_habitacion
                        WHERE r.clave_reservacion=".$clave_reservacion;
        $query = $this->db->query($query_l);
        return $query->row_array();
    }

}
?>
