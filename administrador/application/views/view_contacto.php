<?php 
$visible='';
$editable='';
if($this->session->userdata('perfil')=='vista'){
	$editable=' disabled="disabled" ';
	$visible=' style="display:none" ';

}
?>
<div class="row">
	<div class="col-md-12">
		<div class="block-flat">
			<div class="header">							
				<h3><i class="fa fa-users"></i> <strong>Contacto</strong></h3>
			</div>
			<div class="content">
				<div class="table-responsive" style="overflow-x: none;">
					<table class="table table-bordered" id="datatable_hoteles" >
						<thead>
							<tr>
								
								<th>Nombre</th>
								<th>Correo</th>
								<th>Teléfono</th>
								<th>Fecha de registro</th>
								<th>Estatus</th>
								<?php if($this->session->userdata('perfil')!='vista'){ ?>
								<th>Acciones</th>
								<?php } ?>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($contacto_result as $contacto_row){ ?>
								<tr class="odd gradeX">
									<td><?=$contacto_row->nombre_contacto?></td>
									<td><?=$contacto_row->correo_contacto?></td>
									<td><?=$contacto_row->telefono_contacto?></td>
									<td><?=$contacto_row->fecha_registro?></td>
									<td><?=$contacto_row->status_contacto?></td>
									<?php if($this->session->userdata('perfil')!='vista'){ ?>
									<td style="text-align:center">
										<button type="button" title="Ver detalle de contacto" class="btn btn-primary btn-sm" data-dismiss="modal" onclick="verSolicitud(<?=$contacto_row->id_contacto?>);"><span class="glyphicon glyphicon-search"></span> </button>
									</td>
									<?php } ?>
								</tr>
							<?php }?>
										
						</tbody>
					</table>			
				</div>

			</div>
		</div>				
	</div>
</div>


<!-- MODAL EDITAR PAQUETE -->
 <div class="modal fade" id="ModalEditUser" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="padding:15px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4><span class="glyphicon glyphicon-pencil"></span> Contacto</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">

			<form role="form" id="frmHotelEdit" name="frmHotelEdit" action="contacto/actualizarContacto" method="POST" enctype="multipart/form-data"  target="ifrm_update">

				<input type="hidden" name="id_contacto_edit" id="id_contacto_edit" value="0" />

				<div class="form-group">
					<h5 style="font-family: 'Open Sans', sans-serif;font-weight: 500;border-bottom: 1px solid #dadada;font-size: 16px;padding-bottom: 20px;">Datos de contacto</h5>
				</div>
              
	            
	            <div class="form-group">
	              <label for="nombre">Nombre*</label><input type="text"  maxlength="50" class="form-control" placeholder="Nombre" id="nombre_contacto_edit" name="nombre_contacto_edit">
	            </div>

	            <div class="form-group">
	             <label for="nombre">Correo*</label><input type="text"  maxlength="50" class="form-control" placeholder="Correo" id="correo_contacto_edit" name="correo_contacto_edit">
	            </div>

	            <div class="form-group">
					<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">Fecha de nacimiento*</label>
	              		<input type="text" maxlength="50"  class="form-control" placeholder="Fecha de nacimiento" id="nacimiento_contacto_edit" name="nacimiento_contacto_edit">
					</div>
					<div style="width:45%;float: left;">
	              		<label for="nombre">Género*</label>
	              		<select class="form-control" id="id_status_factura_edit" name="genero_contacto_edit">
	                        <option value="M"> Masculino</option>
	                        <option value="F"> Femenino</option>
	                    </select>
					</div>
	            </div>

	            <div class="form-group">
						<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">Teléfono*</label><input type="text" maxlength="50"  class="form-control" placeholder="RFC" id="telefono_contacto_edit" name="telefono_contacto_edit">
					</div>
						<div style="width:45%;float: left;">
	              		<label for="nombre">Estatus de contacto*</label>
	              		<select class="form-control" id="id_status_contacto_edit" name="id_status_contacto_edit">
	                        <option value=""> Seleccione una opción</option>
	                        <?php foreach($status_solicitud as $estatus){ ?>
	                        <option value="<?=$estatus->id_status_contacto?>"><?=$estatus->descripcion?></option>
	                        <?php } ?>
	                    </select>
						</div>
	            </div>

	            
            
	           

	            <div class="form-group">
						<label for="nombre">Comentarios*</label>
						<textarea class="form-control" rows="6" id="comentarios_contacto_edit" name="comentarios_contacto_edit" style="resize: none;"></textarea>
	            </div>


	             <div class="form-group">
						<label for="nombre">Respuesta*</label>
						<textarea class="form-control" rows="6" id="respuesta_contacto_edit" name="respuesta_contacto_edit" style="resize: none;"></textarea>
	            </div>


		  	</form>
		  	<iframe id="ifrm_update" name="ifrm_update" style="display: none;"></iframe>

        </div>
        <div class="modal-footer">
        	<button title="Enviar por correo factura en formato PDF y XML" type="button" class="btn btn-success pull-left" onclick="enviarRespuesta()"><span class="glyphicon glyphicon-send"></span> Enviar respuesta por correo</button>

        	<button type="button" class="btn btn-danger pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
			<button type="button" class="btn btn-success pull-right" onclick="actualizarContacto()"><span class="glyphicon glyphicon-floppy-disk"></span> Actualizar contacto</button>
		  
		  <span id="msg_validacion"></span>
        </div>
      </div>
    </div>
  </div>

			
<script>

function actualizarContacto(){
	$("#frmHotelEdit").submit();
}

$(document).ready(function(){

	$("#ifrm_update").load(function(){
	   var ifrAddS = $("#ifrm_update").contents().find("#div_resultado").html();
	   if(ifrAddS == 1){
	   		 $.gritter.add({
				title: 'Actualizar contacto',
				text: 'Se actualizó correctamente el contacto.',
		        class_name: 'success'
			 });
	         setTimeout ("window.location.href = 'contacto';", 2000);
	   }
	   
	});


	
});

function verSolicitud(id) {

        $.ajax({
            url: "contacto/recuperarContacto",
            data: {id_contacto: id},
            type: "POST",
            async: 'true',
            success: function(response) {
				var dat = jQuery.parseJSON(response);
				$("#id_contacto_edit").val(dat.id_contacto);
				$("#nombre_contacto_edit").val(dat.nombre_contacto);
                $("#nacimiento_contacto_edit").val(dat.nacimiento_contacto);
                $("#telefono_contacto_edit").val(dat.telefono_contacto);
                $("#correo_contacto_edit").val(dat.correo_contacto);
                $("#genero_contacto_edit").val(dat.genero_contacto);
                //$("#comentarios_contacto_edit").val(dat.comentarios_contacto);
                //$("#respuesta_contacto_edit").val(dat.respuesta_contacto);
                $("#id_status_contacto_edit").val(dat.id_status_contacto);


                tinyMCE.get('comentarios_contacto_edit').setContent( dat.comentarios_contacto );
                tinyMCE.get('respuesta_contacto_edit').setContent( dat.respuesta_contacto );
              
				$("#ModalEditUser").modal('show');
            },
            error: function(x, e) {
                $.gritter.add({
				        title: 'Ver contacto',
				        text: 'Ocurrio un error y no se pueden recuperar los datos de contacto, intentelo más tarde.',
				        class_name: 'danger'
				});
            }
        });
}




function enviarRespuesta() {
		id = $("#id_contacto_edit").val();
		correo_contacto_edit = $("#correo_contacto_edit").val();
		respuesta_contacto_edit = tinyMCE.get('respuesta_contacto_edit').getContent();



		mail_ = $("#correo_contacto_edit").val();
        $.ajax({
            url: "contacto/enviarMailContacto",
            data: {id_contacto: id, respuesta_contacto: respuesta_contacto_edit , correo_contacto:correo_contacto_edit },
            type: "POST",
            async: 'true',
            success: function(response) {
				if(response==1){
					$.gritter.add({
						title: 'Enviar respuesta',
						text: 'Se envió exitosamente la respuesta a '+mail_,
				        class_name: 'success'
					 });
					$("#ModalEditUser").modal('hide');
				}else{
					$.gritter.add({
						title: 'Enviar respuesta',
						text: 'Ocurrio un error y no se puedo enviar la respuesta, por favor intentelo más tarde.',
				        class_name: 'danger'
					 });
				}
            },
            error: function(x, e) {
                $.gritter.add({
				        title: 'Enviar respuesta',
				        text: 'Ocurrio un error y no se puedo enviar la respuesta, por favor intentelo más tarde.',
				        class_name: 'danger'
				});
            }
        });
}
</script>
