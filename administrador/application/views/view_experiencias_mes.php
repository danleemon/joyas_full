<?php 
$visible='';
$editable='';
if($this->session->userdata('perfil')=='vista'){
	$editable=' disabled="disabled" ';
	$visible=' style="display:none" ';

}
?>
<div class="row">
	<div class="col-md-12">
		<div class="block-flat">
			<div class="header">							
				<h3><i class="fa fa-tags"></i> <strong>Experiencias del mes</strong></h3>
			</div>
			<div class="content">
				<div class="table-responsive" style="overflow-x: none;">
					<table class="table table-bordered" id="datatable_hoteles" >
						<thead>
							<tr>
								<th>Hotel</th>
								<th>Nombre</th>
								<th>Precio</th>
								<th>Fecha de registro</th>
								
								<?php if($this->session->userdata('perfil')!='vista'){ ?>
								<th>Visible</th>
								<th>Acciones</th>
								<?php } ?>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($paquetes_result as $paquete_row){ ?>
								<tr class="odd gradeX">
									<td><?=$paquete_row->nombre_hotel?></td>
									<td><?=$paquete_row->nombre_paquete?></td>
									<td>$<?=$paquete_row->precio?> <?=$paquete_row->nombre_moneda?></td>
									<td><?=$paquete_row->fecha_registro?></td>
									<?php if($this->session->userdata('perfil')!='vista'){ ?>
									<td style="text-align:center"><input type="checkbox" name="my-checkbox" id="<?=$paquete_row->id_paquete?>" <?php if($paquete_row->id_status_general == 1){ echo "checked";}?>></td>
									<td style="text-align:center">
										<button type="button" title="Editar paquete" class="btn btn-primary btn-sm" data-dismiss="modal" onclick="editarPaquete(<?=$paquete_row->id_paquete?>);"><span class="glyphicon glyphicon-pencil"></span> </button>
										<button type="button" title="Eliminar paquete" class="btn btn-danger btn-sm" data-dismiss="modal" onclick="eliminarPaquete(<?=$paquete_row->id_paquete?>, '<?=$paquete_row->nombre_paquete?>');"><span class="glyphicon glyphicon-trash"></span> </button>
									</td>
									<?php } ?>
								</tr>
							<?php }?>
										
						</tbody>
					</table>			
				</div>

				<button type="button" title="Agregar nueva experiencia" class="btn btn-info btn-sm" data-dismiss="modal" onclick="agregarPaquete();"><span class="glyphicon glyphicon-plus"></span> Agregar nueva experiencia</button>

			</div>
		</div>				
	</div>
</div>


<!-- MODAL EDITAR PAQUETE -->
 <div class="modal fade" id="ModalEditUser" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="padding:15px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4><span class="glyphicon glyphicon-pencil"></span> Editar experiencia</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">

			<form role="form" id="frmHotelEdit" name="frmHotelEdit" action="actualizarPaquete" method="POST" enctype="multipart/form-data"  target="ifrm_update">

				<input type="hidden" name="id_paquete_edit" id="id_paquete_edit" value="0" />

				<div class="form-group">
					<h5 style="font-family: 'Open Sans', sans-serif;font-weight: 500;border-bottom: 1px solid #dadada;font-size: 16px;padding-bottom: 20px;">Datos generales</h5>
				</div>
              
	            <div class="form-group">
	              <label for="nombre">Nombre de la experiencia*</label><input type="text"  maxlength="50" class="form-control" placeholder="Nombre del paquete" id="nombre_paquete_edit" name="nombre_paquete_edit">
	            </div>


	            <div class="form-group">
						<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">Layout*</label>
	              		<select class="form-control" id="layout_edit" name="layout_edit">
	                        <option value="0"> Seleccione una opción</option>
	                        <option value="1"> Completo</option>
	                        <option value="2"> Columnas</option>
	                        
	                    </select>
						</div>
						<div style="width:45%;float: left;">
	              		<label for="nombre">Orden*</label>
	              		<input type="text" maxlength="2"  class="form-control onlynumbers" placeholder="Orden" id="orden_edit" name="orden_edit">
						</div>
	            </div>



	            <div class="form-group">
						<div style="float: left;margin-right: 5%;width: 50%;">

							<label for="nombre">Hotel*</label>
		              		<select class="form-control" id="id_hotel_edit" name="id_hotel_edit">
		                        <option value="0"> Seleccione una opción</option>
		                        <?php foreach($hoteles as $hotel){ ?>
		                        <option value="<?=$hotel->id_hotel?>"><?=$hotel->nombre_hotel?></option>
		                        <?php } ?>
		                    </select>
						</div>
						<div style="width:45%;float: left;">
	              			<label for="nombre">Destino</label>
	              			<input type="text" maxlength="50"  class="form-control" placeholder="Destino" id="destino_edit" name="destino_edit">
						</div>
	            </div>




	            <div class="form-group">
						<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">Tipo de habitación*</label>
	              		<select class="form-control" id="id_habitacion_edit" name="id_habitacion_edit">
	                        <option value="0"> Seleccione una opción</option>
	                    </select>
						</div>
						<div style="width:45%;float: left;">
						<label for="nombre">Aviso de disponibilidad*</label>
		              	<select class="form-control" id="id_disponibilidad_edit" name="id_disponibilidad_edit">
		              		<option value="0">Habitaciones disponibles</option>
		              		<option value="1">Últimas habitaciones</option>
		              		<option value="2">Habitaciones agotadas</option>
		              	</select>

						
						</div>
	            </div>

	            <div class="form-group">
						<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">Tipo de tarifa*</label>
	              		<select class="form-control" id="tipo_tarifa_edit" name="tipo_tarifa_edit">
	                        <option value="0"> Seleccione una opción</option>
	                        <option value="1"> Por paquete</option>
	                        <option value="2"> Por habitación</option>
	                    </select>
						</div>
						<div style="width:45%;float: left;">
	              		<label for="nombre">Tipo de ocupación*</label>
		              	<select class="form-control" id="ocupacion_edit" name="ocupacion_edit">
		              		<option value="0"> Seleccione una opción</option>
		              		<option value="1">Sencilla</option>
		              		<option value="2">Doble</option>
		              	</select>
						</div>
	            </div>


	            <div class="form-group">
						<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">Habitaciones disponibles</label>
	              		<input type="text" maxlength="50"  class="form-control" placeholder="Habitaciones disponibles" id="numero_habitaciones_edit" name="numero_habitaciones_edit">
						</div>
						<div style="width:45%;float: left;">
						<!--label for="nombre">Contempla menores*</label>
	              		<select class="form-control" id="contempla_menores_edit" name="contempla_menores_edit">
		              		<option value="0"> Seleccione una opción</option>
		              		<option value="1">Sí</option>
		              		<option value="2">No</option>
		              	</select-->
		              	<label for="nombre">Duración (días)</label>
	              		<input type="text" maxlength="2"  class="form-control" placeholder="Días que dura el paquete" id="dias_edit" name="dias_edit">
	              		
						</div>
	            </div>

	             <div class="form-group">
						<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">Adultos admitidos</label>
	              		<input type="text" maxlength="50"  class="form-control" placeholder="Adultos admitidos por paquete" id="personas_edit" name="personas_edit">
						</div>
						<div style="width:45%;float: left;">
		              	<label for="nombre">Menores admitidos</label>
	              		<input type="text" maxlength="2"  class="form-control" placeholder="Menores amitidos por paquete" id="menores_edit" name="menores_edit">
	              		
						</div>
	            </div>





	             





	            <div class="form-group">
						<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">Moneda*</label>
	              		<select class="form-control" id="id_moneda_edit" name="id_moneda_edit">
	                        <option value="0"> Seleccione una opción</option>
	                        <?php foreach($monedas as $moneda){ ?>
	                        <option value="<?=$moneda->id_moneda?>"><?=$moneda->descripcion?></option>
	                        <?php } ?>
	                    </select>
						</div>
						<div style="width:45%;float: left;">
	              		<label for="nombre">Tarifa en dólares se cobra*</label>
	              		<select class="form-control" id="cargo_moneda_edit" name="cargo_moneda_edit">
	                        <option value="0"> Seleccione una opción</option>
	                        <option value="1">En pesos</option>
	                        <option value="2">En dólares</option>
	                    </select>
						</div>
	            </div>



	            



 				


            
	            <div class="form-group">
					<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">Precio*</label><input type="text" maxlength="50"  class="form-control" placeholder="Precio" id="precio_edit" name="precio_edit">
					</div>
					<div style="width:45%;float: left;">
	              		<label for="nombre">IVA*</label><input type="text"  maxlength="50" class="form-control" placeholder="IVA" id="iva_edit" name="iva_edit">
					</div>
	            </div>
	            <div class="form-group">
					<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">ISH*</label><input type="text" maxlength="50"  class="form-control" placeholder="ISH" id="ish_edit" name="ish_edit">
					</div>
					<div style="width:45%;float: left;">
	              		<label for="nombre">Cargo por servicio*</label><input type="text"  maxlength="50" class="form-control" placeholder="Cargo por servicio" id="cargo_servicio_edit" name="cargo_servicio_edit">
					</div>
	            </div>

	            <div class="form-group">
						<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">Tipo de cargo*</label>
	              		<select class="form-control" id="tipo_cargo_edit" name="tipo_cargo_edit">
	                        <option value="0"> Seleccione una opción</option>
	                        <option value="1">Fijo</option>
	                        <option value="2">Porcentual</option>
	                    </select>
						</div>
						<div style="width:45%;float: left;">
	              		<label for="nombre">Incluye impuestos*</label>
	              		<select class="form-control" id="incluye_impuestos_edit" name="incluye_impuestos_edit">
	                        <option value="0"> Seleccione una opción</option>
	                        <option value="1">Sí</option>
	                        <option value="2">No</option>
	                    </select>
						</div>
	            </div>


	            
	            <div class="form-group">
					<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">Inicio de vigencia*</label><input type="text" maxlength="50"  class="form-control" placeholder="Inicio de vigencia" id="vigencia_fin_edit" name="vigencia_fin_edit">
					</div>
					<div style="width:45%;float: left;">
	              		<label for="nombre">Fin de vigencia*</label><input type="text"  maxlength="50" class="form-control" placeholder="Fin de vigencia" id="vigencia_inicio_edit" name="vigencia_inicio_edit">
					</div>
	            </div>

	            <div class="form-group">
						<label for="nombre">Descripción*</label>
						<textarea class="form-control" rows="6" id="descripcion_edit" name="descripcion_edit" style="resize: none;"></textarea>
	            </div>


		  		<!-- IMAGEN DETALLE -->
				<div class="form-group">
	            	<label>Imagen:</label><br/>
	            	<img src="" id="input_imagen_img" style="width:195px;max-width:100%;padding-bottom: 10px;display:none"><br/>
		          	<div class="controls span3">
		          		<span class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar nueva imagen <input type="file" id="input_imagen_edit" name ="input_imagen_edit"/>
						</span>
						<br/>
						<i id="home_mini">No se ha seleccionado ninguna imagen</i><br/>
		            </div>
				</div>


				<!-- IMAGEN DETALLE -->
				<div class="form-group">
	            	<label>Imagen 1:</label><br/>
	            	<img src="" id="input_imagen_a_img" style="width:195px;max-width:100%;padding-bottom: 10px;display:none"><br/>
		          	<div class="controls span3">
		          		<span class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar nueva imagen <input type="file" id="input_imagen_a_edit" name ="input_imagen_a_edit"/>
						</span>
						<br/>
						<i id="home_imagen_a">No se ha seleccionado ninguna imagen</i><br/>
		            </div>
				</div>
				<!-- IMAGEN DETALLE -->
				<div class="form-group">
	            	<label>Imagen 2:</label><br/>
	            	<img src="" id="input_imagen_b_img" style="width:195px;max-width:100%;padding-bottom: 10px;display:none"><br/>
		          	<div class="controls span3">
		          		<span class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar nueva imagen <input type="file" id="input_imagen_b_edit" name ="input_imagen_b_edit"/>
						</span>
						<br/>
						<i id="home_imagen_b">No se ha seleccionado ninguna imagen</i><br/>
		            </div>
				</div>
				<!-- IMAGEN DETALLE -->
				<div class="form-group">
	            	<label>Imagen 3:</label><br/>
	            	<img src="" id="input_imagen_c_img" style="width:195px;max-width:100%;padding-bottom: 10px;display:none"><br/>
		          	<div class="controls span3">
		          		<span class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar nueva imagen <input type="file" id="input_imagen_c_edit" name ="input_imagen_c_edit"/>
						</span>
						<br/>
						<i id="home_imagen_c">No se ha seleccionado ninguna imagen</i><br/>
		            </div>
				</div>
				<!-- IMAGEN DETALLE -->
				<div class="form-group">
	            	<label>Imagen 4:</label><br/>
	            	<img src="" id="input_imagen_d_img" style="width:195px;max-width:100%;padding-bottom: 10px;display:none"><br/>
		          	<div class="controls span3">
		          		<span class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar nueva imagen <input type="file" id="input_imagen_d_edit" name ="input_imagen_d_edit"/>
						</span>
						<br/>
						<i id="home_imagen_d">No se ha seleccionado ninguna imagen</i><br/>
		            </div>
				</div>
				<!-- IMAGEN DETALLE -->
				<div class="form-group">
	            	<label>Imagen 5:</label><br/>
	            	<img src="" id="input_imagen_e_img" style="width:195px;max-width:100%;padding-bottom: 10px;display:none"><br/>
		          	<div class="controls span3">
		          		<span class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar nueva imagen <input type="file" id="input_imagen_e_edit" name ="input_imagen_e_edit"/>
						</span>
						<br/>
						<i id="home_imagen_e">No se ha seleccionado ninguna imagen</i><br/>
		            </div>
				</div>


				<!-- IMAGEN DETALLE -->
				<div class="form-group">
	            	<label>Imagen:</label><br/>
	            	<img src="" id="input_imagen_mini_img" style="width:195px;max-width:100%;padding-bottom: 10px;display:none"><br/>
		          	<div class="controls span3">
		          		<span class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar nueva imagen <input type="file" id="input_imagen_mini_edit" name ="input_imagen_mini_edit"/>
						</span>
						<br/>
						<i id="home_mini_mini">No se ha seleccionado ninguna imagen</i><br/>
		            </div>
				</div>


				<!-- IMAGEN DETALLE -->
				<div class="form-group">
	            	<label>Imagen 1:</label><br/>
	            	<img src="" id="input_imagen_mini_a_img" style="width:195px;max-width:100%;padding-bottom: 10px;display:none"><br/>
		          	<div class="controls span3">
		          		<span class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar nueva imagen <input type="file" id="input_imagen_mini_a_edit" name ="input_imagen_mini_a_edit"/>
						</span>
						<br/>
						<i id="home_imagen_mini_a">No se ha seleccionado ninguna imagen</i><br/>
		            </div>
				</div>
				<!-- IMAGEN DETALLE -->
				<div class="form-group">
	            	<label>Imagen 2:</label><br/>
	            	<img src="" id="input_imagen_mini_b_img" style="width:195px;max-width:100%;padding-bottom: 10px;display:none"><br/>
		          	<div class="controls span3">
		          		<span class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar nueva imagen <input type="file" id="input_imagen_mini_b_edit" name ="input_imagen_mini_b_edit"/>
						</span>
						<br/>
						<i id="home_imagen_mini_b">No se ha seleccionado ninguna imagen</i><br/>
		            </div>
				</div>
				<!-- IMAGEN DETALLE -->
				<div class="form-group">
	            	<label>Imagen 3:</label><br/>
	            	<img src="" id="input_imagen_mini_c_img" style="width:195px;max-width:100%;padding-bottom: 10px;display:none"><br/>
		          	<div class="controls span3">
		          		<span class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar nueva imagen <input type="file" id="input_imagen_mini_c_edit" name ="input_imagen_mini_c_edit"/>
						</span>
						<br/>
						<i id="home_imagen_mini_c">No se ha seleccionado ninguna imagen</i><br/>
		            </div>
				</div>
				<!-- IMAGEN DETALLE -->
				<div class="form-group">
	            	<label>Imagen 4:</label><br/>
	            	<img src="" id="input_imagen_mini_d_img" style="width:195px;max-width:100%;padding-bottom: 10px;display:none"><br/>
		          	<div class="controls span3">
		          		<span class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar nueva imagen <input type="file" id="input_imagen_mini_d_edit" name ="input_imagen_mini_d_edit"/>
						</span>
						<br/>
						<i id="home_imagen_mini_d">No se ha seleccionado ninguna imagen</i><br/>
		            </div>
				</div>
				<!-- IMAGEN DETALLE -->
				<div class="form-group">
	            	<label>Imagen 5:</label><br/>
	            	<img src="" id="input_imagen_mini_e_img" style="width:195px;max-width:100%;padding-bottom: 10px;display:none"><br/>
		          	<div class="controls span3">
		          		<span class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar nueva imagen <input type="file" id="input_imagen_mini_e_edit" name ="input_imagen_mini_e_edit"/>
						</span>
						<br/>
						<i id="home_imagen_mini_e">No se ha seleccionado ninguna imagen</i><br/>
		            </div>
				</div>



		  	</form>
		  	<iframe id="ifrm_update" name="ifrm_update" style="display: none;"></iframe>

        </div>
        <div class="modal-footer">
        	<button type="button" class="btn btn-danger pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
			<button type="button" class="btn btn-success pull-right" onclick="actualizarPaquete()"><span class="glyphicon glyphicon-floppy-disk"></span> Actualizar experiencia</button>
		  
		  <span id="msg_validacion"></span>
        </div>
      </div>
    </div>
  </div>







<!-- MODAL AGREGAR HOTEL -->
 <div class="modal fade" id="ModalAgregarHotel" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="padding:15px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4><span class="glyphicon glyphicon-pencil"></span> Agregar experiencia</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">

			<form role="form" id="frmHotelAdd" name="frmHotelAdd" action="guardarPaquete" method="POST" enctype="multipart/form-data"  target="ifrm_add">

				<div class="form-group">
					<h5 style="font-family: 'Open Sans', sans-serif;font-weight: 500;border-bottom: 1px solid #dadada;font-size: 16px;padding-bottom: 20px;">Datos generales</h5>
				</div>
              
	            <div class="form-group">
	              <label for="nombre">Nombre de la experiencia*</label><input type="text"  maxlength="50" class="form-control" placeholder="Nombre del paquete" id="nombre_paquete_add" name="nombre_paquete_add">
	            </div>

	
				<div class="form-group">
						<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">Layout*</label>
	              		<select class="form-control" id="layout_add" name="layout_add">
	                        <option value="0"> Seleccione una opción</option>
	                        <option value="1"> Completo</option>
	                        <option value="2"> Columnas</option>
	                        
	                    </select>
						</div>
						<div style="width:45%;float: left;">
	              		<label for="nombre">Orden*</label>
	              		<input type="text" maxlength="2"  class="form-control onlynumbers" placeholder="Orden" id="orden_add" name="orden_add">
						</div>
	            </div>




	            <div class="form-group">
						<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">Hotel*</label>
	              		<select class="form-control" id="id_hotel_add" name="id_hotel_add">
	                        <option value="0"> Seleccione una opción</option>
	                        <?php foreach($hoteles as $hotel){ ?>
	                        <option value="<?=$hotel->id_hotel?>"><?=$hotel->nombre_hotel?></option>
	                        <?php } ?>
	                    </select>
						</div>
						<div style="width:45%;float: left;">
	              		<label for="nombre">Destino</label>
	              		<input type="text" maxlength="50"  class="form-control" placeholder="Destino" id="destino_add" name="destino_add">
						</div>
	            </div>

	            <div class="form-group">
						<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">Tipo de habitación*</label>
	              		<select class="form-control" id="id_habitacion_add" name="id_habitacion_add">
	                        <option value="0"> Seleccione una opción</option>
	                    </select>
						</div>
						<div style="width:45%;float: left;">
	              		<label for="nombre">Aviso de disponibilidad*</label>
		              	<select class="form-control" id="id_disponibilidad_add" name="id_disponibilidad_add">
		              		<option value="0">Habitaciones disponibles</option>
		              		<option value="1">Últimas habitaciones</option>
		              		<option value="2">Habitaciones agotadas</option>
		              	</select>
						</div>
	            </div>

	            <div class="form-group">
						<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">Tipo de tarifa*</label>
	              		<select class="form-control" id="tipo_tarifa_add" name="tipo_tarifa_add">
	                        <option value="0"> Seleccione una opción</option>
	                        <option value="1"> Por paquete</option>
	                        <option value="2"> Por habitación</option>
	                    </select>
						</div>
						<div style="width:45%;float: left;">
	              		<label for="nombre">Tipo de ocupación*</label>
		              	<select class="form-control" id="ocupacion_add" name="ocupacion_add">
		              		<option value="0"> Seleccione una opción</option>
		              		<option value="1">Sencilla</option>
		              		<option value="2">Doble</option>
		              	</select>
						</div>
	            </div>


	             <div class="form-group">
						<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">Habitaciones disponibles</label>
	              		<input type="text" maxlength="50"  class="form-control" placeholder="Habitaciones disponibles" id="numero_habitaciones_add" name="numero_habitaciones_add">
						</div>
						<div style="width:45%;float: left;">
		              	<label for="nombre">Duración (días)</label>
	              		<input type="text" maxlength="2"  class="form-control" placeholder="Días que dura el paquete" id="dias_add" name="dias_add">
	              		
						</div>
	            </div>

	             <div class="form-group">
						<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">Adultos admitidos</label>
	              		<input type="text" maxlength="50"  class="form-control" placeholder="Adultos admitidos por paquete" id="personas_add" name="personas_add">
						</div>
						<div style="width:45%;float: left;">
		              	<label for="nombre">Menores admitidos</label>
	              		<input type="text" maxlength="2"  class="form-control" placeholder="Menores amitidos por paquete" id="menores_add" name="menores_add">
	              		
						</div>
	            </div>


	            



	            <div class="form-group">
						<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">Moneda*</label>
	              		<select class="form-control" id="id_moneda_add" name="id_moneda_add">
	                        <option value="0"> Seleccione una opción</option>
	                        <?php foreach($monedas as $moneda){ ?>
	                        <option value="<?=$moneda->id_moneda?>"><?=$moneda->descripcion?></option>
	                        <?php } ?>
	                    </select>
						</div>
						<div style="width:45%;float: left;">
	              		<label for="nombre">Tarifa en dólares se cobra*</label>
	              		<select class="form-control" id="cargo_moneda_add" name="cargo_moneda_add">
	                        <option value="0"> Seleccione una opción</option>
	                        <option value="1">En pesos</option>
	                        <option value="2">En dólares</option>
	                    </select>
						</div>
	            </div>


 				

            
	            <div class="form-group">
					<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">Precio*</label><input type="text" maxlength="50"  class="form-control" placeholder="Precio" id="precio_add" name="precio_add">
					</div>
					<div style="width:45%;float: left;">
	              		<label for="nombre">IVA*</label><input type="text"  maxlength="50" class="form-control" placeholder="IVA" id="iva_add" name="iva_add">
					</div>
	            </div>
	            <div class="form-group">
					<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">ISH*</label><input type="text" maxlength="50"  class="form-control" placeholder="ISH" id="ish_add" name="ish_add">
					</div>
					<div style="width:45%;float: left;">
	              		<label for="nombre">Cargo por servicio*</label><input type="text"  maxlength="50" class="form-control" placeholder="Cargo por servicio" id="cargo_servicio_edit" name="cargo_servicio_add">
					</div>
	            </div>

	            <div class="form-group">
						<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">Tipo de cargo*</label>
	              		<select class="form-control" id="tipo_cargo_add" name="tipo_cargo_add">
	                        <option value="0"> Seleccione una opción</option>
	                        <option value="1">Fijo</option>
	                        <option value="2">Porcentual</option>
	                    </select>
						</div>
						<div style="width:45%;float: left;">
	              		<label for="nombre">Incluye impuestos*</label>
	              		<select class="form-control" id="incluye_impuestos_add" name="incluye_impuestos_add">
	                        <option value="0"> Seleccione una opción</option>
	                        <option value="1">Sí</option>
	                        <option value="2">No</option>
	                    </select>
						</div>
	            </div>

	            
	            <div class="form-group">
					<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">Inicio de vigencia*</label><input type="text" maxlength="50"  class="form-control" placeholder="Inicio de vigencia" id="vigencia_fin_add" name="vigencia_fin_add">
					</div>
					<div style="width:45%;float: left;">
	              		<label for="nombre">Fin de vigencia*</label><input type="text"  maxlength="50" class="form-control" placeholder="Fin de vigencia" id="vigencia_inicio_add" name="vigencia_inicio_add">
					</div>
	            </div>

	            <div class="form-group">
						<label for="nombre">Descripción*</label>
						<textarea class="form-control" rows="6" id="descripcion_add" name="descripcion_add" style="resize: none;"></textarea>
	            </div>


		  		<!-- IMAGEN DETALLE -->
				<div class="form-group">
	            	<label>Imagen principal:</label>
		          	<div class="controls span3">
		          		<span class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar nueva imagen <input type="file" id="input_imagen_add" name ="input_imagen_add"/>
						</span>
						<br/>
						<i id="home_mini_add">No se ha seleccionado ninguna imagen</i><br/>
		            </div>
				</div>



				<!-- IMAGEN DETALLE -->
				<div class="form-group">
	            	<label>Imagen 1:</label>
		          	<div class="controls span3">
		          		<span class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar nueva imagen <input type="file" id="input_imagen_a_add" name ="input_imagen_a_add"/>
						</span>
						<br/>
						<i id="imagen_a_add">No se ha seleccionado ninguna imagen</i><br/>
		            </div>
				</div>

				<!-- IMAGEN DETALLE -->
				<div class="form-group">
	            	<label>Imagen 2:</label>
		          	<div class="controls span3">
		          		<span class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar nueva imagen <input type="file" id="input_imagen_b_add" name ="input_imagen_b_add"/>
						</span>
						<br/>
						<i id="imagen_b_add">No se ha seleccionado ninguna imagen</i><br/>
		            </div>
				</div>

				<!-- IMAGEN DETALLE -->
				<div class="form-group">
	            	<label>Imagen 3:</label>
		          	<div class="controls span3">
		          		<span class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar nueva imagen <input type="file" id="input_imagen_c_add" name ="input_imagen_c_add"/>
						</span>
						<br/>
						<i id="imagen_c_add">No se ha seleccionado ninguna imagen</i><br/>
		            </div>
				</div>

				<!-- IMAGEN DETALLE -->
				<div class="form-group">
	            	<label>Imagen 4:</label>
		          	<div class="controls span3">
		          		<span class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar nueva imagen <input type="file" id="input_imagen_d_add" name ="input_imagen_d_add"/>
						</span>
						<br/>
						<i id="imagen_d_add">No se ha seleccionado ninguna imagen</i><br/>
		            </div>
				</div>

				<!-- IMAGEN DETALLE -->
				<div class="form-group">
	            	<label>Imagen 5:</label>
		          	<div class="controls span3">
		          		<span class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar nueva imagen <input type="file" id="input_imagen_e_add" name ="input_imagen_e_add"/>
						</span>
						<br/>
						<i id="imagen_e_add">No se ha seleccionado ninguna imagen</i><br/>
		            </div>
				</div>


				<!-- IMAGEN DETALLE -->
				<div class="form-group">
	            	<label>Imagen principal:</label>
		          	<div class="controls span3">
		          		<span class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar nueva imagen <input type="file" id="input_imagen_mini_add" name ="input_imagen_mini_add"/>
						</span>
						<br/>
						<i id="home_mini_mini_add">No se ha seleccionado ninguna imagen</i><br/>
		            </div>
				</div>



				<!-- IMAGEN DETALLE -->
				<div class="form-group">
	            	<label>Imagen 1:</label>
		          	<div class="controls span3">
		          		<span class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar nueva imagen <input type="file" id="input_imagen_mini_a_add" name ="input_imagen_mini_a_add"/>
						</span>
						<br/>
						<i id="imagen_mini_a_add">No se ha seleccionado ninguna imagen</i><br/>
		            </div>
				</div>

				<!-- IMAGEN DETALLE -->
				<div class="form-group">
	            	<label>Imagen 2:</label>
		          	<div class="controls span3">
		          		<span class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar nueva imagen <input type="file" id="input_imagen_mini_b_add" name ="input_imagen_mini_b_add"/>
						</span>
						<br/>
						<i id="imagen_mini_b_add">No se ha seleccionado ninguna imagen</i><br/>
		            </div>
				</div>

				<!-- IMAGEN DETALLE -->
				<div class="form-group">
	            	<label>Imagen 3:</label>
		          	<div class="controls span3">
		          		<span class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar nueva imagen <input type="file" id="input_imagen_mini_c_add" name ="input_imagen_mini_c_add"/>
						</span>
						<br/>
						<i id="imagen_mini_c_add">No se ha seleccionado ninguna imagen</i><br/>
		            </div>
				</div>

				<!-- IMAGEN DETALLE -->
				<div class="form-group">
	            	<label>Imagen 4:</label>
		          	<div class="controls span3">
		          		<span class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar nueva imagen <input type="file" id="input_imagen_mini_d_add" name ="input_imagen_mini_d_add"/>
						</span>
						<br/>
						<i id="imagen_mini_d_add">No se ha seleccionado ninguna imagen</i><br/>
		            </div>
				</div>

				<!-- IMAGEN DETALLE -->
				<div class="form-group">
	            	<label>Imagen 5:</label>
		          	<div class="controls span3">
		          		<span class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar nueva imagen <input type="file" id="input_imagen_mini_e_add" name ="input_imagen_mini_e_add"/>
						</span>
						<br/>
						<i id="imagen_mini_e_add">No se ha seleccionado ninguna imagen</i><br/>
		            </div>
				</div>




		  	</form>
		  	<iframe id="ifrm_add" name="ifrm_add" style="display: none;"></iframe>

        </div>
        <div class="modal-footer">
        	<button type="button" class="btn btn-danger pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
			<button type="button" class="btn btn-success pull-right" onclick="guardarPaquete()"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar experiencia</button>
		  
		  <span id="msg_validacion"></span>
        </div>
      </div>
    </div>
  </div>



<!-- MODAL BORRAR HOTEL -->
<div class="modal fade" id="DeleteUser" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="background-color:#df4b33;padding:15px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 style="background-color:#df4b33;"><span class="glyphicon glyphicon-exclamation-sign"></span> Eliminar paquete</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
        	<input type="hidden" name="id_paquete_borrar" id="id_paquete_borrar" value="0" />
			<form role="form">
          		<p >¿Confirma que desea eliminar el paquete <b class="form-subtitle" id="nombre_paquete_borrar"></b>?</p>
			</form>
										
        </div>
        <div class="modal-footer">
        	<button type="button" class="btn btn-danger pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
        	<button type="button" class="btn btn-danger pull-right" data-dismiss="modal" onclick="borrarPaquete();"><span class="glyphicon glyphicon-trash"></span> Eliminar</button>
        </div>
      </div>
	</div>
</div>

			
<script>

	$('input[name="my-checkbox"]').on('switchChange.bootstrapSwitch', function(event, state) {
		var target_id = $(this).attr('id');
		var status_ = 0;

		if(state){
			status_ = 1;
		}
		$.ajax({
	        url: "habilitarPaquete",
	        data: {id_paquete: target_id, status: status_ },
	        type: "POST",
	        async: 'true',
	        success: function(response) {
	            if (response >= 1) {
	                $.gritter.add({
				        title: 'Actualizar status',
				        text: 'Se actualizó correctamente el status.',
				        class_name: 'success'
				      });
	            } else {
	                $.gritter.add({
				        title: 'Agregar status',
				        text: 'Ocurrió un error y no se pudo actualizar el status, intentelo más tarde.',
				        class_name: 'danger'
				      });
	            }
	        },
	        error: function(x, e) {
	            $.gritter.add({
				        title: 'Actualizar status',
				        text: 'Ocurrió un error y no se pudo actualizar el status: "' + e.messager+ '"',
				        class_name: 'danger'
				});
	        }
		});

	});


function actualizarPaquete(){
	$("#frmHotelEdit").submit();
}


function guardarPaquete(){
	$("#frmHotelAdd").submit();
}



function eliminarPaquete(id, nombre){
	$("#nombre_paquete_borrar").html(nombre);
	$("#id_paquete_borrar").val(id);


	$("#DeleteUser").modal();
}

$(document).ready(function(){

	$("#id_hotel_edit").change(function(){

		 $.ajax({
            url: "recuperarHabitaciones",
            data: {id_hotel: $(this).val() },
            type: "POST",
            async: 'true',
            success: function(response) {
				console.log(response);
				$("#id_habitacion_edit").html('<option value="0"> Seleccione una opción</option>'+response);

            },
            error: function(x, e) {
               console.log('Error');
            }
        });

	});

	$("#id_hotel_add").change(function(){

		 $.ajax({
            url: "recuperarHabitaciones",
            data: {id_hotel: $(this).val() },
            type: "POST",
            async: 'true',
            success: function(response) {
				console.log(response);
				$("#id_habitacion_add").html('<option value="0"> Seleccione una opción</option>'+response);

            },
            error: function(x, e) {
               console.log('Error');
            }
        });

	});

	$("#ifrm_update").load(function(){
		console.log('load iframe');
	   var ifrAddS = $("#ifrm_update").contents().find("#div_resultado").html();
	   if(ifrAddS == 1){
	   		 $.gritter.add({
				title: 'Subir archivo',
				text: 'Se actualizó correctamente la experiencia.',
		        class_name: 'success'
			 });
	         setTimeout ("window.location.href = 'https://www.joyasdemexico.mx/administrador/experiencias_mes/';", 2000);
	   }else{
	   		 /*$.gritter.add({
	            title: 'Subir archivo',
	            text: 'Ocurrio un error y no se pudo agregar el archivo: ',
	            class_name: 'danger'
	         });*/
	   }
	   
	});

	$("#ifrm_add").load(function(){
		console.log('load iframe');
		var ifrAddS = $("#ifrm_add").contents().find("#div_resultado").html();
		if(ifrAddS == 1){
	   		 $.gritter.add({
				title: 'Subir archivo',
				text: 'Se guardó correctamente la experiencia.',
		        class_name: 'success'
			 });
	         setTimeout ("window.location.href = 'https://www.joyasdemexico.mx/administrador/experiencias_mes/';", 2000);
		}
	   
	});
	

	$("#input_imagen_edit").change(function(){
		$("#home_mini").html("Seleccionado: <strong>"+$("#input_imagen_edit").val()+"</strong>");
	});
	$("#input_imagen_a_edit").change(function(){
		$("#home_imagen_a").html("Seleccionado: <strong>"+$("#input_imagen_a_edit").val()+"</strong>");
	});
	$("#input_imagen_b_edit").change(function(){
		$("#home_imagen_b").html("Seleccionado: <strong>"+$("#input_imagen_b_edit").val()+"</strong>");
	});
	$("#input_imagen_c_edit").change(function(){
		$("#home_imagen_c").html("Seleccionado: <strong>"+$("#input_imagen_c_edit").val()+"</strong>");
	});
	$("#input_imagen_d_edit").change(function(){
		$("#home_imagen_d").html("Seleccionado: <strong>"+$("#input_imagen_d_edit").val()+"</strong>");
	});
	$("#input_imagen_e_edit").change(function(){
		$("#home_imagen_e").html("Seleccionado: <strong>"+$("#input_imagen_e_edit").val()+"</strong>");
	});


	$("#input_imagen_mini_edit").change(function(){
		$("#home_mini_mini").html("Seleccionado: <strong>"+$("#input_imagen_mini_edit").val()+"</strong>");
	});
	$("#input_imagen_mini_a_edit").change(function(){
		$("#home_imagen_mini_a").html("Seleccionado: <strong>"+$("#input_imagen_mini_a_edit").val()+"</strong>");
	});
	$("#input_imagen_mini_b_edit").change(function(){
		$("#home_imagen_mini_b").html("Seleccionado: <strong>"+$("#input_imagen_mini_b_edit").val()+"</strong>");
	});
	$("#input_imagen_mini_c_edit").change(function(){
		$("#home_imagen_mini_c").html("Seleccionado: <strong>"+$("#input_imagen_mini_c_edit").val()+"</strong>");
	});
	$("#input_imagen_mini_d_edit").change(function(){
		$("#home_imagen_mini_d").html("Seleccionado: <strong>"+$("#input_imagen_mini_d_edit").val()+"</strong>");
	});
	$("#input_imagen_mini_e_edit").change(function(){
		$("#home_imagen_mini_e").html("Seleccionado: <strong>"+$("#input_imagen_mini_e_edit").val()+"</strong>");
	});



	

	$("#input_imagen_add").change(function(){
		$("#home_mini_add").html("Seleccionado: <strong>"+$("#input_imagen_add").val()+"</strong>");
	});
	$("#input_imagen_a_add").change(function(){
		$("#imagen_a_add").html("Seleccionado: <strong>"+$("#input_imagen_a_add").val()+"</strong>");
	});
	$("#input_imagen_b_add").change(function(){
		$("#imagen_b_add").html("Seleccionado: <strong>"+$("#input_imagen_b_add").val()+"</strong>");
	});
	$("#input_imagen_c_add").change(function(){
		$("#imagen_c_add").html("Seleccionado: <strong>"+$("#input_imagen_c_add").val()+"</strong>");
	});
	$("#input_imagen_d_add").change(function(){
		$("#imagen_d_add").html("Seleccionado: <strong>"+$("#input_imagen_d_add").val()+"</strong>");
	});
	$("#input_imagen_e_add").change(function(){
		$("#imagen_e_add").html("Seleccionado: <strong>"+$("#input_imagen_e_add").val()+"</strong>");
	});

	$("#input_imagen_mini_add").change(function(){
		$("#home_mini_add").html("Seleccionado: <strong>"+$("#input_imagen_mini_add").val()+"</strong>");
	});
	$("#input_imagen_mini_a_add").change(function(){
		$("#imagen_mini_a_add").html("Seleccionado: <strong>"+$("#input_imagen_mini_a_add").val()+"</strong>");
	});
	$("#input_imagen_mini_b_add").change(function(){
		$("#imagen_mini_b_add").html("Seleccionado: <strong>"+$("#input_imagen_mini_b_add").val()+"</strong>");
	});
	$("#input_imagen_mini_c_add").change(function(){
		$("#imagen_mini_c_add").html("Seleccionado: <strong>"+$("#input_imagen_mini_c_add").val()+"</strong>");
	});
	$("#input_imagen_mini_d_add").change(function(){
		$("#imagen_mini_d_add").html("Seleccionado: <strong>"+$("#input_imagen_mini_d_add").val()+"</strong>");
	});
	$("#input_imagen_mini_e_add").change(function(){
		$("#imagen_mini_e_add").html("Seleccionado: <strong>"+$("#input_imagen_mini_e_add").val()+"</strong>");
	});
	
	
	
});

function agregarPaquete() {
	$("#ModalAgregarHotel").modal('show');
}

function borrarPaquete(){
	var id_paquete_borrar = $('#id_paquete_borrar').val();
    $.ajax({
        url: "eliminarPaquete",
        data: {id_paquete: id_paquete_borrar},
        type: "POST",
        async: 'true',
        success: function(response) {
			 $.gritter.add({
				title: 'Eliminar experiencia',
				text: 'Se eliminó correctamente la experiencia',
		        class_name: 'success'
			 });
	         setTimeout ("window.location.href = 'https://www.joyasdemexico.mx/administrador/experiencias_mes/';", 2000);
        },
        error: function(x, e) {
            $.gritter.add({
			        title: 'Eliminar experiencia',
			        text: 'Ocurrio un error y no se pudo eliminar la experiencia, intentelo más tarde.',
			        class_name: 'danger'
			});
        }
    });


}


function editarPaquete(id) {

        $.ajax({
            url: "recuperarPaquete",
            data: {id_paquete: id},
            type: "POST",
            async: 'true',
            success: function(response) {
				var dat = jQuery.parseJSON(response);
				$("#id_paquete_edit").val(dat.id_paquete);
				$("#destino_edit").val(dat.destino);
				$("#nombre_paquete_edit").val(dat.nombre_paquete);
                //$("#descripcion_edit").val(dat.descripcion);
                $("#id_disponibilidad_edit").val(dat.id_disponibilidad);
                $("#precio_edit").val(dat.precio);
                $("#id_moneda_edit").val(dat.id_moneda);
                $("#cargo_moneda_edit").val(dat.cargo_moneda);
                $("#id_hotel_edit").val(dat.id_hotel);
                $("#iva_edit").val(dat.iva);
				$("#ish_edit").val(dat.ish);
                $("#cargo_servicio_edit").val(dat.cargo_servicio);
                $("#incluye_impuestos_edit").val(dat.incluye_impuestos);
                $("#tipo_cargo_edit").val(dat.tipo_cargo);
                $("#vigencia_inicio_edit").val(dat.vigencia_inicio);
                $("#vigencia_fin_edit").val(dat.vigencia_fin);

                $("#layout_edit").val(dat.layout);
                $("#orden_edit").val(dat.orden);

                $("#numero_habitaciones_edit").val(dat.numero_habitaciones);
                $("#tipo_tarifa_edit").val(dat.tipo_tarifa);
                $("#ocupacion_edit").val(dat.ocupacion);
                $("#personas_edit").val(dat.personas);
                $("#menores_edit").val(dat.menores);
                $("#dias_edit").val(dat.dias);
                //$("#contempla_menores_edit").val(dat.contempla_menores);

                tinyMCE.get('descripcion_edit').setContent( dat.descripcion );



                        $.ajax({
				            url: "recuperarHabitaciones",
				            data: {id_hotel: dat.id_hotel},
				            type: "POST",
				            async: 'true',
				            success: function(response) {
								console.log(response);
								$("#id_habitacion_edit").html('<option value="0"> Seleccione una opción</option>'+response);
								$("#id_habitacion_edit").val(dat.id_habitacion);

				            },
				            error: function(x, e) {
				               console.log('Error');
				            }
				        });




                if( dat.imagen_detalle!="" ){
                	$("#home_mini").html("Actual: <strong>"+dat.imagen_detalle+"</strong>");
                	$("#input_imagen_img").attr('src','../assets/imagenes/paquetes/'+dat.imagen_detalle);
                	$("#input_imagen_img").show();
                }else{
                	$("#home_mini").html("No se ha seleccionado ninguna imagen");
                	$("#input_imagen_img").hide();
                }




                if( dat.imagen_detalle_a!="" ){
                	$("#home_imagen_a").html("Actual: <strong>"+dat.imagen_detalle_a+"</strong>");
                	$("#input_imagen_a_img").attr('src','../assets/imagenes/paquetes/'+dat.imagen_detalle_a);
                	$("#input_imagen_a_img").show();
                }else{
                	$("#home_imagen_a").html("No se ha seleccionado ninguna imagen");
                	$("#input_imagen_a_img").hide();
                }
                if( dat.imagen_detalle_b!="" ){
                	$("#home_imagen_b").html("Actual: <strong>"+dat.imagen_detalle_b+"</strong>");
                	$("#input_imagen_b_img").attr('src','../assets/imagenes/paquetes/'+dat.imagen_detalle_b);
                	$("#input_imagen_b_img").show();
                }else{
                	$("#home_imagen_b").html("No se ha seleccionado ninguna imagen");
                	$("#input_imagen_b_img").hide();
                }
                if( dat.imagen_detalle_c!="" ){
                	$("#home_imagen_c").html("Actual: <strong>"+dat.imagen_detalle_c+"</strong>");
                	$("#input_imagen_c_img").attr('src','../assets/imagenes/paquetes/'+dat.imagen_detalle_c);
                	$("#input_imagen_c_img").show();
                }else{
                	$("#home_imagen_c").html("No se ha seleccionado ninguna imagen");
                	$("#input_imagen_c_img").hide();
                }
                if( dat.imagen_detalle_d!="" ){
                	$("#home_imagen_d").html("Actual: <strong>"+dat.imagen_detalle_d+"</strong>");
                	$("#input_imagen_d_img").attr('src','../assets/imagenes/paquetes/'+dat.imagen_detalle_d);
                	$("#input_imagen_d_img").show();
                }else{
                	$("#home_imagen_d").html("No se ha seleccionado ninguna imagen");
                	$("#input_imagen_d_img").hide();
                }
                if( dat.imagen_detalle_e!="" ){
                	$("#home_imagen_e").html("Actual: <strong>"+dat.imagen_detalle_e+"</strong>");
                	$("#input_imagen_e_img").attr('src','../assets/imagenes/paquetes/'+dat.imagen_detalle_e);
                	$("#input_imagen_e_img").show();
                }else{
                	$("#home_imagen_e").html("No se ha seleccionado ninguna imagen");
                	$("#input_imagen_e_img").hide();
                }


                if( dat.imagen_mini!="" ){
                	$("#home_mini_mini").html("Actual: <strong>"+dat.imagen_detalle+"</strong>");
                	$("#input_imagen_mini_img").attr('src','../assets/imagenes/paquetes/'+dat.imagen_mini);
                	$("#input_imagen_mini_img").show();
                }else{
                	$("#home_mini").html("No se ha seleccionado ninguna imagen");
                	$("#input_imagen_mini_img").hide();
                }
                if( dat.imagen_mini_a!="" ){
                	$("#home_imagen_mini_a").html("Actual: <strong>"+dat.imagen_detalle_a+"</strong>");
                	$("#input_imagen_mini_a_img").attr('src','../assets/imagenes/paquetes/'+dat.imagen_mini_a);
                	$("#input_imagen_mini_a_img").show();
                }else{
                	$("#home_imagen_mini_a").html("No se ha seleccionado ninguna imagen");
                	$("#input_imagen_mini_a_img").hide();
                }
                if( dat.imagen_mini_b!="" ){
                	$("#home_imagen_mini_b").html("Actual: <strong>"+dat.imagen_detalle_b+"</strong>");
                	$("#input_imagen_mini_b_img").attr('src','../assets/imagenes/paquetes/'+dat.imagen_mini_b);
                	$("#input_imagen_mini_b_img").show();
                }else{
                	$("#home_imagen_mini_b").html("No se ha seleccionado ninguna imagen");
                	$("#input_imagen_mini_b_img").hide();
                }
                if( dat.imagen_mini_c!="" ){
                	$("#home_imagen_mini_c").html("Actual: <strong>"+dat.imagen_detalle_c+"</strong>");
                	$("#input_imagen_mini_c_img").attr('src','../assets/imagenes/paquetes/'+dat.imagen_mini_c);
                	$("#input_imagen_mini_c_img").show();
                }else{
                	$("#home_imagen_mini_c").html("No se ha seleccionado ninguna imagen");
                	$("#input_imagen_mini_c_img").hide();
                }
                if( dat.imagen_mini_d!="" ){
                	$("#home_imagen_mini_d").html("Actual: <strong>"+dat.imagen_detalle_d+"</strong>");
                	$("#input_imagen_mini_d_img").attr('src','../assets/imagenes/paquetes/'+dat.imagen_mini_d);
                	$("#input_imagen_mini_d_img").show();
                }else{
                	$("#home_imagen_mini_d").html("No se ha seleccionado ninguna imagen");
                	$("#input_imagen_mini_d_img").hide();
                }
                if( dat.imagen_mini_e!="" ){
                	$("#home_imagen_mini_e").html("Actual: <strong>"+dat.imagen_detalle_e+"</strong>");
                	$("#input_imagen_mini_e_img").attr('src','../assets/imagenes/paquetes/'+dat.imagen_mini_e);
                	$("#input_imagen_mini_e_img").show();
                }else{
                	$("#home_imagen_mini_e").html("No se ha seleccionado ninguna imagen");
                	$("#input_imagen_mini_e_img").hide();
                }





				$("#ModalEditUser").modal('show');
            },
            error: function(x, e) {
                $.gritter.add({
				        title: 'Editar paquete',
				        text: 'Ocurrio un error y no se pueden recuperar los datos del paquete, intentelo más tarde.',
				        class_name: 'danger'
				});
            }
        });
}

</script>
