<?php 
$visible='';
$editable='';
if($this->session->userdata('perfil')=='vista'){
	$editable=' disabled="disabled" ';
	$visible=' style="display:none" ';

}
?>
<div class="row">
	<div class="col-md-12">
		<div class="block-flat">
			<div class="header">							
				<h3><i class="fa fa-money"></i> <strong>Solicitudes de facturación</strong></h3>
			</div>
			<div class="content">
				<div class="table-responsive" style="overflow-x: none;">

					<button title="Descargar excel" type="button" class="btn btn-info pull-left" data-dismiss="modal" onclick="descarga_excel_facturacion()"><span class="glyphicon glyphicon-arrow-down"></span> Descargar excel</button>

					<table class="table table-bordered" id="datatable_hoteles" >
						<thead>
							<tr>
								<th>Clave de reservación</th>
								<th>Nombre</th>
								<th>RFC</th>
								<th>Fecha de registro</th>
								<th>Estatus</th>
								<?php if($this->session->userdata('perfil')!='vista'){ ?>
								<th>Acciones</th>
								<?php } ?>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($facturacion_result as $facturacion_row){ ?>
								<tr class="odd gradeX">
									<td><?=$facturacion_row->clave_reservacion?></td>
									<td><?=$facturacion_row->nombre_factura?></td>
									<td><?=$facturacion_row->rfc_factura?></td>
									<td><?=$facturacion_row->fecha_registro?></td>
									<td><?=$facturacion_row->status_factura?></td>
									<?php if($this->session->userdata('perfil')!='vista'){ ?>
									<td style="text-align:center">
										<button type="button" title="Ver detalle de solicitud" class="btn btn-primary btn-sm" data-dismiss="modal" onclick="verSolicitud(<?=$facturacion_row->id_facturacion?>);"><span class="glyphicon glyphicon-search"></span> </button>
									</td>
									<?php } ?>
								</tr>
							<?php }?>
										
						</tbody>
					</table>			
				</div>

			</div>
		</div>				
	</div>
</div>


<!-- MODAL EDITAR PAQUETE -->
 <div class="modal fade" id="ModalEditUser" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="padding:15px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4><span class="glyphicon glyphicon-pencil"></span> Solicitud de facturación</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">

			<form role="form" id="frmHotelEdit" name="frmHotelEdit" action="facturacion/actualizarFacturacion" method="POST" enctype="multipart/form-data"  target="ifrm_update">

				<input type="hidden" name="id_facturacion_edit" id="id_facturacion_edit" value="0" />

				<div class="form-group">
					<h5 style="font-family: 'Open Sans', sans-serif;font-weight: 500;border-bottom: 1px solid #dadada;font-size: 16px;padding-bottom: 20px;">Datos generales</h5>
				</div>
              
	            

	            <div class="form-group">
						<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">Clave de reservación*</label><input type="text" maxlength="50"  class="form-control" placeholder="Clave de reservación" id="clave_reservacion_edit" name="clave_reservacion_edit">
						</div>
						<div style="width:45%;float: left;">
	              		<label for="nombre">Estatus de solicitud*</label>
	              		<select class="form-control" id="id_status_factura_edit" name="id_status_factura_edit">
	                        <option value=""> Seleccione una opción</option>
	                        <?php foreach($status_solicitud as $estatus){ ?>
	                        <option value="<?=$estatus->id_status_factura?>"><?=$estatus->descripcion?></option>
	                        <?php } ?>
	                    </select>
						</div>
	            </div>

	            <div class="form-group">
	              <label for="nombre">Nombre*</label><input type="text"  maxlength="50" class="form-control" placeholder="Nombre" id="nombre_factura_edit" name="nombre_factura_edit">
	            </div>
            
	            <div class="form-group">
					<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">RFC*</label><input type="text" maxlength="50"  class="form-control" placeholder="RFC" id="rfc_factura_edit" name="rfc_factura_edit">
					</div>
					<div style="width:45%;float: left;">
	              		<label for="nombre">Correo*</label><input type="text"  maxlength="50" class="form-control" placeholder="Correo" id="correo_factura_edit" name="correo_factura_edit">
					</div>
	            </div>

	            <div class="form-group">
						<label for="nombre">Dirección*</label>
						<textarea class="form-control" rows="6" id="direccion_factura_edit" name="direccion_factura_edit" style="resize: none;"></textarea>
	            </div>


		  		<!-- IMAGEN DETALLE -->
				<div class="form-group">
	            	<label>Archivo PDF:</label><br/>
		          	<div class="controls span3">
		          		<span class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar nuevo archivo <input type="file" id="input_factura_pdf_edit" name ="input_factura_pdf_edit"/>
						</span>
						<br/>
						<i id="archivo_factura_pdf">No se ha seleccionado ningun archivo</i><br/>
		            </div>
				</div>

				<div class="form-group">
	            	<label>Archivo XML:</label><br/>
		          	<div class="controls span3">
		          		<span class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar nuevo archivo <input type="file" id="input_factura_xml_edit" name ="input_factura_xml_edit"/>
						</span>
						<br/>
						<i id="archivo_factura_xml">No se ha seleccionado ningun archivo</i><br/>
		            </div>
				</div>


		  	</form>
		  	<iframe id="ifrm_update" name="ifrm_update" style="display: none;"></iframe>

        </div>
        <div class="modal-footer">
        	<button title="Enviar por correo factura en formato PDF y XML" type="button" class="btn btn-success pull-left" onclick="enviarFactura()"><span class="glyphicon glyphicon-send"></span> Enviar factura por correo</button>

        	<button type="button" class="btn btn-danger pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
			<button type="button" class="btn btn-success pull-right" onclick="actualizarFacturacion()"><span class="glyphicon glyphicon-floppy-disk"></span> Actualizar solicitud</button>
		  
		  <span id="msg_validacion"></span>
        </div>
      </div>
    </div>
  </div>

			
<script>


function descarga_excel_facturacion(){
    window.location.href="facturacion/descarga_csv";
}

function actualizarFacturacion(){
	$("#frmHotelEdit").submit();
}



$(document).ready(function(){

	$("#ifrm_update").load(function(){
		console.log('load iframe');
	   var ifrAddS = $("#ifrm_update").contents().find("#div_resultado").html();
	   if(ifrAddS == 1){
	   		 $.gritter.add({
				title: 'Subir archivo',
				text: 'Se actualizó correctamente la solicitud.',
		        class_name: 'success'
			 });
	         setTimeout ("window.location.href = 'facturacion';", 2000);
	   }else{
	   		 /*$.gritter.add({
	            title: 'Subir archivo',
	            text: 'Ocurrio un error y no se pudo agregar el archivo: ',
	            class_name: 'danger'
	         });*/
	   }
	   
	});



	$("#input_factura_pdf_edit").change(function(){
		$("#archivo_factura_pdf").html("Seleccionado: <strong>"+$("#input_factura_pdf_edit").val()+"</strong>");
	});
	

	$("#input_factura_xml_edit").change(function(){
		$("#archivo_factura_xml").html("Seleccionado: <strong>"+$("#input_factura_xml_edit").val()+"</strong>");
	});
	
	
});

function verSolicitud(id) {

        $.ajax({
            url: "facturacion/recuperarFacturacion",
            data: {id_facturacion: id},
            type: "POST",
            async: 'true',
            success: function(response) {
				var dat = jQuery.parseJSON(response);
				$("#id_facturacion_edit").val(dat.id_facturacion);
                $("#clave_reservacion_edit").val(dat.clave_reservacion);
                $("#nombre_factura_edit").val(dat.nombre_factura);
                $("#rfc_factura_edit").val(dat.rfc_factura);
                //$("#direccion_factura_edit").val(dat.direccion_factura);

                 tinyMCE.get('direccion_factura_edit').setContent( dat.direccion_factura );

                $("#correo_factura_edit").val(dat.correo_factura);
                $("#id_status_factura_edit").val(dat.id_status_factura);

                if( dat.factura_xml!="" ){
                	$("#archivo_factura_xml").html("Actual: <strong>"+dat.factura_xml+"</strong>");
                	//$("#input_imagen_img").attr('src','assets/imagenes/facturacion/'+dat.imagen_detalle);
                	//$("#input_imagen_img").show();
                }else{
                	$("#archivo_factura_xml").html("No se ha seleccionado ningun archivo");
                	//$("#input_imagen_img").hide();
                }
                if( dat.factura_pdf!="" ){
                	$("#archivo_factura_pdf").html("Actual: <strong>"+dat.factura_pdf+"</strong>");
                	//$("#input_imagen_img").attr('src','assets/imagenes/facturacion/'+dat.imagen_detalle);
                	//$("#input_imagen_img").show();
                }else{
                	$("#archivo_factura_pdf").html("No se ha seleccionado ningun archivo");
                	//$("#input_imagen_img").hide();
                }


				$("#ModalEditUser").modal('show');
            },
            error: function(x, e) {
                $.gritter.add({
				        title: 'Ver solicitud',
				        text: 'Ocurrio un error y no se pueden recuperar los datos de la solicitud, intentelo más tarde.',
				        class_name: 'danger'
				});
            }
        });
}




function enviarFactura() {
		id = $("#id_facturacion_edit").val();
		mail_ = $("#correo_factura_edit").val();
        $.ajax({
            url: "facturacion/enviarMailFactura",
            data: {id_facturacion: id},
            type: "POST",
            async: 'true',
            success: function(response) {
				if(response==1){
					$.gritter.add({
						title: 'Enviar factura',
						text: 'Se envió exitosamente la factura a '+mail_,
				        class_name: 'success'
					 });
					$("#ModalEditUser").modal('hide');
				}else if(response==2){
					$.gritter.add({
						title: 'Enviar factura',
						text: 'No se han cargado los archivos PDF y XML, por favor verifique',
				        class_name: 'danger'
					 });
				}else{
					$.gritter.add({
						title: 'Enviar factura',
						text: 'Ocurrio un error y no se puedo enviar la factura, por favor intentelo más tarde.',
				        class_name: 'danger'
					 });
				}
            },
            error: function(x, e) {
                $.gritter.add({
				        title: 'Enviar factura',
				        text: 'Ocurrio un error y no se puedo enviar la factura, por favor intentelo más tarde.',
				        class_name: 'danger'
				});
            }
        });
}
</script>
