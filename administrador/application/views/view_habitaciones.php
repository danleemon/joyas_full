<?php 
$visible='';
$editable='';
if($this->session->userdata('perfil')=='vista'){
	$editable=' disabled="disabled" ';
	$visible=' style="display:none" ';

}
?>
<div class="row">
	<div class="col-md-12">
		<div class="block-flat">
			<div class="header">							
				<h3><i class="fa fa-building-o"></i> <strong>Habitaciones</strong></h3>
			</div>
			<div class="content">
				<div class="table-responsive" style="overflow-x: none;">
					<table class="table table-bordered" id="datatable_hoteles" >
						<thead>
							<tr>
								
								<th>Tipo de habitación</th>
								<th>Hotel</th>
								<th>Fecha de registro</th>
								<?php if($this->session->userdata('perfil')!='vista'){ ?>
								<th>Visible</th>
								
								<th>Acciones</th>
								<?php } ?>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($habitaciones_result as $habitacion_row){ ?>
								<tr class="odd gradeX">
									<td><?=$habitacion_row->nombre_habitacion?></td>
									<td><?=$habitacion_row->nombre_hotel?></td>
									<td><?=$habitacion_row->fecha_registro?></td>
									<?php if($this->session->userdata('perfil')!='vista'){ ?>
									<td style="text-align:center"><input type="checkbox" name="my-checkbox" id="<?=$habitacion_row->id_habitacion?>" <?php if($habitacion_row->id_status_general == 1){ echo "checked";}?>></td>
									<td style="text-align:center">
										<button type="button" title="Editar habitación" class="btn btn-primary btn-sm" data-dismiss="modal" onclick="editarHabitacion(<?=$habitacion_row->id_habitacion?>);"><span class="glyphicon glyphicon-pencil"></span> </button>
										<button type="button" title="Eliminar habitación" class="btn btn-danger btn-sm" data-dismiss="modal" onclick="eliminarHabitacion(<?=$habitacion_row->id_habitacion?>, '<?=$habitacion_row->nombre_habitacion?>');"><span class="glyphicon glyphicon-trash"></span> </button>
									</td>
									<?php } ?>
								</tr>
							<?php }?>
										
						</tbody>
					</table>			
				</div>

				<button type="button" title="Agregar nueva habitacion" class="btn btn-info btn-sm" data-dismiss="modal" onclick="agregarHabitacion();"><span class="glyphicon glyphicon-plus"></span> Agregar nueva habitación</button>

			</div>
		</div>				
	</div>
</div>


<!-- MODAL EDITAR PAQUETE -->
 <div class="modal fade" id="ModalEditUser" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="padding:15px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4><span class="glyphicon glyphicon-pencil"></span> Editar habitación</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">

			<form role="form" id="frmHotelEdit" name="frmHotelEdit" action="habitaciones/actualizarHabitacion" method="POST" enctype="multipart/form-data"  target="ifrm_update">

				<input type="hidden" name="id_habitacion_edit" id="id_habitacion_edit" value="0" />

				<div class="form-group">
					<h5 style="font-family: 'Open Sans', sans-serif;font-weight: 500;border-bottom: 1px solid #dadada;font-size: 16px;padding-bottom: 20px;">Datos generales</h5>
				</div>
              
	            <div class="form-group">
	              <label for="nombre">Nombre de la habitacion*</label><input type="text"  maxlength="50" class="form-control" placeholder="Nombre de la habitación" id="nombre_habitacion_edit" name="nombre_habitacion_edit">
	            </div>


	            <div class="form-group">
						<div style="float: left;margin-right: 5%;width: 50%;">
	              			<label for="nombre">Ocupación máxima</label><input type="text" maxlength="1"  class="form-control onlynumbers" placeholder="Ocupación máxima" id="ocupacion_maxima_edit" name="ocupacion_maxima_edit">
	              		</div>
						<div style="width:45%;float: left;">
		              		<label for="nombre">Hotel*</label>
		              		<select class="form-control" id="id_hotel_edit" name="id_hotel_edit">
		                        <option value="0"> Seleccione una opción</option>
		                        <?php foreach($hoteles as $hotel){ ?>
		                        <option value="<?=$hotel->id_hotel?>"><?=$hotel->nombre_hotel?></option>
		                        <?php } ?>
		                    </select>
						</div>
	            </div>






	            <div class="form-group">
						<div style="float: left;margin-right: 5%;width: 50%;">
	              			<label for="nombre">Tarifa base*</label><input type="text" maxlength="50"  class="form-control" placeholder="Tarifa base" id="tarifa_base_edit" name="tarifa_base_edit">
						</div>
						<div style="width:45%;float: left;">
	              		<label for="nombre">Moneda*</label>
	              		<select class="form-control" id="id_moneda_edit" name="id_moneda_edit">
	                        <option value="0"> Seleccione una opción</option>
	                        <?php foreach($monedas as $moneda){ ?>
	                        <option value="<?=$moneda->id_moneda?>"><?=$moneda->descripcion?></option>
	                        <?php } ?>
	                    </select>
						</div>
	            </div>
            

	            <div class="form-group">
						<label for="nombre">Descripción*</label>
						<textarea class="form-control" rows="6" id="descripcion_edit" name="descripcion_edit" style="resize: none;"></textarea>
	            </div>


		  	</form>
		  	<iframe id="ifrm_update" name="ifrm_update" style="display: none;"></iframe>

        </div>
        <div class="modal-footer">
        	<button type="button" class="btn btn-danger pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
			<button type="button" class="btn btn-success pull-right" onclick="actualizarHabitacion()"><span class="glyphicon glyphicon-floppy-disk"></span> Actualizar habitación</button>
		  
		  <span id="msg_validacion"></span>
        </div>
      </div>
    </div>
  </div>







<!-- MODAL AGREGAR HOTEL -->
 <div class="modal fade" id="ModalAgregarHotel" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="padding:15px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4><span class="glyphicon glyphicon-pencil"></span> Agregar habitación</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">

			<form role="form" id="frmHotelAdd" name="frmHotelAdd" action="habitaciones/guardarHabitacion" method="POST" enctype="multipart/form-data"  target="ifrm_add">

				<div class="form-group">
					<h5 style="font-family: 'Open Sans', sans-serif;font-weight: 500;border-bottom: 1px solid #dadada;font-size: 16px;padding-bottom: 20px;">Datos generales</h5>
				</div>
              
	            <div class="form-group">
	              <label for="nombre">Nombre del paquete*</label><input type="text"  maxlength="50" class="form-control" placeholder="Nombre de la habitación" id="nombre_habitacion_add" name="nombre_habitacion_add">
	            </div>

	             <div class="form-group">
						<div style="float: left;margin-right: 5%;width: 50%;">
	              			<label for="nombre">Ocupación máxima</label><input type="text" maxlength="1"  class="form-control onlynumbers" placeholder="Ocupación máxima" id="ocupacion_maxima_add" name="ocupacion_maxima_add">
	              		</div>
						<div style="width:45%;float: left;">
		              		<label for="nombre">Hotel*</label>
		              		<select class="form-control" id="id_hotel_add" name="id_hotel_add">
		                        <option value="0"> Seleccione una opción</option>
		                        <?php foreach($hoteles as $hotel){ ?>
		                        <option value="<?=$hotel->id_hotel?>"><?=$hotel->nombre_hotel?></option>
		                        <?php } ?>
		                    </select>
						</div>
	            </div>


	             <div class="form-group">
						<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">Tarifa base*</label><input type="text" maxlength="50"  class="form-control" placeholder="Tarifa base" id="tarifa_base_add" name="tarifa_base_add">
						</div>
						<div style="width:45%;float: left;">
	              		<label for="nombre">Moneda*</label>
	              		<select class="form-control" id="id_moneda_add" name="id_moneda_add">
	                        <option value="0"> Seleccione una opción</option>
	                        <?php foreach($monedas as $moneda){ ?>
	                        <option value="<?=$moneda->id_moneda?>"><?=$moneda->descripcion?></option>
	                        <?php } ?>
	                    </select>
						</div>
	            </div>
            
	         
	            <div class="form-group">
						<label for="nombre">Descripción*</label>
						<textarea class="form-control" rows="6" id="descripcion_add" name="descripcion_add" style="resize: none;"></textarea>
	            </div>



		  	</form>
		  	<iframe id="ifrm_add" name="ifrm_add" style="display: none;"></iframe>

        </div>
        <div class="modal-footer">
        	<button type="button" class="btn btn-danger pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
			<button type="button" class="btn btn-success pull-right" onclick="guardarHabitacion()"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar habitación</button>
		  
		  <span id="msg_validacion"></span>
        </div>
      </div>
    </div>
  </div>



<!-- MODAL BORRAR HOTEL -->
<div class="modal fade" id="DeleteUser" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="background-color:#df4b33;padding:15px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 style="background-color:#df4b33;"><span class="glyphicon glyphicon-exclamation-sign"></span> Eliminar habitación</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
        	<input type="hidden" name="id_habitacion_borrar" id="id_habitacion_borrar" value="0" />
			<form role="form">
          		<p >¿Confirma que desea eliminar el paquete <b class="form-subtitle" id="nombre_habitacion_borrar"></b>?</p>
			</form>
										
        </div>
        <div class="modal-footer">
        	<button type="button" class="btn btn-danger pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
        	<button type="button" class="btn btn-danger pull-right" data-dismiss="modal" onclick="borrarHabitacion();"><span class="glyphicon glyphicon-trash"></span> Eliminar</button>
        </div>
      </div>
	</div>
</div>

			
<script>

	$('input[name="my-checkbox"]').on('switchChange.bootstrapSwitch', function(event, state) {
		var target_id = $(this).attr('id');
		var status_ = 0;

		if(state){
			status_ = 1;
		}
		$.ajax({
	        url: "habitaciones/habilitarHabitacion",
	        data: {id_habitacion: target_id, status: status_ },
	        type: "POST",
	        async: 'true',
	        success: function(response) {
	            if (response >= 1) {
	                $.gritter.add({
				        title: 'Actualizar status',
				        text: 'Se actualizó correctamente la habitación.',
				        class_name: 'success'
				      });
	            } else {
	                $.gritter.add({
				        title: 'Agregar status',
				        text: 'Ocurrió un error y no se pudo actualizar la habitación, intentelo más tarde.',
				        class_name: 'danger'
				      });
	            }
	        },
	        error: function(x, e) {
	            $.gritter.add({
				        title: 'Actualizar status',
				        text: 'Ocurrió un error y no se pudo actualizar la habitación: "' + e.messager+ '"',
				        class_name: 'danger'
				});
	        }
		});

	});


function actualizarHabitacion(){
	$("#frmHotelEdit").submit();
}


function guardarHabitacion(){
	$("#frmHotelAdd").submit();
}



function eliminarHabitacion(id, nombre){
	$("#nombre_habitacion_borrar").html(nombre);
	$("#id_habitacion_borrar").val(id);


	$("#DeleteUser").modal();
}

$(document).ready(function(){

	$("#ifrm_update").load(function(){
		console.log('load iframe');
	   var ifrAddS = $("#ifrm_update").contents().find("#div_resultado").html();
	   if(ifrAddS == 1){
	   		 $.gritter.add({
				title: 'Editar habitación',
				text: 'Se actualizó correctamente la habitación.',
		        class_name: 'success'
			 });
	         setTimeout ("window.location.href = 'habitaciones';", 2000);
	   }else{
	   		 /*$.gritter.add({
	            title: 'Subir archivo',
	            text: 'Ocurrio un error y no se pudo agregar el archivo: ',
	            class_name: 'danger'
	         });*/
	   }
	   
	});
	$("#ifrm_add").load(function(){
		console.log('load iframe');
		var ifrAddS = $("#ifrm_add").contents().find("#div_resultado").html();
		if(ifrAddS == 1){
	   		 $.gritter.add({
				title: 'Guardar habitación',
				text: 'Se guardó correctamente la habitación.',
		        class_name: 'success'
			 });
	         setTimeout ("window.location.href = 'habitaciones';", 2000);
		}
	   
	});

	
	
	
});

function agregarHabitacion() {
	$("#ModalAgregarHotel").modal('show');
}

function borrarHabitacion(){
	var id_habitacion_borrar = $('#id_habitacion_borrar').val();
    $.ajax({
        url: "habitaciones/eliminarHabitacion",
        data: {id_habitacion: id_habitacion_borrar},
        type: "POST",
        async: 'true',
        success: function(response) {
			 $.gritter.add({
				title: 'Eliminar habitación',
				text: 'Se eliminó correctamente la habitación.',
		        class_name: 'success'
			 });
	         setTimeout ("window.location.href = 'habitaciones';", 2000);
        },
        error: function(x, e) {
            $.gritter.add({
			        title: 'Eliminar habitación',
			        text: 'Ocurrio un error y no se pudo eliminar la habitación, intentelo más tarde.',
			        class_name: 'danger'
			});
        }
    });


}


function editarHabitacion(id) {

        $.ajax({
            url: "habitaciones/recuperarHabitacion",
            data: {id_habitacion: id},
            type: "POST",
            async: 'true',
            success: function(response) {
				var dat = jQuery.parseJSON(response);
				$("#id_habitacion_edit").val(dat.id_habitacion);
				$("#nombre_habitacion_edit").val(dat.nombre_habitacion);
                //$("#descripcion_edit").val(dat.descripcion);
                $("#ocupacion_maxima_edit").val(dat. ocupacion_maxima);
                $("#id_hotel_edit").val(dat.id_hotel);
                $("#tarifa_base_edit").val(dat.tarifa_base);
                $("#id_moneda_edit").val(dat.id_moneda);

                tinyMCE.get('descripcion_edit').setContent( dat.descripcion );
				$("#ModalEditUser").modal('show');
            },
            error: function(x, e) {
                $.gritter.add({
				        title: 'Editar habitacion',
				        text: 'Ocurrio un error y no se pueden recuperar los datos de la habitación, intentelo más tarde.',
				        class_name: 'danger'
				});
            }
        });
}

</script>
