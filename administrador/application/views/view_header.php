<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="shortcut icon" href="images/favicon.png">

	<title>JOYAS SOFT 3.0 - Administrador</title>
	
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Raleway:300,200,100' rel='stylesheet' type='text/css'>

	<!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/js/bootstrap.switch/bootstrap-switch.css" />
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/js/bootstrap.datetimepicker/css/bootstrap-datetimepicker.min.css" />
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/js/jquery.gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/js/jquery.select2/select2.css" />
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/js/bootstrap.slider/css/slider.css" />
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/style.css"/>	
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/font-awesome-4/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/pygments.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/js/jquery.nanoscroller/nanoscroller.css" />
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/js/jquery.easypiechart/jquery.easy-pie-chart.css" />
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/js/jquery.niftymodals/css/component.css" />
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/js/jquery.datatables/bootstrap-adapter/css/datatables.css" />
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/js/jquery.icheck/skins/square/blue.css" >
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/js/jquery.magnific-popup/dist/magnific-popup.css" />
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/js/dropzone/css/dropzone.css" />
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/js/bootstrap.wysihtml5/src/bootstrap-wysihtml5.css"></link>
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/js/bootstrap.summernote/dist/summernote.css" />
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/bootstrap-switch.min.css" />
  <link rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/fullcalendar.css"  />
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/fullcalendar.print.css" media="print" />

  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>


  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
    <script src="../../assets/js/html5shiv.js"></script>
    <script src="../../assets/js/respond.min.js"></script>
  <![endif]-->








	<style>
		/******* FORM validacion*******/
	input.error{
		background: #f8dbdb !important;
		border-color: #e77776;
	}
	select.error{
		background: #f8dbdb !important;
		border-color: #e77776;	
	}
	textarea.error{
		background: #f8dbdb !important;
		border-color: #e77776;
	}
	#error{
		margin-bottom: 20px;
		border: 1px solid #efefef !important;
	}

    .modal-header .close {
        margin-top: 12px;
    }

	/******* /FORM *******/
	</style>

<style>
        .modal-header, h4, .close {
            background-color: #337ab7;
            color:white !important;
            text-align: center;
            font-size: 30px;
        }
        .modal-footer {
            background-color: #f9f9f9;
        }
        .btn-link{
            color: #FFFFFF;
        }
        .modal-dialog {
            top: 50px;
        }
        .form-subtitle {
            font-weight: 700;
        }

.resumen_concentrado{
  display:none;
}

.detalle_concentrado{
  display:none;
}



input[type=checkbox]:not(old),
input[type=radio   ]:not(old){
  width     : 2em;
  margin    : 0;
  padding   : 0;
  font-size : 1em;
  opacity   : 0;
  display: none;
}


input[type=checkbox]:not(old) + label > span,
input[type=radio   ]:not(old) + label > span{
  display          : inline-block;
  width            : 1.5em;
  height           : 1.5em;
  margin           :0.5em 0em 0 0.25em;
  border-radius    : 0.25em;
  background       : rgb(150,150,150);
  background-image :    -moz-linear-gradient(rgb(150,150,150),rgb(180,180,180));
  background-image :     -ms-linear-gradient(rgb(150,150,150),rgb(180,180,180));
  background-image :      -o-linear-gradient(rgb(150,150,150),rgb(180,180,180));
  background-image : -webkit-linear-gradient(rgb(150,150,150),rgb(180,180,180));
  background-image :         linear-gradient(rgb(150,150,150),rgb(180,180,180));
  vertical-align   : bottom;
}



input[type=checkbox]:not(old):checked + label > span:before{
  content     : '✓';
  display     : block;
  width: 1.1em;
  color       : rgb(255,255,255);
  font-size   : 1.2em;
  text-align  : center;
  font-weight : bold;
}

input[type=radio]:not(old):checked + label > span > span{
  display          : block;
  width            : 0.5em;
  height           : 0.5em;
  margin           : 0.125em;
  border           : 0.0625em solid rgb(115,153,77);
  border-radius    : 0.125em;
  background       : rgb(153,204,102);
  background-image :    -moz-linear-gradient(rgb(179,217,140),rgb(153,204,102));
  background-image :     -ms-linear-gradient(rgb(179,217,140),rgb(153,204,102));
  background-image :      -o-linear-gradient(rgb(179,217,140),rgb(153,204,102));
  background-image : -webkit-linear-gradient(rgb(179,217,140),rgb(153,204,102));
  background-image :         linear-gradient(rgb(179,217,140),rgb(153,204,102));
}


input[type=checkbox].select_territorio:not(old) + label > span{
  display          : inline-block;
  width            : 1.5em;
  height           : 1.5em;
  margin           :0.5em 0em 0 0.25em;
  border-radius    : 0.25em;
  background       : rgb(150,150,150);
  background-image :    -moz-linear-gradient(rgb(38,97,255),rgb(78,157,255));
  background-image :     -ms-linear-gradient(rgb(38,97,255),rgb(78,157,255));
  background-image :      -o-linear-gradient(rgb(38,97,255),rgb(78,157,255));
  background-image : -webkit-linear-gradient(rgb(38,97,255),rgb(78,157,255));
  background-image :         linear-gradient(rgb(38,97,255),rgb(78,157,255));
  vertical-align   : bottom;
}

#calendar {
		max-width: 400px;
		margin: 0 auto;
	}



    .btn-file {
        position: relative;
        overflow: hidden;
    }
    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background: white;
        cursor: inherit;
        display: block;
    }


    
        </style>

	
	<script src="<?=base_url()?>assets/js/jquery.js"></script>
	
</head>

<body>

<ul class="nav navbar-nav navbar-right">
                        <li class="dropdown notifications hidden-xs hidden-sm">
                        	<i class="fa fa-user"></i> Bienvenido <br> <b><?=$this->session->userdata('nombre')?></b>
                        </li>
                        <li class="dropdown notifications hidden-xs hidden-sm">
                                <a href="<?=base_url()?>cambiar-pass"><i class="fa fa-key"></i> Cambiar Contraseña</a>
                        </li>
                        <li class="dropdown notifications hidden-xs hidden-sm">
                                <a href="<?=base_url()?>home/logout"><i class="fa fa-sign-out"></i> Salir</a>
                        </li>
                    </ul>
                    
                    
  <!-- Fixed navbar -->
  <div id="head-nav" class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="fa fa-gear"></span>
        </button>
        <a class="navbar-brand" href="<?=base_url()?>"><span>JOYAS SOFT 3.0</span></a>
      </div>
      <div class="navbar-collapse collapse">
		    <ul class="nav navbar-nav navbar-right user-nav">
		      <li class="dropdown profile_menu">
		        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img alt="Avatar" src="<?=base_url()?>assets/images/icon_p10.gif" /><i class="fa fa-user"></i> Bienvenido <b><?=$this->session->userdata('nombre')?></b> <b class="caret"></b></a>
		        <ul class="dropdown-menu">
		          <li><a href="<?=base_url()?>cambiar-pass"><i class="fa fa-key"></i> Cambiar Contraseña</a></li>
		          <li><a href="<?=base_url()?>home/logout"><i class="fa fa-sign-out"></i> Salir</a></li>
		        </ul>
		      </li>
		    </ul>	
      </div><!--/.nav-collapse -->
    </div>
  </div>
	
<div id="cl-wrapper">
		<div class="cl-sidebar">
			<div class="cl-toggle"><i class="fa fa-bars"></i></div>
			<div class="cl-navblock">
        <div class="menu-space">
          <div class="content">
            <ul class="cl-vnavigation">

            <?php if($this->session->userdata('perfil')!='administrador-mkt' ){ ?>
              <li>
                 <a <?php if(isset($seccion_hoteles)){ echo 'style="color:#d8e3ff;font-weight: 600;"';}?> href="<?=base_url()?>hoteles"><i class="fa fa-building-o"></i> <span>Hoteles</span></a>
              </li>
            
 
              <li>
                 <a <?php if(isset($seccion_habitaciones)){ echo 'style="color:#d8e3ff;font-weight: 600;"';}?> href="<?=base_url()?>habitaciones"><i class="fa fa-building-o"></i> <span>Habitaciones</span></a>
              </li>

              <?php } ?>

  <?php if($this->session->userdata('perfil')!='administrador-mkt' ){ ?>
              <li>
                 <a <?php if(isset($seccion_paquetes)){ echo 'style="color:#d8e3ff;font-weight: 600;"';}?> href="<?=base_url()?>paquetes"><i class="fa fa-tags"></i> <span>Tarifas y paquetes</span></a>
              </li>
  <?php } ?>


              <li>
                <a style="cursor:default"><i class="fa fa-flag"></i> <span>Promo Experience Manager</span></a>
              </li>

  <?php if($this->session->userdata('perfil')=='vista' || $this->session->userdata('perfil')=='administrador' || $this->session->userdata('perfil')=='administrador-mkt'  || $this->session->userdata('perfil')=='administrador-branding'){ ?>
               <li>
                <a <?php if(isset($seccion_banner_home)){ echo 'style="color:#d8e3ff;font-weight: 600;margin-left:25px"';}else{ echo 'style="margin-left:25px"'; }?> href="<?=base_url()?>promo_home/"><i class="fa fa-circle-o"></i><span>Banners Home</span></a>
              </li>
              <li>
                <a <?php if(isset($seccion_banners_hoteles)){ echo 'style="color:#d8e3ff;font-weight: 600;margin-left:25px"';}else{ echo 'style="margin-left:25px"'; }?> href="<?=base_url()?>promo_hoteles/"><i class="fa fa-circle-o"></i><span>Banners Hoteles</span></a>
              </li>

 <?php } ?>

  <?php if($this->session->userdata('perfil')!='administrador-mkt' ){ ?>
              <li>
                <a <?php if(isset($seccion_especiales)){ echo 'style="color:#d8e3ff;font-weight: 600;margin-left:25px"';}else{ echo 'style="margin-left:25px"'; }?> href="<?=base_url()?>experiencias_especiales/"><i class="fa fa-circle-o"></i><span>Experiencias exclusivas</span></a>
              </li>
              <li>
                <a <?php if(isset($seccion_temporada)){ echo 'style="color:#d8e3ff;font-weight: 600;margin-left:25px"';}else{ echo 'style="margin-left:25px"'; }?> href="<?=base_url()?>experiencias_temporada/"><i class="fa fa-circle-o"></i><span>Experiencias de temporada</span></a>
              </li>
              <li>
                <a <?php if(isset($seccion_mes)){ echo 'style="color:#d8e3ff;font-weight: 600;margin-left:25px"';}else{ echo 'style="margin-left:25px"'; }?> href="<?=base_url()?>experiencias_mes/"><i class="fa fa-circle-o"></i><span>Experiencias del mes</span></a>
              </li>

              </li>
     <?php } ?>


			<?php if($this->session->userdata('perfil')=='vista' || $this->session->userdata('perfil')=='administrador'  || $this->session->userdata('perfil')=='administrador-mkt' || $this->session->userdata('perfil')=='administrador-branding'){ ?>
              <li>
                 <a <?php if(isset($seccion_solicitudes)){ echo 'style="color:#d8e3ff;font-weight: 600;"';}?> href="<?=base_url()?>solicitudes"><i class="fa fa-hand-o-right"></i> <span>Solicitudes</span></a>
              </li>
            <?php } ?>
          
			<?php if($this->session->userdata('perfil')=='vista' || $this->session->userdata('perfil')=='administrador'  || $this->session->userdata('perfil')=='administrador-mkt'  || $this->session->userdata('perfil')=='administrador-branding'){ ?>
              <li>
                 <a <?php if(isset($seccion_reservaciones)){ echo 'style="color:#d8e3ff;font-weight: 600;"';}?> href="<?=base_url()?>reservaciones"><i class="fa fa-calendar"></i> <span>Reservaciones</span></a>
              </li>
            <?php } ?>
          

			<?php if($this->session->userdata('perfil')=='vista' || $this->session->userdata('perfil')=='administrador' || $this->session->userdata('perfil')=='administrador-mkt'){ ?>
              <li>
                 <a <?php if(isset($seccion_facturacion)){ echo 'style="color:#d8e3ff;font-weight: 600;"';}?> href="<?=base_url()?>facturacion"><i class="fa fa-money"></i> <span>Facturación</span></a>
              </li>
			<?php } ?>
          
          
			<?php if($this->session->userdata('perfil')=='vista' || $this->session->userdata('perfil')=='administrador' || $this->session->userdata('perfil')=='administrador-mkt'  || $this->session->userdata('perfil')=='administrador-branding'){ ?>
              <li>
                 <a <?php if(isset($seccion_contacto)){ echo 'style="color:#d8e3ff;font-weight: 600;"';}?> href="<?=base_url()?>contacto"><i class="fa fa-users"></i> <span>Contacto</span></a>
              </li>
			<?php } ?>
			
			
			
              <!--li>
                 <a <?php if(isset($seccion_catalogos)){ echo 'style="color:#d8e3ff;font-weight: 600;"';}?> href="<?=base_url()?>catalogos"><i class="fa fa-list-ul"></i> <span>Catálogos</span></a>
              </li-->

             
            </ul>
          </div>
        </div>

			</div>
		</div>
	
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont" id="agasajo">
