<?php 
$visible='';
$editable='';
if($this->session->userdata('perfil')=='vista'){
	$editable=' disabled="disabled" ';
	$visible=' style="display:none" ';

}
?>


<div class="row">
	<div class="col-md-12">
		<div class="block-flat">
			<div class="header">							
				<h3><i class="fa fa-building-o"></i> <strong>Hoteles</strong></h3>
			</div>
			<div class="content">
				<div class="table-responsive" style="overflow-x: none;">
					<table class="table table-bordered" id="datatable_hoteles" >
						<thead>
							<tr>
								<th>Región</th>
								<th>Destino</th>
								<th>Hotel</th>
								<th>Fecha de registro</th>
								<?php if($this->session->userdata('perfil')!='vista'){ ?>
								<th>Mostrar en banners</th>
								<th>Mostrar en paquetes</th>
								<?php } ?>
								
							</tr>
						</thead>
						<tbody>
							<?php foreach ($hoteles_result as $hotel_row){ ?>
								<tr class="odd gradeX">
									<td><?=$hotel_row->region?></td>
									<td><?=$hotel_row->destino?></td>
									<td><?=$hotel_row->nombre_hotel?></td>
									<td><?=$hotel_row->fecha_registro?></td>
									
									<?php if($this->session->userdata('perfil')!='vista'){ ?>
									<td style="text-align:center"><input type="checkbox" name="my-checkbox" id="<?=$hotel_row->id_hotel?>" <?php if($hotel_row->id_status_general == 1){ echo "checked";}?>></td>
									<td style="text-align:center"><input type="checkbox" name="my-statusbox" id="<?=$hotel_row->id_hotel?>" <?php if($hotel_row->visible_promociones == 1){ echo "checked";}?>></td>
									<td style="text-align:center">
										<button type="button" title="Editar hotel" class="btn btn-primary btn-sm" data-dismiss="modal" onclick="editarHotel(<?=$hotel_row->id_hotel?>);"><span class="glyphicon glyphicon-pencil"></span> </button>
										<button type="button" title="Eliminar hotel" class="btn btn-danger btn-sm" data-dismiss="modal" onclick="eliminarHotel(<?=$hotel_row->id_hotel?>, '<?=$hotel_row->nombre_hotel?>');"><span class="glyphicon glyphicon-trash"></span> </button>
									</td>
									
									<?php } ?>
								</tr>
							<?php }?>
										
						</tbody>
					</table>			
				</div>

				<?php if($this->session->userdata('perfil')!='vista'){ ?>
				<button type="button" title="Agregar nuevo hotel" class="btn btn-info btn-sm" data-dismiss="modal" onclick="agregarHotel();"><span class="glyphicon glyphicon-plus"></span> Agregar nuevo hotel</button>
				<?php } ?>
			</div>
		</div>				
	</div>
</div>


<!-- MODAL EDITAR HOTEL -->
 <div class="modal fade" id="ModalEditUser" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="padding:15px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4><span class="glyphicon glyphicon-pencil"></span> Editar Hotel</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">

			<form role="form" id="frmHotelEdit" name="frmHotelEdit" action="hoteles/actualizarHotel" method="POST" enctype="multipart/form-data"  target="ifrm_update">

				<input type="hidden" name="id_hotel_edit" id="id_hotel_edit" value="0" />

				<div class="form-group">
					<h5 style="font-family: 'Open Sans', sans-serif;font-weight: 500;border-bottom: 1px solid #dadada;font-size: 16px;padding-bottom: 20px;">Datos generales</h5>
				</div>
              
	            <div class="form-group">
	              <label for="nombre">Nombre del hotel*</label><input type="text" <?=$editable?> maxlength="50" class="form-control" placeholder="Nombre" id="nombre_hotel_edit" name="nombre_hotel_edit">
	            </div>

	            <div class="form-group">
						<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">Región*</label>
	              		<select <?=$editable?>class="form-control" id="region_edit" name="region_edit">
	                        <option value="0"> Seleccione una opción</option>
	                        <?php foreach($regiones as $region){ ?>
	                        <option value="<?=$region->id_region?>"><?=$region->descripcion?></option>
	                        <?php } ?>
	                    </select>
						</div>
						<div style="width:45%;float: left;">
	              		<label for="nombre">Destino*</label>
	              		<select <?=$editable?>class="form-control" id="destino_edit" name="destino_edit">
	                        <option value="0"> Seleccione una opción</option>
	                        <?php foreach($destinos as $destino){ ?>
	                        <option value="<?=$destino->id_destino?>"><?=$destino->descripcion?></option>
	                        <?php } ?>
	                    </select>
						</div>
	            </div>
            
	            <div class="form-group">
					<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">Ciudad*</label><input type="text" <?=$editable?>maxlength="50"  class="form-control" placeholder="Ciudad" id="ciudad_edit" name="ciudad_edit">
					</div>
					<div style="width:45%;float: left;">
	              		<label for="nombre">Código postal*</label><input type="text" <?=$editable?> maxlength="50" class="form-control" placeholder="Código postal" id="codigo_postal_edit" name="codigo_postal_edit">
					</div>
	            </div>

	            <div class="form-group">
						<label for="nombre">Leyenda*</label>

						<textarea class="form-control" <?=$editable?>rows="6" id="leyenda_edit" name="leyenda_edit" style="resize: none;"></textarea>
	            </div>
	            <div class="form-group">
						<label for="nombre">Declaración*</label>
						<textarea class="form-control" <?=$editable?>rows="6" id="declaracion_edit" name="declaracion_edit" style="resize: none;"></textarea>
	            </div>

	            <div class="form-group">
						<label for="nombre">Dirección*</label>
						<textarea class="form-control" <?=$editable?>rows="6" id="direccion_edit" name="direccion_edit" style="resize: none;"></textarea>
	            </div>

				<!-- SLIDER HOME -->
			
        		<div class="form-group">
					<h5 style="font-family: 'Open Sans', sans-serif;font-weight: 500;border-bottom: 1px solid #dadada;font-size: 16px;padding-bottom: 20px;">Imágenes</h5>
				</div>
				<div class="form-group">
	            	<label>Slider Home:</label>
	            	<img src="" id="input_slider_home_img" style="width:100%;padding-bottom: 10px;display:none">
		          	<div class="controls span3">
		          		<span <?=$visible?> class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar nueva imagen <input type="file" id="input_slider_home_edit" name ="input_slider_home_edit"/>
						</span>
						<br/>
						<i id="slider_home">No se ha seleccionado ninguna imagen</i><br/>
			            <!--i>*La extensión del archivo puede ser doc, pdf, xls, jpg, png</i-->
						   
		            </div>
				</div>

		  		<!-- HOME MINI -->
				<div class="form-group">
	            	<label>Home icono:</label><br/>
	            	<img src="" id="input_home_mini_img" style="max-width:100%;width:188px;padding-bottom: 10px;display:none">
		          	<div class="controls span3">
		          		<span <?=$visible?> class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar nueva imagen <input type="file" id="input_home_mini_edit" name ="input_home_mini_edit"/>
						</span>
						<br/>
						<i id="home_mini">No se ha seleccionado ninguna imagen</i><br/>
		            </div>
				</div>

		  		<!-- SLIDER INTERNO A -->
				<div class="form-group">
	            	<label>Banner interno (1):</label>
	            	<img src="" id="input_slider_interno_a_img" style="width:100%;padding-bottom: 10px;display:none"><br/>
		          	<div class="controls span3">
		          		<span <?=$visible?> class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar nueva imagen <input type="file" id="input_slider_interno_a_edit" name ="input_slider_interno_a_edit"/>
						</span>
						<br/>
						<i id="slider_interno_a">No se ha seleccionado ninguna imagen</i><br/>
		            </div>
				</div>
		  	

		  		<!-- SLIDER INTERNO B -->
				<div class="form-group">
	            	<label>Banner interno (2):</label>
	            	<img src="" id="input_slider_interno_b_img" style="width:100%;padding-bottom: 10px;display:none">
		          	<div class="controls span3">
		          		<span <?=$visible?> class="btn btn-info btn-file" >
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar nueva imagen <input type="file" id="input_slider_interno_b_edit" name ="input_slider_interno_b_edit"/>
						</span>
						<br/>
						<i id="slider_interno_b">No se ha seleccionado ninguna imagen</i><br/>
		            </div>
				</div>

		  		<!-- SLIDER INTERNO C -->
				<div class="form-group">
	            	<label>Banner interno (3):</label>
	            	<img src="" id="input_slider_interno_c_img" style="width:100%;padding-bottom: 10px;display:none">
		          	<div class="controls span3">
		          		<span <?=$visible?> class="btn btn-info btn-file" >
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar nueva imagen <input type="file" id="input_slider_interno_c_edit" name ="input_slider_interno_c_edit"/>
						</span>
						<br/>
						<i id="slider_interno_c">No se ha seleccionado ninguna imagen</i><br/>
		            </div>
				</div>
		  

		  		<!-- SLIDER INTERNO D -->
				<div class="form-group">
	            	<label>Banner interno (4):</label>
	            	<img src="" id="input_slider_interno_d_img" style="width:100%;padding-bottom: 10px;display:none">
		          	<div class="controls span3">
		          		<span <?=$visible?> class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar nueva imagen <input type="file" id="input_slider_interno_d_edit" name ="input_slider_interno_d_edit"/>
						</span>
						<br/>
						<i id="slider_interno_d">No se ha seleccionado ninguna imagen</i><br/>
		            </div>
				</div>
		 

		  		<!-- SLIDER INTERNO E -->
				<div class="form-group">
	            	<label>Banner interno (5):</label>
	            	<img src="" id="input_slider_interno_e_img" style="width:100%;padding-bottom: 10px;display:none">
		          	<div class="controls span3">
		          		<span <?=$visible?> class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar nueva imagen <input type="file" id="input_slider_interno_e_edit" name ="input_slider_interno_e_edit"/>
						</span>
						<br/>
						<i id="slider_interno_e">No se ha seleccionado ninguna imagen</i><br/>
		            </div>
				</div>
		  
		  		<!-- SLIDER INTERNO F -->
				<div class="form-group">
	            	<label>Banner interno (6):</label>
	            	<img src="" id="input_slider_interno_f_img" style="width:100%;padding-bottom: 10px;display:none">
		          	<div class="controls span3">
		          		<span <?=$visible?> class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar nueva imagen <input type="file" id="input_slider_interno_f_edit" name ="input_slider_interno_f_edit"/>
						</span>
						<br/>
						<i id="slider_interno_f">No se ha seleccionado ninguna imagen</i><br/>
		            </div>
				</div>

		  	</form>
		  	<iframe id="ifrm_update" name="ifrm_update" style="display: none;"></iframe>

        </div>
        <div class="modal-footer">

        	<?php if($this->session->userdata('perfil')!='vista'){ ?>
        	<button type="button" class="btn btn-danger pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
			<button type="button" class="btn btn-success pull-right" onclick="actualizarHotel()"><span class="glyphicon glyphicon-floppy-disk"></span> Actualizar hotel</button>
		  	<?php }else{ ?>
		  	<button type="button" class="btn btn-info pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cerrar</button>
		  	<?php } ?>
		  <span id="msg_validacion"></span>
        </div>
      </div>
    </div>
  </div>







<!-- MODAL AGREGAR HOTEL -->
 <div class="modal fade" id="ModalAgregarHotel" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="padding:15px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4><span class="glyphicon glyphicon-pencil"></span> Agregar Hotel</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">

			<form role="form" id="frmHotelAdd" name="frmHotelAdd" action="hoteles/agregarHotel" method="POST" enctype="multipart/form-data"  target="ifrm_add">

				<div class="form-group">
					<h5 style="font-family: 'Open Sans', sans-serif;font-weight: 500;border-bottom: 1px solid #dadada;font-size: 16px;padding-bottom: 20px;">Datos generales</h5>
				</div>
              
	            <div class="form-group">
	              <label for="nombre">Nombre del hotel*</label><input type="text"  maxlength="50" class="form-control" placeholder="Nombre" id="nombre_hotel_add" name="nombre_hotel_add">
	            </div>

	            <div class="form-group">
						<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">Región*</label>
	              		<select class="form-control" id="region_add" name="region_add">
	                        <option value="0"> Seleccione una opción</option>
	                        <?php foreach($regiones as $region){ ?>
	                        <option value="<?=$region->id_region?>"><?=$region->descripcion?></option>
	                        <?php } ?>
	                    </select>
						</div>
						<div style="width:45%;float: left;">
	              		<label for="nombre">Destino*</label>
	              		<select class="form-control" id="destino_add" name="destino_add">
	                        <option value="0"> Seleccione una opción</option>
	                        <?php foreach($destinos as $destino){ ?>
	                        <option value="<?=$destino->id_destino?>"><?=$destino->descripcion?></option>
	                        <?php } ?>
	                    </select>
						</div>
	            </div>
            
	            <div class="form-group">
					<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">Ciudad*</label><input type="text" maxlength="50"  class="form-control" placeholder="Ciudad" id="ciudad_add" name="ciudad_add">
					</div>
					<div style="width:45%;float: left;">
	              		<label for="nombre">Código postal*</label><input type="text"  maxlength="50" class="form-control" placeholder="Código postal" id="codigo_postal_add" name="codigo_postal_add">
					</div>
	            </div>

	            <div class="form-group">
						<label for="nombre">Leyenda*</label>

						<textarea class="form-control" rows="6" id="leyenda_add" name="leyenda_add" style="resize: none;"></textarea>
	            </div>
	            <div class="form-group">
						<label for="nombre">Declaración*</label>
						<textarea class="form-control" rows="6" id="declaracion_add" name="declaracion_add" style="resize: none;"></textarea>
	            </div>

	            <div class="form-group">
						<label for="nombre">Dirección*</label>
						<textarea class="form-control" rows="6" id="direccion_add" name="direccion_add" style="resize: none;"></textarea>
	            </div>

				<!-- SLIDER HOME -->
			
        		<div class="form-group">
					<h5 style="font-family: 'Open Sans', sans-serif;font-weight: 500;border-bottom: 1px solid #dadada;font-size: 16px;padding-bottom: 20px;">Imágenes</h5>
				</div>
				<div class="form-group">
	            	<label>Slider Home:</label>
		          	<div class="controls span3">
		          		<span class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar imagen <input type="file" id="input_slider_home_add" name ="input_slider_home_add"/>
						</span>
						<button type="button" style="display:none" class="btn btn-success pull-right" onclick="subirArchivo();"><span class="glyphicon glyphicon-floppy-disk"></span> Actualizar imagen</button>
						<br/>
						<i id="slider_home_add">No se ha seleccionado ninguna imagen</i><br/>
			            <!--i>*La extensión del archivo puede ser doc, pdf, xls, jpg, png</i-->
						   
		            </div>
				</div>

		  		<!-- HOME MINI -->
				<div class="form-group">
	            	<label>Home icono:</label>
		          	<div class="controls span3">
		          		<span class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar imagen <input type="file" id="input_home_mini_add" name ="input_home_mini_add"/>
						</span>
						<br/>
						<i id="home_mini_add">No se ha seleccionado ninguna imagen</i><br/>
		            </div>
				</div>

		  		<!-- SLIDER INTERNO A -->
				<div class="form-group">
	            	<label>Banner interno (1):</label>
		          	<div class="controls span3">
		          		<span class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar imagen <input type="file" id="input_slider_interno_a_add" name ="input_slider_interno_a_add"/>
						</span>
						<br/>
						<i id="slider_interno_a_add">No se ha seleccionado ninguna imagen</i><br/>
		            </div>
				</div>
		  	

		  		<!-- SLIDER INTERNO B -->
				<div class="form-group">
	            	<label>Banner interno (2):</label>
		          	<div class="controls span3">
		          		<span class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar imagen <input type="file" id="input_slider_interno_b_add" name ="input_slider_interno_b_add"/>
						</span>
						<br/>
						<i id="slider_interno_b_add">No se ha seleccionado ninguna imagen</i><br/>
		            </div>
				</div>

		  		<!-- SLIDER INTERNO C -->
				<div class="form-group">
	            	<label>Banner interno (3):</label>
		          	<div class="controls span3">
		          		<span class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar imagen <input type="file" id="input_slider_interno_c_add" name ="input_slider_interno_c_add"/>
						</span>
						<br/>
						<i id="slider_interno_c_add">No se ha seleccionado ninguna imagen</i><br/>
		            </div>
				</div>
		  

		  		<!-- SLIDER INTERNO D -->
				<div class="form-group">
	            	<label>Banner interno (4):</label>
		          	<div class="controls span3">
		          		<span class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar imagen <input type="file" id="input_slider_interno_d_add" name ="input_slider_interno_d_add"/>
						</span>
						<br/>
						<i id="slider_interno_d_add">No se ha seleccionado ninguna imagen</i><br/>
		            </div>
				</div>
		 

		  		<!-- SLIDER INTERNO E -->
				<div class="form-group">
	            	<label>Banner interno (5):</label>
		          	<div class="controls span3">
		          		<span class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar imagen <input type="file" id="input_slider_interno_e_add" name ="input_slider_interno_e_add"/>
						</span>
						<br/>
						<i id="slider_interno_e_add">No se ha seleccionado ninguna imagen</i><br/>
		            </div>
				</div>
		  
		  		<!-- SLIDER INTERNO F -->
				<div class="form-group">
	            	<label>Banner interno (6):</label>
		          	<div class="controls span3">
		          		<span class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar imagen <input type="file" id="input_slider_interno_f_add" name ="input_slider_interno_f_add"/>
						</span>
						<br/>
						<i id="slider_interno_f_add">No se ha seleccionado ninguna imagen</i><br/>
		            </div>
				</div>

		  	</form>
		  	<iframe id="ifrm_add" name="ifrm_add" style="display: none;"></iframe>

        </div>
        <div class="modal-footer">
        	<button type="button" class="btn btn-danger pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
			<button type="button" class="btn btn-success pull-right" onclick="guardarHotel()"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar hotel</button>
		  
		  <span id="msg_validacion"></span>
        </div>
      </div>
    </div>
  </div>



<!-- MODAL BORRAR HOTEL -->
<div class="modal fade" id="DeleteUser" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="background-color:#df4b33;padding:15px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 style="background-color:#df4b33;"><span class="glyphicon glyphicon-exclamation-sign"></span> Eliminar hotel</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
        	<input type="hidden" name="id_hotel_borrar" id="id_hotel_borrar" value="0" />
			<form role="form">
          		<p >¿Confirma que desea eliminar el hotel <b class="form-subtitle" id="nombre_hotel_borrar"></b>?</p>
			</form>
										
        </div>
        <div class="modal-footer">
        	<button type="button" class="btn btn-danger pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
        	<button type="button" class="btn btn-danger pull-right" data-dismiss="modal" onclick="borrarHotel();"><span class="glyphicon glyphicon-trash"></span> Eliminar</button>
        </div>
      </div>
	</div>
</div>

			
<script>

	$('input[name="my-checkbox"]').on('switchChange.bootstrapSwitch', function(event, state) {
		var target_id = $(this).attr('id');
		var status_ = 0;

		if(state){
			status_ = 1;
		}
		$.ajax({
	        url: "hoteles/habilitarHotel",
	        data: {id_hotel: target_id, status: status_ },
	        type: "POST",
	        async: 'true',
	        success: function(response) {
	            if (response >= 1) {
	                $.gritter.add({
				        title: 'Actualizar status',
				        text: 'Se actualizó correctamente el status.',
				        class_name: 'success'
				      });
	            } else {
	                $.gritter.add({
				        title: 'Agregar status',
				        text: 'Ocurrió un error y no se pudo actualizar el status, intentelo más tarde.',
				        class_name: 'danger'
				      });
	            }
	        },
	        error: function(x, e) {
	            $.gritter.add({
				        title: 'Actualizar status',
				        text: 'Ocurrió un error y no se pudo actualizar el status: "' + e.messager+ '"',
				        class_name: 'danger'
				});
	        }
		});

	});



	$('input[name="my-statusbox"]').on('switchChange.bootstrapSwitch', function(event, state) {
		var target_id = $(this).attr('id');
		var status_ = 0;

		if(state){
			status_ = 1;
		}
		$.ajax({
	        url: "hoteles/habilitarPaquetes",
	        data: {id_hotel: target_id, status: status_ },
	        type: "POST",
	        async: 'true',
	        success: function(response) {
	            if (response >= 1) {
	                $.gritter.add({
				        title: 'Actualizar status',
				        text: 'Se actualizó correctamente el status.',
				        class_name: 'success'
				      });
	            } else {
	                $.gritter.add({
				        title: 'Agregar status',
				        text: 'Ocurrió un error y no se pudo actualizar el status, intentelo más tarde.',
				        class_name: 'danger'
				      });
	            }
	        },
	        error: function(x, e) {
	            $.gritter.add({
				        title: 'Actualizar status',
				        text: 'Ocurrió un error y no se pudo actualizar el status: "' + e.messager+ '"',
				        class_name: 'danger'
				});
	        }
		});

	});





function actualizarHotel(){
	$("#frmHotelEdit").submit();
}


function guardarHotel(){
	$("#frmHotelAdd").submit();
}



function eliminarHotel(id, nombre){
	$("#nombre_hotel_borrar").html(nombre);
	$("#id_hotel_borrar").val(id);


	$("#DeleteUser").modal();
}

$(document).ready(function(){

	$("#ifrm_update").load(function(){
		console.log('load iframe');
	   var ifrAddS = $("#ifrm_update").contents().find("#div_resultado").html();
	   if(ifrAddS == 1){
	   		 $.gritter.add({
				title: 'Subir archivo',
				text: 'Se actualizó correctamente el hotel.',
		        class_name: 'success'
			 });
	         setTimeout ("window.location.href = 'hoteles';", 2000);
	   }else{
	   		 /*$.gritter.add({
	            title: 'Subir archivo',
	            text: 'Ocurrio un error y no se pudo agregar el archivo: ',
	            class_name: 'danger'
	         });*/
	   }
	   
	});

	$("#ifrm_add").load(function(){
		console.log('load iframe');
		var ifrAddS = $("#ifrm_add").contents().find("#div_resultado").html();
		if(ifrAddS == 1){
	   		 $.gritter.add({
				title: 'Subir archivo',
				text: 'Se guardó correctamente el hotel.',
		        class_name: 'success'
			 });
	         setTimeout ("window.location.href = 'hoteles';", 2000);
		}
	   
	});
	
	$("#input_slider_home_edit").change(function(){
		$("#slider_home").html("Seleccionado: <strong>"+$("#input_slider_home_edit").val()+"</strong>");
	});
	$("#input_home_mini_edit").change(function(){
		$("#home_mini").html("Seleccionado: <strong>"+$("#input_home_mini_edit").val()+"</strong>");
	});
	
	$("#input_slider_interno_a_edit").change(function(){
		$("#slider_interno_a").html("Seleccionado: <strong>"+$("#input_slider_interno_a_edit").val()+"</strong>");
	});
	$("#input_slider_interno_b_edit").change(function(){
		$("#slider_interno_b").html("Seleccionado: <strong>"+$("#input_slider_interno_b_edit").val()+"</strong>");
	});
	$("#input_slider_interno_c_edit").change(function(){
		$("#slider_interno_c").html("Seleccionado: <strong>"+$("#input_slider_interno_c_edit").val()+"</strong>");
	});
	$("#input_slider_interno_d_edit").change(function(){
		$("#slider_interno_d").html("Seleccionado: <strong>"+$("#input_slider_interno_d_edit").val()+"</strong>");
	});
	$("#input_slider_interno_e_edit").change(function(){
		$("#slider_interno_e").html("Seleccionado: <strong>"+$("#input_slider_interno_e_edit").val()+"</strong>");
	});
	$("#input_slider_interno_f_edit").change(function(){
		$("#slider_interno_f").html("Seleccionado: <strong>"+$("#input_slider_interno_f_edit").val()+"</strong>");
	});



	$("#input_slider_home_add").change(function(){
		$("#slider_home_add").html("Seleccionado: <strong>"+$("#input_slider_home_add").val()+"</strong>");
	});
	$("#input_home_mini_add").change(function(){
		$("#home_mini_add").html("Seleccionado: <strong>"+$("#input_home_mini_add").val()+"</strong>");
	});
	
	$("#input_slider_interno_a_add").change(function(){
		$("#slider_interno_a_add").html("Seleccionado: <strong>"+$("#input_slider_interno_a_add").val()+"</strong>");
	});
	$("#input_slider_interno_b_add").change(function(){
		$("#slider_interno_b_add").html("Seleccionado: <strong>"+$("#input_slider_interno_b_add").val()+"</strong>");
	});
	$("#input_slider_interno_c_add").change(function(){
		$("#slider_interno_c_add").html("Seleccionado: <strong>"+$("#input_slider_interno_c_add").val()+"</strong>");
	});
	$("#input_slider_interno_d_add").change(function(){
		$("#slider_interno_d_add").html("Seleccionado: <strong>"+$("#input_slider_interno_d_add").val()+"</strong>");
	});
	$("#input_slider_interno_e_add").change(function(){
		$("#slider_interno_e_add").html("Seleccionado: <strong>"+$("#input_slider_interno_e_add").val()+"</strong>");
	});
	$("#input_slider_interno_f_add").change(function(){
		$("#slider_interno_f_add").html("Seleccionado: <strong>"+$("#input_slider_interno_f_add").val()+"</strong>");
	});
});

function agregarHotel() {
	$("#ModalAgregarHotel").modal('show');
}

function borrarHotel(){
	var id_hotel_borrar = $('#id_hotel_borrar').val();
    $.ajax({
        url: "hoteles/eliminarHotel",
        data: {id_hotel: id_hotel_borrar},
        type: "POST",
        async: 'true',
        success: function(response) {
			 $.gritter.add({
				title: 'Eliminar hotel',
				text: 'Se eliminó correctamente el hotel.',
		        class_name: 'success'
			 });
	         setTimeout ("window.location.href = 'hoteles';", 2000);
        },
        error: function(x, e) {
            $.gritter.add({
			        title: 'Eliminar hotel',
			        text: 'Ocurrio un error y no se pudo eliminar el hotel, intentelo más tarde.',
			        class_name: 'danger'
			});
        }
    });


}


function editarHotel(id) {

        $.ajax({
            url: "hoteles/recuperarHotel",
            data: {id_hotel: id},
            type: "POST",
            async: 'true',
            success: function(response) {
				var dat = jQuery.parseJSON(response);
				$("#id_hotel_edit").val(dat.id_hotel);
				$("#nombre_hotel_edit").val(dat.nombre_hotel);
                $("#region_edit").val(dat.id_region);
                $("#ciudad_edit").val(dat.ciudad);
                $("#destino_edit").val(dat.id_destino);
				$("#codigo_postal_edit").val(dat.codigo_postal);
                //$("#leyenda_edit").val(dat.leyenda);
                //$("#declaracion_edit").val(dat.declaracion);
                //$("#direccion_edit").val(dat.direccion_hotel);

                tinyMCE.get('leyenda_edit').setContent( dat.leyenda );
                tinyMCE.get('declaracion_edit').setContent( dat.declaracion );
                tinyMCE.get('direccion_edit').setContent( dat.direccion_hotel );


                if( dat.imagen_slider_home!="" ){
                	$("#slider_home").html("Actual: <strong>"+dat.imagen_slider_home+"</strong>");
                	$("#input_slider_home_img").attr('src','assets/imagenes/hoteles/'+dat.imagen_slider_home);
                	$("#input_slider_home_img").show();
                }else{
                	$("#slider_home").html("No se ha seleccionado ninguna imagen");
                	$("#input_slider_home_img").hide();
                }

                if( dat.imagen_mini_home!="" ){
                	$("#home_mini").html("Actual: <strong>"+dat.imagen_mini_home+"</strong>");
                	$("#input_home_mini_img").attr('src','assets/imagenes/hoteles/'+dat.imagen_mini_home);
                	$("#input_home_mini_img").show();
                }else{
                	$("#home_mini").html("No se ha seleccionado ninguna imagen");
                	$("#input_home_mini_img").hide();
                }

                if( dat.imagen_interior_a!="" ){
                	$("#slider_interno_a").html("Actual: <strong>"+dat.imagen_interior_a+"</strong>");
                	$("#input_slider_interno_a_img").attr('src','assets/imagenes/hoteles/'+dat.imagen_interior_a);
                	$("#input_slider_interno_a_img").show();
                }else{
                	$("#slider_interno_a").html("No se ha seleccionado ninguna imagen");
                	$("#input_slider_interno_a_img").hide();
                }
                if( dat.imagen_interior_b!="" ){
                	$("#slider_interno_b").html("Actual: <strong>"+dat.imagen_interior_b+"</strong>");
                	$("#input_slider_interno_b_img").attr('src','assets/imagenes/hoteles/'+dat.imagen_interior_b);
                	$("#input_slider_interno_b_img").show();
                }else{
                	$("#slider_interno_b").html("No se ha seleccionado ninguna imagen");
                	$("#input_slider_interno_b_img").hide();
                }
                if( dat.imagen_interior_c!="" ){
                	$("#slider_interno_c").html("Actual: <strong>"+dat.imagen_interior_c+"</strong>");
                	$("#input_slider_interno_c_img").attr('src','assets/imagenes/hoteles/'+dat.imagen_interior_c);
                	$("#input_slider_interno_c_img").show();
                }else{
                	$("#slider_interno_c").html("No se ha seleccionado ninguna imagen");
                	$("#input_slider_interno_c_img").hide();
                }
                if( dat.imagen_interior_d!="" ){
                	$("#slider_interno_d").html("Actual: <strong>"+dat.imagen_interior_d+"</strong>");
                	$("#input_slider_interno_d_img").attr('src','assets/imagenes/hoteles/'+dat.imagen_interior_d);
                	$("#input_slider_interno_d_img").show();
                }else{
                	$("#slider_interno_d").html("No se ha seleccionado ninguna imagen");
                	$("#input_slider_interno_d_img").hide();
                }
                if( dat.imagen_interior_e!="" ){
                	$("#slider_interno_e").html("Actual: <strong>"+dat.imagen_interior_e+"</strong>");
                	$("#input_slider_interno_e_img").attr('src','assets/imagenes/hoteles/'+dat.imagen_interior_e);
                	$("#input_slider_interno_e_img").show();
                }else{
                	$("#slider_interno_e").html("No se ha seleccionado ninguna imagen");
                	$("#input_slider_interno_e_img").hide();
                }
                if( dat.imagen_interior_f!="" ){
                	$("#slider_interno_f").html("Actual: <strong>"+dat.imagen_interior_f+"</strong>");
                	$("#input_slider_interno_f_img").attr('src','assets/imagenes/hoteles/'+dat.imagen_interior_f);
                	$("#input_slider_interno_f_img").show();
                }else{
                	$("#slider_interno_f").html("No se ha seleccionado ninguna imagen");
                	$("#input_slider_interno_f_img").hide();
                }

				$("#ModalEditUser").modal('show');
            },
            error: function(x, e) {
                $.gritter.add({
				        title: 'Editar hotel',
				        text: 'Ocurrio un error y no se pueden recuperar los datos del hotel, intentelo más tarde.',
				        class_name: 'danger'
				});
            }
        });
}

</script>
