<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>JOYAS SOFT 3.0 - Administrador</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
		<script src="../assets/js/notify.js"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
		<link href="<?=base_url()?>assets/css/login.css" rel="stylesheet" type="text/css" />
		<style>
			.modal-header, h4, .close {
				background-color: #5cb85c;
				color:white !important;
				text-align: center;
				font-size: 30px;
			}
		.modal-footer {
			background-color: #f9f9f9;
		}
		.btn-link{
			color: #FFFFFF;
		}
		.modal-dialog {
			top: 80px;
		}
		.form-subtitle {
			font-weight: 700;
		}
		</style>
	</head>



<body>


  <div id="form-div">
      	<!--div align="center" >
            <img src="http://104.236.244.76/assets/maqueta//img/logo.svg" style="width: 90px; height: 86px; top: 10px;>
            
        </div-->
        <span id="titulo_login">
        	<img src="<?=base_url()?>assets/images/logo_joyas_admin.png"/>

        </span>

        <span id="menu_login">
        		<p>Administrador de contenidos</p>
        		
        </span>
        <span id="menu_login_button">
        		<p><b>Haga click aquí para ingresar</b></p>
        		
        </span>
  </div>



  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="padding:35px 50px;background-color:#fff">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 style="background-color:#fff;color:#58585a;"><span style="color:#58585a;" class="glyphicon glyphicon-lock"></span> <span style="color:#58585a;">Login</span></h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
          <form role="form">
            <div class="form-group" style="color:#58585a">
              <label for="usrname"><span class="glyphicon glyphicon-user"></span> Nombre de usuario</label>
              <input type="text" class="form-control" id="usrname" placeholder="Nombre de usuario" onkeypress="c_enter(event);">
            </div>
            <div class="form-group">
              <label for="psw"><span class="glyphicon glyphicon-eye-open"></span> Contraseña</label>
              <input type="password" class="form-control" id="psw" placeholder="Contraseña" onkeypress="c_enter(event);">
            </div>
              <button style="background-color:#b08a16;color:#fff;border-color: #b08a16;" type="button" onclick="enviar();" class="btn btn-success btn-block"><span class="glyphicon glyphicon-ok"></span> ENTRAR</button>
          </form>
        </div>
        <div class="modal-footer" >
          <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
          <p style="color:#58585a"><a onclick="formPassword()">No puedo acceder a mi cuenta</a></p>
        </div>
      </div>
      
    </div>
  </div>




  <!-- Modal -->
  <div class="modal fade" id="forgetPass" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding:35px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4><span class="glyphicon glyphicon-lock"></span> Inicio de sesión</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
          <form role="form">

          	<p class="form-subtitle">¿Por qué no puedes iniciar sesión?</p>


            
            <div class="checkbox">
              <label><input type="checkbox" value="1" checked>He olvidado mi contraseña</label><br/>
              <label><input type="checkbox" value="2" checked>Sé mi contraseña pero no puedo iniciar sesión</label><br/>
              <label><input type="checkbox" value="3" checked>Creo que otra persona está usando mi cuenta</label>
            </div>

          </form>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
          <button type="button" class="btn btn-success btn-default pull-right" data-dismiss="modal" onclick="formSendPassword()"><span class="glyphicon glyphicon-ok"></span> Continuar</button>
        </div>
      </div>
    </div>
  </div>


  <!-- Modal -->
  <div class="modal fade" id="sendPassword" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding:35px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4><span class="glyphicon glyphicon-lock"></span> Restablece tu contraseña</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
          <form role="form">

          	<p class="form-subtitle">Podemos ayudarte a restablecer tu contraseña, por favor ingresa tu cuenta <b>Experto</b></p>


            <div class="form-group">
              
              <input type="text" class="form-control" id="usrname" placeholder="Ingresa tu cuenta Experto">
            </div>
            
          </form>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
          <button type="button" onclick="sendPass()" class="btn btn-success btn-default pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-ok"></span> Finalizar</button>
        </div>
      </div>
      
    </div>
  </div>

<script>

$(document).ready(function(){
    $("#menu_login_button").click(function(){
        $("#myModal").modal();
    });
});

function formPassword(){
	$("#myModal").modal("hide");
	$("#forgetPass").modal();
}


function formSendPassword(){
	$("#forgetPass").modal("hide");
	$("#sendPassword").modal();
}

function sendPass(){
	$("#sendPassword").modal("hide");
	$.notify("Se ha enviado una nueva contraseña a tu correo, por favor verifica\ny sigue las instrucciones para personalizar tu contraseña", "success");
}


function sp(t){
	if(t == 1){
		$("#log_").hide();
		$("#recuperar_pass").fadeIn(400);
	}else{
		$("#recuperar_pass").hide();
		$("#log_").fadeIn(400);
	}
	
}
//global vars
	var nombre = $("#usrname");
	var pass = $("#psw");
	var recover_m = $("#nombre_rec");
	//On blur
	nombre.blur(validaNombre);
	pass.blur(validaPass);
	recover_m.blur(validaNombreRec);
	//On key press
	nombre.keyup(validaNombre);
	pass.keyup(validaPass);
	recover_m.keyup(validaNombreRec);
	
	
	function validaNombre(){
		//if it's NOT valid
		if(nombre.val().length < 2 || nombre.val() == null || /^\s+$/.test(nombre.val())){
			nombre.addClass("error");
			return false;
		}
		//if it's valid
		else{
			nombre.removeClass("error");
			return true;
		}
	}
	
	function validaNombreRec(){
		//if it's NOT valid
		if(recover_m.val().length < 2 || recover_m.val() == null || /^\s+$/.test(recover_m.val())){
			recover_m.addClass("error");
			return false;
		}
		//if it's valid
		else{
			recover_m.removeClass("error");
			return true;
		}
	}
	
	function validaPass(){
		//if it's NOT valid
		if(pass.val().length < 2 || pass.val() == null || /^\s+$/.test(pass.val())){
			pass.addClass("error");
			return false;
		}
		//if it's valid
		else{
			pass.removeClass("error");
			return true;
		}
	}
	
	
function c_enter(caracter) { //:::::::::::: funcion para detectar la tecla enter y loggear
	letra = caracter.which || event.keyCode;
	l = String.fromCharCode(letra);
	if(letra == 13){
		submit();
	}
}

function c_enter_r(caracter) { //:::::::::::: funcion para detectar la tecla enter y loggear
	letra = caracter.which || event.keyCode;
	l = String.fromCharCode(letra);
	if(letra == 13){
		recover();
	}
}

function enviar(){
	if(validaNombre()& validaPass() ){
           //console.log("user: "+nombre.val()+" --- password: "+pass.val());
    		$.ajax({
				data:  {username: nombre.val(), password: pass.val()},
				url: "<?php echo base_url('home/log') ?>",
				type:  'post',
				success:  function (response) {
					console.log(response);
	                if(response == 1){
	   	            	window.location.reload();
	                }else{
	                	//alert("Los datos son incorrectos");
	                	$.notify("Los datos proporcionados son incorrectos", "error");
                    }
				}
			});
    }
}


function submit(){
        if(validaNombre()& validaPass() ){
           //console.log("user: "+nombre.val()+" --- password: "+pass.val());
    		$.ajax({
				data:  {username: nombre.val(), password: pass.val()},
				url: "<?php echo base_url('home/log') ?>",
				type:  'post',
				success:  function (response) {
					console.log(response);
	                if(response == 1){
	   	            	//window.location.reload();
	                }else{
	                	//alert("Los datos son incorrectos");
	                	$.notify("Los datos proporcionados son incorrectos", "error");
                    }
				}
			});
    	}
}

function showLogin(){
	$('#modalEditarLead').modal("show");

}
function recover(){
        if(validaNombreRec()){
           //console.log("user: "+nombre.val()+" --- password: "+pass.val());
    		$.ajax({
						data:  {correo: $("#nombre_rec").val()},
						url: "<?php echo base_url('login_controller/get_usuario') ?>",
						type:  'post',
						beforeSend: function () {
							//console.log('Procesando, espere por favor...');
						},
						success:  function (response) {
							console.log(response);
							if(response == '2'){
								$.notify("El usuario que ingreso no existe!", "error"); 
							}else{
								obj = JSON.parse(response);
							        $.ajax({
											data:  {id_usuario: obj.id_usuario, nombre: obj.nombre, correo: obj.username},
											url: "<?php echo base_url('login_controller/recover') ?>",
											type:  'post',
											beforeSend: function () {
												//$("#res").html('Procesando, espere por favor...');
											},
											success:  function (response) {
												//console.log(response);
												var matriz = response.split("-");
												if(matriz[1] != "no"){
													$("#recuperar_pass").html('<br><br><h1 class="text_registration" >Se ha mandado un correo con su contraseña. </span><br><br></h1>');
													setTimeout ("window.location.reload();", 2300);
												}
											}
									});
							}
						}
				});
    	}
}


</script> 
    
    
</body>
</html>


   
