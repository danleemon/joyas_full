	<?php 
$visible='';
$editable='';
if($this->session->userdata('perfil')=='vista'){
	$editable=' disabled="disabled" ';
	$visible=' style="display:none" ';

}
?>
<div class="row">
	<div class="col-md-12">
		<div class="block-flat">
			<div class="header">							
				<h3><i class="fa fa-tags"></i> <strong>Banners Home</strong></h3>
			</div>
			<div class="content">
				<div class="table-responsive" style="overflow-x: none;">
					<table class="table table-bordered" id="datatable_hoteles" >
						<thead>
							<tr>
								<th>Nombre Banner</th>
								<th>Ubicación CTA</th>
								<th>Paquete</th>
								<th>Fecha de registro</th>
								
								<?php if($this->session->userdata('perfil')!='vista'){ ?>
								<th>Publicada</th>
								<th>Acciones</th>
								<?php } ?>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($promociones_result as $paquete_row){ ?>
								<tr class="odd gradeX">
									<td><?=$paquete_row->encabezado?></td>
									<td><?php
										if($paquete_row->tipo_layout==1){
											echo "Izquierda";
										}else{
											echo "Derecha";
										}
									?></td>
									<td><?=$paquete_row->nombre_paquete?></td>
									<td><?=$paquete_row->fecha_registro?></td>
									<?php if($this->session->userdata('perfil')!='vista'){ ?>
									<td style="text-align:center"><input type="checkbox" name="my-checkbox" id="<?=$paquete_row->id_promo_especial?>" <?php if($paquete_row->publicado == 1){ echo "checked";}?>></td>
									<td style="text-align:center">
										<button type="button" title="Editar paquete" class="btn btn-primary btn-sm" data-dismiss="modal" onclick="editarPaquete(<?=$paquete_row->id_promo_especial?>);"><span class="glyphicon glyphicon-pencil"></span> </button>
										<button type="button" title="Eliminar paquete" class="btn btn-danger btn-sm" data-dismiss="modal" onclick="eliminarPaquete(<?=$paquete_row->id_promo_especial?>, '<?=$paquete_row->encabezado?>');"><span class="glyphicon glyphicon-trash"></span> </button>
									</td>
									<?php } ?>
								</tr>
							<?php }?>
										
						</tbody>
					</table>			
				</div>

				<button type="button" title="Agregar nueva experiencia" class="btn btn-info btn-sm" data-dismiss="modal" onclick="agregarPaquete();"><span class="glyphicon glyphicon-plus"></span> Agregar nuevo banner</button>

			</div>
		</div>				
	</div>
</div>


<!-- MODAL EDITAR PAQUETE -->
 <div class="modal fade" id="ModalEditUser" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="padding:15px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4><span class="glyphicon glyphicon-pencil"></span> Editar banner</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">

			<form role="form" id="frmHotelEdit" name="frmHotelEdit" action="actualizarPromoEspecial" method="POST" enctype="multipart/form-data"  target="ifrm_update">

				<input type="hidden" name="id_promo_especial_edit" id="id_promo_especial_edit" value="0" />

				<div class="form-group">
					<h5 style="font-family: 'Open Sans', sans-serif;font-weight: 500;border-bottom: 1px solid #dadada;font-size: 16px;padding-bottom: 20px;">Datos generales</h5>
				</div>
              
	           <div class="form-group">
	              <label for="nombre">Título*</label><input type="text"  maxlength="50" class="form-control" placeholder="Título" id="titulo_experiencia_edit" name="titulo_experiencia_edit">
	            </div>

	            <div class="form-group">
						<label for="nombre">Descripción*</label>
						<textarea class="form-control" <?=$editable?>rows="6" id="texto_banner_edit" name="texto_banner_edit" style="resize: none;"></textarea>
	            </div>

	            <div class="form-group">
						<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">Ubicación de CTA</label>
		              	<select class="form-control" id="id_layout_edit" name="id_layout_edit">
		              		<option value="1">Izquierda</option>
		              		<option value="2">Derecha</option>
		              		<option value="3">Oculto</option>
		              	</select>
						</div>
						<div style="width:45%;float: left;">
	              		<label for="nombre">Orden*</label>
		              	<input type="text"  maxlength="2" class="form-control onlynumbers" placeholder="Orden" id="orden_edit" name="orden_edit">
						</div>
	            </div>

            	<!-- IMAGEN DETALLE -->
				<div class="form-group">
	            	<label>Imagen:</label>
		          	<div class="controls span3">
		          		<span class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar nueva imagen <input type="file" id="input_imagen_completo_edit" name ="input_imagen_completo_edit"/>
						</span>
						<br/>
						<i id="home_mini_edit">No se ha seleccionado ninguna imagen</i><br/>
		            </div>
				</div>
         
	            <div class="form-group">
	              	<label for="nombre">Hotel - Paquete*</label>
	              	<select class="form-control" id="id_paquete_edit" name="id_paquete_edit">
	                    <option value="0"> Seleccione una opción</option>
	                   
	                    <?php foreach($paquetes as $paquete){ ?>
	                    <option value="<?=$paquete->id_paquete?>"><?=$paquete->nombre_hotel?> - <?=$paquete->nombre_paquete?></option>
	                    <?php } ?>
	                 
	                </select>
	            </div>

	   

		  	</form>
		  	<iframe id="ifrm_update" name="ifrm_update" style="display: none;"></iframe>

        </div>
        <div class="modal-footer">
        	<button type="button" class="btn btn-danger pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
			<button type="button" class="btn btn-success pull-right" onclick="actualizarPaquete()"><span class="glyphicon glyphicon-floppy-disk"></span> Actualizar banner</button>
		  
		  <span id="msg_validacion"></span>
        </div>
      </div>
    </div>
  </div>







<!-- MODAL AGREGAR HOTEL -->
 <div class="modal fade" id="ModalAgregarHotel" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="padding:15px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4><span class="glyphicon glyphicon-pencil"></span> Agregar banner home</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">

			<form role="form" id="frmHotelAdd" name="frmHotelAdd" action="guardarPaquete" method="POST" enctype="multipart/form-data"  target="ifrm_add">

				<div class="form-group">
					<h5 style="font-family: 'Open Sans', sans-serif;font-weight: 500;border-bottom: 1px solid #dadada;font-size: 16px;padding-bottom: 20px;">Datos generales</h5>
				</div>
              
	            <div class="form-group">
	              <label for="nombre">Título*</label><input type="text"  maxlength="50" class="form-control" placeholder="Título" id="titulo_experiencia_add" name="titulo_experiencia_add">
	            </div>

	            <div class="form-group">
						<label for="nombre">Declaración*</label>
						<textarea class="form-control" <?=$editable?>rows="6" id="texto_banner_add" name="texto_banner_add" style="resize: none;"></textarea>
	            </div>

	            <div class="form-group">
						<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">Ubicación de CTA</label>
		              	<select class="form-control" id="id_layout_add" name="id_layout_add">
		              		<option value="1">Izquierda</option>
		              		<option value="2">Derecha</option>
		              		<option value="3">Oculto</option>
		              	</select>
						</div>
						<div style="width:45%;float: left;">
	              		<label for="nombre">Orden*</label>
		              	<input type="text"  maxlength="2" class="form-control onlynumbers" placeholder="Orden" id="orden_add" name="orden_add">
						</div>
	            </div>
	  
            	<!-- IMAGEN DETALLE -->
				<div class="form-group">
	            	<label>Imagen:</label>
		          	<div class="controls span3">
		          		<span class="btn btn-info btn-file">
							<span class="glyphicon glyphicon-arrow-up"></span> Seleccionar nueva imagen <input type="file" id="input_imagen_completo_add" name ="input_imagen_completo_add"/>
						</span>
						<br/>
						<i id="home_mini_add">No se ha seleccionado ninguna imagen</i><br/>
		            </div>
				</div>
         
	            <div class="form-group">
	              	<label for="nombre">Hotel - Paquete*</label>
	              	<select class="form-control" id="id_paquete_add" name="id_paquete_add">
	                    <option value="0"> Seleccione una opción</option>
	                   
	                    <?php foreach($paquetes as $paquete){ ?>
	                    <option value="<?=$paquete->id_paquete?>"><?=$paquete->nombre_hotel?> - <?=$paquete->nombre_paquete?></option>
	                    <?php } ?>
	                 
	                </select>
	            </div>
		  


		  	</form>
		  	<iframe id="ifrm_add" name="ifrm_add" style="display: none;"></iframe>

        </div>
        <div class="modal-footer">
        	<button type="button" class="btn btn-danger pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
			<button type="button" class="btn btn-success pull-right" onclick="guardarPaquete()"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar banner</button>
		  
		  <span id="msg_validacion"></span>
        </div>
      </div>
    </div>
  </div>



<!-- MODAL BORRAR HOTEL -->
<div class="modal fade" id="DeleteUser" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="background-color:#df4b33;padding:15px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 style="background-color:#df4b33;"><span class="glyphicon glyphicon-exclamation-sign"></span> Eliminar banner</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
        	<input type="hidden" name="id_paquete_borrar" id="id_paquete_borrar" value="0" />
			<form role="form">
          		<p >¿Confirma que desea eliminar el banner <b class="form-subtitle" id="nombre_paquete_borrar"></b>?</p>
			</form>
										
        </div>
        <div class="modal-footer">
        	<button type="button" class="btn btn-danger pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
        	<button type="button" class="btn btn-danger pull-right" data-dismiss="modal" onclick="borrarPaquete();"><span class="glyphicon glyphicon-trash"></span> Eliminar</button>
        </div>
      </div>
	</div>
</div>

			
<script>

	$('input[name="my-checkbox"]').on('switchChange.bootstrapSwitch', function(event, state) {
		var target_id = $(this).attr('id');
		var status_ = 0;

		if(state){
			status_ = 1;
		}
		$.ajax({
	        url: "habilitarPromocionEspecial",
	        data: {id_promo_especial: target_id, status: status_ },
	        type: "POST",
	        async: 'true',
	        success: function(response) {
	            if (response >= 1) {
	                $.gritter.add({
				        title: 'Actualizar status',
				        text: 'Se publicó correctamente la experiencia.',
				        class_name: 'success'
				      });
	            } else {
	                $.gritter.add({
				        title: 'Agregar status',
				        text: 'Ocurrió un error y no se pudo actualizar el status, intentelo más tarde.',
				        class_name: 'danger'
				      });
	            }
	        },
	        error: function(x, e) {
	            $.gritter.add({
				        title: 'Actualizar status',
				        text: 'Ocurrió un error y no se pudo actualizar el status: "' + e.messager+ '"',
				        class_name: 'danger'
				});
	        }
		});

	});


function actualizarPaquete(){
	$("#frmHotelEdit").submit();
}


function guardarPaquete(){
	$("#frmHotelAdd").submit();
}



function eliminarPaquete(id, nombre){
	$("#nombre_paquete_borrar").html(nombre);
	$("#id_paquete_borrar").val(id);


	$("#DeleteUser").modal();
}

$(document).ready(function(){

	$("#id_hotel_add").change(function(){

		 $.ajax({
            url: "paquetes/recuperarHabitaciones",
            data: {id_hotel: $(this).val() },
            type: "POST",
            async: 'true',
            success: function(response) {
				console.log(response);
				$("#id_habitacion_add").html('<option value="0"> Seleccione una opción</option>'+response);

            },
            error: function(x, e) {
               console.log('Error');
            }
        });

	});

	$("#ifrm_update").load(function(){
		console.log('load iframe');
	   var ifrAddS = $("#ifrm_update").contents().find("#div_resultado").html();
	   if(ifrAddS == 1){
	   		 $.gritter.add({
				title: 'Subir archivo',
				text: 'Se actualizó correctamente el banner.',
		        class_name: 'success'
			 });
	         setTimeout ("window.location.href = 'https://www.joyasdemexico.mx/administrador/promo_home/';", 2000);
	   }else{
	   		 /*$.gritter.add({
	            title: 'Subir archivo',
	            text: 'Ocurrio un error y no se pudo agregar el archivo: ',
	            class_name: 'danger'
	         });*/
	   }
	   
	});

	$("#ifrm_add").load(function(){
		console.log('load iframe');
		var ifrAddS = $("#ifrm_add").contents().find("#div_resultado").html();
		if(ifrAddS == 1){
	   		 $.gritter.add({
				title: 'Subir archivo',
				text: 'Se guardó correctamente el banner.',
		        class_name: 'success'
			 });
	         setTimeout ("window.location.href = 'https://www.joyasdemexico.mx/administrador/promo_home/';", 2000);
		}
	   
	});
	

	$("#input_imagen_edit").change(function(){
		$("#home_mini_edit").html("Seleccionado: <strong>"+$("#input_imagen_edit").val()+"</strong>");
	});
	

	$("#input_imagen_add").change(function(){
		$("#home_mini_add").html("Seleccionado: <strong>"+$("#input_imagen_add").val()+"</strong>");
	});
	
	
});

function agregarPaquete() {
	$("#ModalAgregarHotel").modal('show');
}

function borrarPaquete(){
	var id_paquete_borrar = $('#id_paquete_borrar').val();
    $.ajax({
        url: "eliminarPaquete",
        data: {id_paquete: id_paquete_borrar},
        type: "POST",
        async: 'true',
        success: function(response) {
			 $.gritter.add({
				title: 'Eliminar banner',
				text: 'Se eliminó correctamente el banner.',
		        class_name: 'success'
			 });
	         setTimeout ("window.location.href = 'hhttps://www.joyasdemexico.mx/administrador/promo_home/';", 2000);
        },
        error: function(x, e) {
            $.gritter.add({
			        title: 'Eliminar banner',
			        text: 'Ocurrio un error y no se pudo eliminar el banner, intentelo más tarde.',
			        class_name: 'danger'
			});
        }
    });


}


function editarPaquete(id) {

        $.ajax({
            url: "recuperarPromocion",
            data: {id_promo_especial: id},
            type: "POST",
            async: 'true',
            success: function(response) {
				var dat = jQuery.parseJSON(response);
				$("#id_promo_especial_edit").val(dat.id_promo_especial);
				$("#titulo_experiencia_edit").val(dat.encabezado);
				$("#id_layout_edit").val(dat.tipo_layout);
				$("#id_paquete_edit").val(dat.id_paquete);
				$("#orden_edit").val(dat.orden);
  				tinyMCE.get('texto_banner_edit').setContent( dat.texto_banner );
				

                if( dat.imagen_banner!="" ){
                	$("#home_mini_edit").html("Actual: <strong>"+dat.imagen_banner+"</strong>");
                	$("#input_imagen_completo_edit").attr('src','assets/imagenes/paquetes/'+dat.imagen_banner);
                	$("#input_imagen_completo_edit").show();
                }else{
                	$("#home_mini_edit").html("No se ha seleccionado ninguna imagen");
                	//$("#input_imagen_completo_edit").hide();
                }

				$("#ModalEditUser").modal('show');
            },
            error: function(x, e) {
                $.gritter.add({
				        title: 'Editar paquete',
				        text: 'Ocurrio un error y no se pueden recuperar los datos del paquete, intentelo más tarde.',
				        class_name: 'danger'
				});
            }
        });
}

</script>
