<?php 
$visible='';
$editable='';
if($this->session->userdata('perfil')=='vista'){
	$editable=' disabled="disabled" ';
	$visible=' style="display:none" ';

}
?>
<div class="row">
	<div class="col-md-12">
		<div class="block-flat">
			<div class="header">							
				<h3><i class="fa fa-calendar"></i> <strong>Reservaciones confirmadas</strong></h3>
			</div>
			<div class="content">
				<div class="table-responsive" style="overflow-x: none;">

					<button title="Descargar excel" type="button" class="btn btn-info pull-left" data-dismiss="modal" onclick="descarga_excel_reservaciones()"><span class="glyphicon glyphicon-arrow-down"></span> Descargar excel</button>

					<table class="table table-bordered" id="datatable_hoteles" >
						<thead>
							<tr>
								<th>Clave de reservación</th>
								<th>Cliente</th>
								<th>Hotel</th>
								<th>Paquete</th>
								<th>Fecha de registro</th>
								<th>Estatus</th>
								<?php if($this->session->userdata('perfil')!='vista'){ ?>
								<th>Acciones</th>
								<?php } ?>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($reservaciones_result as $reservacion_row){ ?>
								<tr class="odd gradeX">
									<td><?=$reservacion_row->clave_reservacion?></td>
									<td><?=$reservacion_row->nombre_cliente?> <?=$reservacion_row->apellidos_cliente?></td>
									<td><?=$reservacion_row->nombre_hotel?></td>
									<td><?=$reservacion_row->nombre_paquete?></td>
									<td><?=$reservacion_row->fecha_registro?></td>
									<td><?=$reservacion_row->status_reservacion?></td>
									<?php if($this->session->userdata('perfil')!='vista'){ ?>
									<td style="text-align:center">
										<button type="button" title="Ver detalle de solicitud" class="btn btn-primary btn-sm" data-dismiss="modal" onclick="verReservacion(<?=$reservacion_row->id_reservacion?>);"><span class="glyphicon glyphicon-search"></span> </button>
										<!--button type="button" title="Eliminar paquete" class="btn btn-danger btn-sm" data-dismiss="modal" onclick="eliminarPaquete(<?=$reservacion_row->id_reservacion?>, '<?=$reservacion_row->id_reservacion?>');"><span class="glyphicon glyphicon-trash"></span> </button-->
									</td>
									<?php } ?>
								</tr>
							<?php }?>
										
						</tbody>
					</table>			
				</div>


			</div>
		</div>				
	</div>
</div>


<!-- MODAL EDITAR PAQUETE -->
 <div class="modal fade" id="ModalEditUser" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="padding:15px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4><span class="glyphicon glyphicon-search"></span> Reservación confirmada</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">

			<form role="form" id="frmHotelEdit" name="frmHotelEdit" action="reservaciones/actualizarReservacion" method="POST" enctype="multipart/form-data"  target="ifrm_update">

				<input type="hidden" name="id_reservacion_edit" id="id_reservacion_edit" value="0" />

				<div class="form-group">
					<h5 style="font-family: 'Open Sans', sans-serif;font-weight: 500;border-bottom: 1px solid #dadada;font-size: 16px;padding-bottom: 20px;">Datos generales</h5>
				</div>
              

	            <div class="form-group">
						<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">Clave de reservación*</label><input type="text" maxlength="50"  class="form-control" placeholder="Precio" id="clave_reservacion_edit" name="clave_reservacion_edit">
						</div>
						<div style="width:45%;float: left;">
	              		<label for="nombre">Paquete*</label>
	              		<select class="form-control" id="id_paquete_edit" name="id_paquete_edit">
	                        <option value="0"> Seleccione una opción</option>
	                        <?php foreach($paquetes as $paquete){ ?>
	                        <option value="<?=$paquete->id_paquete?>"><?=$paquete->nombre_paquete?></option>
	                        <?php } ?>
	                    </select>
						</div>
	            </div>
            <br/><br/>
	            <div class="form-group">
		            <label style="width:100%" for="nombre">Nombre del cliente*</label><br/>
					<div style="float: left;margin-right: 2%;width: 8%;">
	              		<input type="text" maxlength="5"  class="form-control" placeholder="Título del cliente" id="titulo_cliente_edit" name="titulo_cliente_edit"> 
					</div>
					<div style="float: left;width: 40%;">
	              		<input type="text" maxlength="40"  class="form-control" placeholder="Nombre del cliente" id="nombre_cliente_edit" name="nombre_cliente_edit">
					</div>
					
					
					<div style="width:45%;margin-left: 5%;float: left;">
	              		<input type="text"  maxlength="50" class="form-control" placeholder="Apellidos del cliente" id="apellidos_cliente_edit" name="apellidos_cliente_edit">
					</div>
	            </div>
	            
	            <div class="form-group">
						<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">Tipo de habitación*</label>
	              		<select class="form-control" id="id_tipo_habitacion_edit" name="id_tipo_habitacion_edit">
	                        <option value="0"> Seleccione una opción</option>
	                        <?php foreach($tipos_habitacion as $habitacion){ ?>
	                        <option value="<?=$habitacion->id_habitacion?>"><?=$habitacion->nombre_habitacion?></option>
	                        <?php } ?>
	                    </select>
						</div>
						<div style="width:45%;float: left;">
	              		<label for="nombre">Estatus de reservación*</label>
	              		<select class="form-control" id="id_status_reservacion_edit" name="id_status_reservacion_edit">
	                        <option value=""> Seleccione una opción</option>
	                        <?php foreach($status_reservaciones as $estatus){ ?>
	                        <option value="<?=$estatus->id_status_reservacion?>"><?=$estatus->descripcion?></option>
	                        <?php } ?>
	                    </select>
						</div>
	            </div>
	            
	            <div class="form-group">
	              <label for="nombre">País*</label><input type="text" maxlength="50" class="form-control" placeholder="País" id="pais_cliente_edit" name="pais_cliente_edit">
	            </div>

	             <div class="form-group">
					<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">Ciudad*</label><input type="text"  maxlength="50"  class="form-control" placeholder="Ciudad" id="ciudad_cliente_edit" name="ciudad_cliente_edit">
					</div>
					<div style="width:45%;float: left;">
	              		<label for="nombre">Estado*</label><input type="text" maxlength="50" class="form-control" placeholder="Estado" id="estado_cliente_edit" name="estado_cliente_edit">
					</div>
	            </div>
	            


	            <div class="form-group">
					<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">Delegacion*</label><input type="text" maxlength="50"  class="form-control" placeholder="Delegacion" id="delegacion_cliente_edit" name="delegacion_cliente_edit">
					</div>
					<div style="width:45%;float: left;">
	              		<label for="nombre">Código postal*</label><input type="text"  maxlength="50" class="form-control" placeholder="Código postal" id="codigo_postal_cliente_edit" name="codigo_postal_cliente_edit">
					</div>
	            </div>

	            <div class="form-group">
						<label for="nombre">Dirección*</label>
						<textarea class="form-control" rows="6" id="direccion_cliente_edit" name="direccion_cliente_edit" style="resize: none;"></textarea>
	            </div>
	            
	            <div class="form-group">
					<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">Fecha de nacimiento*</label><input type="text" maxlength="50"  class="form-control" placeholder="Fecha de nacimiento" id="fecha_nacimiento_cliente_edit" name="fecha_nacimiento_cliente_edit">
					</div>
					<div style="width:45%;float: left;">
	              		<label for="nombre">Correo*</label><input type="text" maxlength="50" class="form-control" placeholder="Correo" id="correo_cliente_edit" name="correo_cliente_edit">
					</div>
	            </div>

	            <div class="form-group">
					<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">Teléfono*</label><input type="text" maxlength="50"  class="form-control" placeholder="Teléfono" id="telefono_cliente_edit" name="telefono_cliente_edit">
					</div>
					<div style="width:45%;float: left;">
	              		<label for="nombre">Móvil*</label><input type="text" maxlength="50" class="form-control" placeholder="Móvil" id="movil_cliente_edit" name="movil_cliente_edit">
					</div>
	            </div>
	           

	             <div class="form-group">
					<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">Tipo de tarjeta*</label>
	              		<select class="form-control" id="id_tipo_tarjeta_edit" name="id_tipo_tarjeta_edit">
	                        <option value=""> Seleccione una opción</option>
	                        <?php foreach($tipos_tarjeta as $tarjeta){ ?>
	                        <option value="<?=$tarjeta->id_tipo_tarjeta?>"><?=$tarjeta->descripcion?></option>
	                        <?php } ?>
	                    </select>
					</div>
					<div style="width:45%;float: left;">
	              		<label for="nombre">Titular*</label><input type="text"  maxlength="50" class="form-control" placeholder="Titular" id="titular_tarjeta_edit" name="titular_tarjeta_edit">
					</div>
	            </div>

	             <div class="form-group">
					<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">Número de tarjeta*</label><input type="text" maxlength="50"  class="form-control" placeholder="Número de tarjeta" id="numero_tarjeta_edit" name="numero_tarjeta_edit">
					</div>
					<div style="width:45%;float: left;">
	              		<label for="nombre">Código de seguridad*</label><input type="text"  maxlength="50" class="form-control" placeholder="Código de seguridad" id="codigo_seguridad_edit" name="codigo_seguridad_edit">
					</div>
	            </div>

	             <div class="form-group">
					<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">Vencimiento (mes)*</label><input type="text" maxlength="50"  class="form-control" placeholder="Vencimiento (mes)" id="fecha_vencimiento_mes_edit" name="fecha_vencimiento_mes_edit">
					</div>
					<div style="width:45%;float: left;">
	              		<label for="nombre">Vencimiento (año)*</label><input type="text"  maxlength="50" class="form-control" placeholder="Vencimiento (año)" id="fecha_vencimiento_anio_edit" name="fecha_vencimiento_anio_edit">
					</div>
	            </div>

	            <div class="form-group">
					<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">Fecha de llegada*</label><input type="text" maxlength="50"  class="form-control" placeholder="Fecha de llegada" id="fecha_llegada_edit" name="fecha_llegada_edit">
					</div>
					<div style="width:45%;float: left;">
	              		<label for="nombre">Hora de llegada*</label><input type="text"  maxlength="50" class="form-control" placeholder="Hora de llegada" id="hora_llegada_edit" name="hora_llegada_edit">
					</div>
	            </div>


	            <div class="form-group">
					<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">Fecha de salida*</label><input type="text" maxlength="50"  class="form-control" placeholder="Fecha de salida" id="fecha_salida_edit" name="fecha_salida_edit">
					</div>
					<div style="width:45%;float: left;">
	              		<label for="nombre">Hora de salida*</label><input type="text" maxlength="50" class="form-control" placeholder="Hora de salida" id="hora_salida_edit" name="hora_salida_edit">
					</div>
	            </div>

	            <div class="form-group">
					<div style="float: left;margin-right: 5%;width: 50%;">
	              		<label for="nombre">Número de adultos*</label><input type="text" maxlength="50" class="form-control" placeholder="Número de adultos" id="numero_adultos_edit" name="numero_adultos_edit">
					</div>
					<div style="width:45%;float: left;">
	              		<label for="nombre">Número de menores*</label><input type="text"  maxlength="50" class="form-control" placeholder="Número de menores" id="numero_menores_edit" name="numero_menores_edit">
					</div>
	            </div>

	            <div class="form-group">
						<label for="nombre">Observaciones*</label>
						<textarea class="form-control" rows="6" id="observaciones_edit" name="observaciones_edit" style="resize: none;"></textarea>
	            </div>

	            <div class="form-group">
						<label for="nombre">Otros detalles*</label>
						<textarea class="form-control" rows="6" id="otros_detalles_edit" name="otros_detalles_edit" style="resize: none;"></textarea>
	            </div>

		  	</form>
		  	<iframe id="ifrm_update" name="ifrm_update" style="display: none;"></iframe>

        </div>
        <div class="modal-footer">
        	<button type="button" class="btn btn-danger pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
			<button type="button" class="btn btn-success pull-right" onclick="actualizarReservacion()"><span class="glyphicon glyphicon-floppy-disk"></span> Actualizar solicitud</button>
		  
		  <span id="msg_validacion"></span>
        </div>
      </div>
    </div>
  </div>



<!-- MODAL BORRAR HOTEL -->
<div class="modal fade" id="DeleteUser" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="background-color:#df4b33;padding:15px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 style="background-color:#df4b33;"><span class="glyphicon glyphicon-exclamation-sign"></span> Cancelar reservación</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
        	<input type="hidden" name="id_reservacion_borrar" id="id_reservacion_borrar" value="0" />
			<form role="form">
          		<p >¿Confirma que desea cancelar la reservación <b class="form-subtitle" id="clave_reservacion_borrar"></b>?</p>
			</form>
										
        </div>
        <div class="modal-footer">
        	<button type="button" class="btn btn-danger pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
        	<button type="button" class="btn btn-danger pull-right" data-dismiss="modal" onclick="cancelarReservacion();"><span class="glyphicon glyphicon-ok"></span> Confirmar</button>
        </div>
      </div>
	</div>
</div>



			
<script>



function descarga_excel_reservaciones(){
    window.location.href="reservaciones/descarga_csv";
}


	$('input[name="my-checkbox"]').on('switchChange.bootstrapSwitch', function(event, state) {
		var target_id = $(this).attr('id');
		var status_ = 0;

		if(state){
			status_ = 1;
		}
		$.ajax({
	        url: "paquetes/habilitarPaquete",
	        data: {id_paquete: target_id, status: status_ },
	        type: "POST",
	        async: 'true',
	        success: function(response) {
	            if (response >= 1) {
	                $.gritter.add({
				        title: 'Actualizar status',
				        text: 'Se actualizó correctamente el status.',
				        class_name: 'success'
				      });
	            } else {
	                $.gritter.add({
				        title: 'Agregar status',
				        text: 'Ocurrió un error y no se pudo actualizar el status, intentelo más tarde.',
				        class_name: 'danger'
				      });
	            }
	        },
	        error: function(x, e) {
	            $.gritter.add({
				        title: 'Actualizar status',
				        text: 'Ocurrió un error y no se pudo actualizar el status: "' + e.messager+ '"',
				        class_name: 'danger'
				});
	        }
		});

	});

function crearReservacion(){

	id_solicitud_ = $("#id_solicitud_confirmar").val();

        $.ajax({
            url: "solicitudes/confirmarSolicitud",
            data: {clave_reservacion: id_solicitud_ },
            type: "POST",
            async: 'true',
            success: function(response) {
				//$("#ModalEditUser").modal('show');
				if(response==1){
					$.gritter.add({
				        title: 'Confirmar solicitud',
				        text: 'Se generó correctamente la reservación.',
				        class_name: 'success'
				      });
					 setTimeout ("window.location.href = 'reservaciones';", 2000);
				}
            },
            error: function(x, e) {
                $.gritter.add({
				        title: 'Confirmar solicitud',
				        text: 'Ocurrio un error y no se pudo confirmar la solicitud, intentelo más tarde.',
				        class_name: 'danger'
				});
            }
        });
}



function actualizarReservacion(){

	var nuevo_status = $("#id_status_reservacion_edit").val(); 
	var id = $("#id_reservacion_edit").val(); 
	var clave = $("#clave_reservacion_edit").val(); 
	//$("#frmHotelEdit").submit();
	console.log(nuevo_status);
	if(nuevo_status==4){
		//Notifica envio de correo
		$("#clave_reservacion_borrar").html(clave);
		$("#id_reservacion_borrar").val(id);
		$("#DeleteUser").modal();
		$("#ModalEditUser").modal('hide');

	}else{
		$("#frmHotelEdit").submit();
	}
}


function guardarPaquete(){
	$("#frmHotelAdd").submit();
}



function eliminarPaquete(id, nombre){
	$("#nombre_paquete_borrar").html(nombre);
	$("#id_paquete_borrar").val(id);


	$("#DeleteUser").modal();
}

$(document).ready(function(){

	$("#ifrm_update").load(function(){
		console.log('load iframe');
	   var ifrAddS = $("#ifrm_update").contents().find("#div_resultado").html();
	   if(ifrAddS == 1){
	   		 $.gritter.add({
				title: 'Editar reservación',
				text: 'Se actualizó correctamente la reservación.',
		        class_name: 'success'
			 });
	         setTimeout ("window.location.href = 'reservaciones';", 2000);
	   }
	   
	});

	$("#ifrm_add").load(function(){
		console.log('load iframe');
		var ifrAddS = $("#ifrm_add").contents().find("#div_resultado").html();
		if(ifrAddS == 1){
	   		 $.gritter.add({
				title: 'Subir archivo',
				text: 'Se guardó correctamente la solicitud.',
		        class_name: 'success'
			 });
	         setTimeout ("window.location.href = 'reservaciones';", 2000);
		}
	   
	});
	
});

function agregarPaquete() {
	$("#ModalAgregarHotel").modal('show');
}

function cancelarReservacion(){
	var id_reservacion_borrar = $('#id_reservacion_borrar').val();
	var otros_detalles_ = $('#otros_detalles_edit').val();
	var status_reservacion = $('#id_status_reservacion_edit').val();

    $.ajax({
        url: "reservaciones/actualizarReservacion",
        data: {id_reservacion_edit: id_reservacion_borrar, otros_detalles_edit: otros_detalles_, id_status_reservacion_edit: status_reservacion },
        type: "POST",
        async: 'true',
        success: function(response) {
			 $.gritter.add({
				title: 'Cancelar reservación',
				text: 'Se canceló correctamente la reservación.',
		        class_name: 'success'
			 });
	         setTimeout ("window.location.href = 'reservaciones';", 2000);
        },
        error: function(x, e) {
            $.gritter.add({
			        title: 'Cancelar reservación',
			        text: 'Ocurrio un error y no se pudo cancelar la reservación, intentelo más tarde.',
			        class_name: 'danger'
			});
        }
    });


}


function verReservacion(id) {

        $.ajax({
            url: "reservaciones/recuperarReservacion",
            data: {id_reservacion: id},
            type: "POST",
            async: 'true',
            success: function(response) {
				var dat = jQuery.parseJSON(response);
				$("#id_reservacion_edit").val(dat.id_reservacion);
				$("#id_status_reservacion_edit").val(dat.id_status_reservacion);
				$("#id_paquete_edit").val(dat.id_paquete);
				$("#titulo_cliente_edit").val(dat.titulo_cliente);
				$("#nombre_cliente_edit").val(dat.nombre_cliente);
				$("#apellidos_cliente_edit").val(dat.apellidos_cliente);
				$("#fecha_registro_edit").val(dat.fecha_registro);
                $("#clave_reservacion_edit").val(dat.clave_reservacion);
                $("#ciudad_cliente_edit").val(dat.ciudad_cliente);
                $("#estado_cliente_edit").val(dat.estado_cliente);
                $("#pais_cliente_edit").val(dat.pais_cliente);
                $("#delegacion_cliente_edit").val(dat.delegacion_cliente);
                $("#codigo_postal_cliente_edit").val(dat.codigo_postal_cliente);
                $("#fecha_nacimiento_cliente_edit").val(dat.fecha_nacimiento_cliente);
                $("#telefono_cliente_edit").val(dat.telefono_cliente);
                $("#movil_cliente_edit").val(dat.movil_cliente);
                $("#correo_cliente_edit").val(dat.correo_cliente);
                $("#id_tipo_tarjeta_edit").val(dat.id_tipo_tarjeta);
                $("#titular_tarjeta_edit").val(dat.titular_tarjeta);
                $("#numero_tarjeta_edit").val(dat.numero_tarjeta);
                $("#codigo_seguridad_edit").val(dat.codigo_seguridad);
                $("#fecha_vencimiento_mes_edit").val(dat.fecha_vencimiento_mes);
                $("#fecha_vencimiento_anio_edit").val(dat.fecha_vencimiento_anio);
                $("#fecha_llegada_edit").val(dat.fecha_llegada);
                $("#hora_llegada_edit").val(dat.hora_llegada);
                $("#fecha_salida_edit").val(dat.fecha_salida);
                $("#hora_salida_edit").val(dat.hora_salida);
                $("#id_tipo_habitacion_edit").val(dat.id_tipo_habitacion);
                $("#numero_menores_edit").val(dat.numero_menores);
                $("#numero_adultos_edit").val(dat.numero_adultos);

                //$("#direccion_cliente_edit").val(dat.direccion_cliente);
                //$("#observaciones_edit").val(dat.observaciones);
                //$("#otros_detalles_edit").val(dat.otros_detalles);

                tinyMCE.get('direccion_cliente_edit').setContent( dat.direccion_cliente );
                tinyMCE.get('observaciones_edit').setContent( dat.observaciones );
                tinyMCE.get('otros_detalles_edit').setContent( dat.otros_detalles );


				$("#ModalEditUser").modal('show');
            },
            error: function(x, e) {
                $.gritter.add({
				        title: 'Editar paquete',
				        text: 'Ocurrio un error y no se pueden recuperar los datos de la solicitud, intentelo más tarde.',
				        class_name: 'danger'
				});
            }
        });
}

</script>
