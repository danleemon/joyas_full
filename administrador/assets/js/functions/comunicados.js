var serverLms =  location.host + "/";
var server = location.host + "/admin/";
// --------------------------------------------------------------------------------------------------------------------
// -------------------------------------------------- AGRGAR COMUNICADO --------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------
$(document).ready(function(){
		$("#nombre").blur(validarNombreAddComunicado);
		$("#nombre").keyup(validarNombreAddComunicado);
	});
	
	
	
function AddComunicado() {//Agregar usuario
    if (validarNombreAddComunicado()) {
        $("#frmAddComunicado").submit();
    } else {
        return;
    }
}


function validarNombreAddComunicado() {
    if ($("#nombre").val().length < 2 || $("#nombre").val() == null || /^\s+$/.test($("#nombre").val())) {
        $("#nombre").addClass("error");
        return false;
    } else {
        $("#nombre").removeClass("error");
        return true;
    }
}




// --------------------------------------------------------------------------------------------------------------------
// -------------------------------------------------- AGRGAR COMUNICADO --------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------


function edit_comu(id) {
        $.ajax({
            url: "comunicados_controller/get_comunicado",
            data: {id: id},
            type: "POST",
            async: 'true',
            beforeSend: function() {
                //console.log('Procesando, espere por favor...');
            },
            success: function(response) {
                //console.log(response);
				var dat = jQuery.parseJSON(response);
				$("#comunicado_id").val(id);
				$("#nombre").val(dat.nombre);
				$("#url").val(dat.url);
				$("#descripcion").val(dat.descripcion);
				$("#tipo_"+dat.tipo).prop('checked', true);
				
				$("#ModalEditComu").modal('show');
            },
            error: function(x, e) {
                $.gritter.add({
				        title: 'Editar Comunicado',
				        text: 'Ocurrio un error, intentelo nuevamente.' + e.messager,
				        class_name: 'danger'
				});
            }
        });
}

function UpdateComu() {//Agregar usuario
   if (validarNombreAddComunicado()) {
        $.ajax({
            url: "comunicados_controller/update_comunicado",
            data: $("#FormUpdateComu").serialize(),
            type: "POST",
            async: 'true',
            beforeSend: function() {
                //console.log('Procesando, espere por favor...');
            },
            success: function(response) {
                if (response == 1) {
                	$("#ModalEditComu").modal('hide');
                    $.gritter.add({
				        title: 'Editar Comunicado',
				        text: 'Se modifico correctamente el Comunicado.',
				        class_name: 'success'
				      });
                    setTimeout ("window.location.reload();", 2300);//setTimeout("loadSection('user_controller','agasajo');", 2000);
                } else {
                    $.gritter.add({
				        title: 'Editar Comunicado',
				        text: 'Ocurrio un error y no se pudo modificar el Comunicado, intentelo nuevamente.',
				        class_name: 'danger'
				      });
                }
            },
            error: function(x, e) {
                $.gritter.add({
				        title: 'Editar Comunicado',
				        text: 'Ocurrio un error y no se pudo modificar el Comunicado, intentelo nuevamente.' + e.messager,
				        class_name: 'danger'
				});
            }
        });
    } else {
        return false;
    }
}



function status_comu(id, status){
	$.ajax({
            url: "comunicados_controller/update_status",
            data: {id: id, status: status},
            type: "POST",
            async: 'true',
            beforeSend: function() {
                //console.log('Procesando, espere por favor...');
            },
            success: function(response) {
                if (response == 1) {
                    $.gritter.add({
				        title: 'Cambiar Status de Comunicado',
				        text: 'Se modifico correctamente el Comunicado.',
				        class_name: 'success'
				      });
                    setTimeout ("window.location.reload();", 2300);//setTimeout("loadSection('user_controller','agasajo');", 2000);
                } else {
                    $.gritter.add({
				        title: 'Cambiar Status de Comunicado',
				        text: 'Ocurrio un error y no se pudo modificar el Comunicado, intentelo nuevamente.',
				        class_name: 'danger'
				      });
                }
            },
            error: function(x, e) {
                $.gritter.add({
				        title: 'Cambiar Status de Comunicado',
				        text: 'Ocurrio un error y no se pudo modificar el Comunicado, intentelo nuevamente.' + e.messager,
				        class_name: 'danger'
				});
            }
        });
}


function delete_comu(id){
	$.ajax({
            url: "comunicados_controller/eliminar_comunicado",
            data: {id: id},
            type: "POST",
            async: 'true',
            beforeSend: function() {
                //console.log('Procesando, espere por favor...');
            },
            success: function(response) {
                if (response == 1) {
                	$("#DeleteComu").modal('hide');
                    $.gritter.add({
				        title: 'Eliminar Comunicado',
				        text: 'Se elimino correctamente el Comunicado.',
				        class_name: 'success'
				      });
                    setTimeout ("window.location.reload();", 2300);
                } else {
                    $.gritter.add({
				        title: 'Eliminar Comunicado',
				        text: 'Ocurrio un error y no se pudo eliminar el Comunicado, intentelo nuevamente.',
				        class_name: 'danger'
				      });
                }
            },
            error: function(x, e) {
                $.gritter.add({
				        title: 'Eliminar Comunicado',
				        text: 'Ocurrio un error y no se pudo eliminar el Comunicado, intentelo nuevamente.' + e.messager,
				        class_name: 'danger'
				});
            }
        });
}

