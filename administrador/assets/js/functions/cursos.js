function status_curso(id, status){
	$.ajax({
            url: "course_controller/update_status",
            data: {id: id, status: status},
            type: "POST",
            async: 'true',
            beforeSend: function() {
                //console.log('Procesando, espere por favor...');
            },
            success: function(response) {
                if (response == 1) {
                    $.gritter.add({
				        title: 'Cambiar Status de Curso',
				        text: 'Se modifico correctamente el Curso.',
				        class_name: 'success'
				      });
                    setTimeout ("window.location.reload();", 2300);//setTimeout("loadSection('user_controller','agasajo');", 2000);
                } else {
                    $.gritter.add({
				        title: 'Cambiar Status de Curso',
				        text: 'Ocurrio un error y no se pudo modificar el Curso, intentelo nuevamente.',
				        class_name: 'danger'
				      });
                }
            },
            error: function(x, e) {
                $.gritter.add({
				        title: 'Cambiar Status de Curso',
				        text: 'Ocurrio un error y no se pudo modificar el Curso, intentelo nuevamente.' + e.messager,
				        class_name: 'danger'
				});
            }
        });
}

function edit_curso(id) {
        $.ajax({
            url: "course_controller/get_curso",
            data: {id: id},
            type: "POST",
            async: 'true',
            beforeSend: function() {
                //console.log('Procesando, espere por favor...');
            },
            success: function(response) {
                //console.log(response);
				var dat = jQuery.parseJSON(response);
				$("#curso_id").val(id);
				$("#nombre").val(dat.course_name);
				$("#horas").val(dat.course_duration);
				$("#desc").val(dat.course_description);
                                $("#category").val(dat.cat_id);
				
				$("#ModalEditCurso").modal('show');
            },
            error: function(x, e) {
                $.gritter.add({
				        title: 'Editar Curso',
				        text: 'Ocurrio un error, intentelo nuevamente.' + e.messager,
				        class_name: 'danger'
				});
            }
        });
}


function UpdateCurso() {
        $.ajax({
            url: "course_controller/update_curso",
            data: $("#FormUpdateCurso").serialize(),
            type: "POST",
            async: 'true',
            beforeSend: function() {
                //console.log('Procesando, espere por favor...');
            },
            success: function(response) {
                if (response == 1) {
                	$("#ModalEditComu").modal('hide');
                    $.gritter.add({
				        title: 'Editar Curso',
				        text: 'Se modifico correctamente el Curso.',
				        class_name: 'success'
				      });
                    setTimeout ("window.location.reload();", 2300);//setTimeout("loadSection('user_controller','agasajo');", 2000); 
                } else {
                    $.gritter.add({
				        title: 'Editar Curso',
				        text: 'Ocurrio un error y no se pudo modificar el Curso, intentelo nuevamente.',
				        class_name: 'danger'
				      });
                }
            },
            error: function(x, e) {
                $.gritter.add({
				        title: 'Editar Curso',
				        text: 'Ocurrio un error y no se pudo modificar el Curso, intentelo nuevamente.' + e.messager,
				        class_name: 'danger'
				});
            }
        });
}
