var serverLms =  location.host + "/";
var server = location.host + "/admin/";
// --------------------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------
$(document).ready(function(){
		$("#nombre_file").blur(validarNombreAddFile);
		$("#nombre_file").keyup(validarNombreAddFile);
		$("#tipo_file").blur(validarTipoFile);
		$("#tipo_file").change(validarTipoFile);
	});
	
	
function tip_file(type){
		$("#lbext").show();
		if(type == 4 || type == 6 || type == 7){ var ext = 'JPG, PNG o PDF';}
		if(type == 5){ var ext = 'MP4';}
		
		$("#exten").html(ext);
}
	

function AddFile() {//Agregar
    if (validarNombreAddFile() & validarTipoFile()) {
        $("#frmAddFile").submit();
    } else {
        return;
    }
}


function validarNombreAddFile() {
    if ($("#nombre_file").val().length < 2 || $("#nombre_file").val() == null || /^\s+$/.test($("#nombre_file").val())) {
        $("#nombre_file").addClass("error");
        return false;
    } else {
        $("#nombre_file").removeClass("error");
        return true;
    }
}



function validarTipoFile(){
		tf = document.getElementById("tipo_file").selectedIndex;
		if( tf == null || tf== 0 ) {
			$("#tipo_file").addClass("error");
			return false;
		}else{
			$("#tipo_file").removeClass("error");
			return true;
		}
}




// --------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------  --------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------

function status_file(id, status){
	$.ajax({
            url: "files_controller/update_status",
            data: {id: id, status: status},
            type: "POST",
            async: 'true',
            beforeSend: function() {
                //console.log('Procesando, espere por favor...');
            },
            success: function(response) {
                if (response == 1) {
                    $.gritter.add({
				        title: 'Cambiar Status de Material de apoyo',
				        text: 'Se modifico correctamente el Material de apoyo.',
				        class_name: 'success'
				      });
                    setTimeout ("window.location.reload();", 2300);//setTimeout("loadSection('user_controller','agasajo');", 2000);
                } else {
                    $.gritter.add({
				        title: 'Cambiar Status de Material de apoyo',
				        text: 'Ocurrio un error y no se pudo modificar el Material de apoyo, intentelo nuevamente.',
				        class_name: 'danger'
				      });
                }
            },
            error: function(x, e) {
                $.gritter.add({
				        title: 'Cambiar Status de Material de apoyo',
				        text: 'Ocurrio un error y no se pudo modificar el Material de apoyo, intentelo nuevamente.' + e.messager,
				        class_name: 'danger'
				});
            }
        });
}

function edit_file(id) {
        $.ajax({
            url: "files_controller/get_file",
            data: {id: id},
            type: "POST",
            async: 'true',
            beforeSend: function() {
                //console.log('Procesando, espere por favor...');
            },
            success: function(response) {
                //console.log(response);
				var dat = jQuery.parseJSON(response);
				$("#file_id").val(id);
				$("#nombre_file").val(dat.name);
				$("#tipo_file").val(dat.category);
				
				$("#ModalEditFile").modal('show');
            },
            error: function(x, e) {
                $.gritter.add({
				        title: 'Editar Material de apoyo',
				        text: 'Ocurrio un error, intentelo nuevamente.' + e.messager,
				        class_name: 'danger'
				});
            }
        });
}


function UpdateFile() {//Agregar usuario
   if (validarNombreAddFile() & validarTipoFile()) {
        $.ajax({
            url: "files_controller/update_file",
            data: $("#FormUpdateFile").serialize(),
            type: "POST",
            async: 'true',
            beforeSend: function() {
                //console.log('Procesando, espere por favor...');
            },
            success: function(response) {
                if (response == 1) {
                	$("#ModalEditFile").modal('hide');
                    $.gritter.add({
				        title: 'Editar Material de apoyo',
				        text: 'Se modifico correctamente el Material de apoyo.',
				        class_name: 'success'
				      });
                    setTimeout ("window.location.reload();", 2300);//setTimeout("loadSection('user_controller','agasajo');", 2000);
                } else {
                    $.gritter.add({
				        title: 'Editar Material de apoyo',
				        text: 'Ocurrio un error y no se pudo modificar el Material de apoyo, intentelo nuevamente.',
				        class_name: 'danger'
				      });
                }
            },
            error: function(x, e) {
                $.gritter.add({
				        title: 'Editar Comunicado',
				        text: 'Ocurrio un error y no se pudo modificar el Comunicado, intentelo nuevamente.' + e.messager,
				        class_name: 'danger'
				});
            }
        });
    } else {
        return false;
    }
}



