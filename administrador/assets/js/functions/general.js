var serverLms =  location.host + "/";
var server = location.host + "/admin/";

$("#dvTrans").click(function() {
    $("#dvTrans").hide(300);
});

function loadSection(controller, divSel)//Controlador,Div en el que se despliega la vista
{
    $.ajax({
        url: controller,
        async: 'true',
        cache: false,
        contentType: "application/x-www-form-urlencoded",
        dataType: "html",
        error: function(object, error, anotherObject) {
            alert('Mensaje: ' + object.statusText + 'Status: ' + object.status);
        },
        global: true,
        ifModified: false,
        processData: true,
        success: function(result) {
            //console.log(result);
            $('#' + divSel).html(result);
            
        },
        timeout: 30000,
        type: "GET"
    });
}
// --------------------------------------------------------------------------------------------------------------------
