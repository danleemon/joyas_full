function load_type_jerar(id){
	//console.log(id);
	switch(id) {
		case "1":
		    var nm_lab = "Curso";
		    break;
		case "2":
		    var nm_lab = "Comunicados";
		    break;
		case "3":
		    var nm_lab = "Material";
		    break;
		case "4":
		    var nm_lab = "Programa";
		    break;
		case "5":
		    var nm_lab = "Evaluaciones";
		    break;
	}
	
	
	$.ajax({
	    data:  {tipo: id},
		url: "hierarchy_controller/load_type",
		type:  'post',
		success:  function (response) {
	        $('#recurso_jerar').html(response);
	        $("#lb_recurso").html(nm_lab);
		}
	});
}


function addHierarchy(){
	var tipo_recurso = $("#tipo_jer").val();
	var recurso = $("#recurso_jerar").val();
	var proyectos = $("#proyecto").val();//tipo_learners
	var territorios = $("#territorio").val();//categorias
   var regionales = $("#regional").val();//Regional
   var caps = $("#cap").val();//ciudades
   var sucursales = $("#sucursal").val();//canales
    
    var msg = ' ';
    
    var tj = document.getElementById("tipo_jer").selectedIndex;
	if( tj == null || tj == 0 ) {
		msg = msg+"<br>-<b>Tipo de Jerarquia que desea agregar</b>";
	}
		
	if( $("#proyecto").val() == null) {
		msg = msg+"<br>-<b>Proyecto</b>";
	}
	
	if( $("#territorio").val() == null) {
		msg = msg+"<br>-<b>Territorio</b>";
	}
	
	if( $("#regional").val() == null ) {
		msg = msg+"<br>-<b>Regional</b>";
	}
	
	if( $("#cap").val() == null ) {
		msg = msg+"<br>-<b>CAP</b>";
	}
	
	if( $("#sucursal").val() == null ) {
		msg = msg+"<br>-<b>Sucursal</b>";
	}
		
		
		
	if (msg.length > 5) {
		$.gritter.add({
				title: 'Agregar Jerarquia',
				text: 'Favor de seleccionar por lo menos una opción de los siguientes campos: '+ msg,
				class_name: 'danger'
		});
		return;
	}else{

		$.each(proyectos,function(index_l,contenido_l){
	   var proyecto_ = contenido_l;
	        
	   $.each(territorios,function(index_c,contenido_c){
	   var territorio = contenido_c;
	         
		$.each(regionales,function(index_r,contenido_r){
		var regional = contenido_r;
		        
		$.each(caps,function(index_ciu,contenido_ciu){
		var cap = contenido_ciu;
		        	
		$.each(sucursales,function(index_can,contenido_can){
		var sucursal = contenido_can;

				        	
						        $.ajax({
								    data:  {id_hierarchy_type: tipo_recurso, id: recurso, proyecto: proyecto_, territorio_id: territorio, regional_id: regional, cap_id: cap, sucursal_id: sucursal},
									url: "hierarchy_controller/addHierarchy",
									type:  'post',
									success:  function (response) {
								        //console.log(response);
								        if(response > 0){
								        	//console.log("Se ha insertado correcatmente la jerarquia --- success"+response);
								        	$.ajax({
											    data:  {id: response},
												url: "hierarchy_controller/get_hierarchy_add",
												type:  'post',
												success:  function (response) {
											        //console.log(response);
											        /*obj =  JSON && JSON.parse(response) || $.parseJSON(response);
											        $("#jer_insert").fadeIn(300);
											        $("#nuevos_reg").prepend('<tr id="nu_'+obj[0].id_hierarchy+'"><td>'+obj[0].type+'</td><td>'+obj[0].recurso+'</td><td>'+obj[0].learner_type_name+'</td><td>'+obj[0].categoria+'</td><td>'+obj[0].region+'</td><td>'+obj[0].ciudad+'</td><td>'+obj[0].canal+'</td><td>'+obj[0].cliente+'</td><td class="text-center"><a style="cursor: pointer;" class="label label-danger" onclick="eliminar_jerar('+obj[0].id_hierarchy+',2);" ><i class="fa fa-times"></i></a></td></tr>');*/
											        $.gritter.add({
															title: 'Agregar Jerarquia',
															text: 'Se agrego correctamente la jerarquia',
															class_name: 'success'
													});
											      /*$('html,body').animate({
													    scrollTop: $("#nuevos_reg").offset().top
													}, 800);*/																			
																																		
												}
											});
								        }else if(response == 0){
								        	$.gritter.add({
												title: 'Agregar Jerarquia',
												text: 'La jerarquia que ingreso ya existe',
												class_name: 'danger'
											});
								        }else{
								        	$.gritter.add({
												title: 'Agregar Jerarquia',
												text: 'Ocurrio un error intentelo nuevamente',
												class_name: 'danger'
											});
								        }
									},
									error: function (xhr, ajaxOptions, thrownError) {
								        alert("La jerarquia que intenta ingresar ya existe.");
								    }
									
								});
			
						});//----- each canales
					});//----- each ciudades	
				});//----- each regions	
			});//----- each categorias	
		});//----- each tipo_learners
	
	}
	
}

function eliminar_jerar(id,tipo){
	$.ajax({
	    data:  {id_hierarchy: id},
		url: "hierarchy_controller/deleteHierarchy",
		type:  'post',
		success:  function (response) {
	        	$("#nu_"+id).fadeOut(300);

 				$.gritter.add({
				        title: 'Eliminar jerarquía',
				        text: 'Se elimino correctamente la jerarquía.',
				        class_name: 'success'
				});
	        
		},
      error: function(x, e) {
            $.gritter.add({
				        title: 'Eliminar jerarquía',
				        text: 'Ocurrio un error y no se pudo eliminar la jerarquía, intentelo nuevamente.' + e.messager,
				        class_name: 'danger'
				});
      }
	});
}
