var serverLms =  location.host + "/";
var server = location.host + "/admin/";
// --------------------------------------------------------------------------------------------------------------------
// -------------------------------------------------- AGRGAR LEARNER --------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------

function AddLearner() {//Agregar usuario
		var msg = ' ';
		
		var tipo_user = $("#tipo_user").val();
		var coordinador = $("#coordinador").val();
		var num_empleado = $("#num_empleado").val();
		var nombre = $("#nombre").val();
		var apellidos = $("#apellidos").val();
		var ingreso = $("#ingreso").val();
		var region = $("#region").val();
		var ciudad = $("#ciudad").val();
		var canal = $("#canal").val();
		var cliente = $("#cliente").val();
		var celular = $("#celular").val();
		var telefono = $("#telefono").val();
		var mail = $("#mail").val();
		var pass = $("#pass").val();
		var playera = $("#playera").val();
		var pantalon = $("#pantalon").val();
		var saco = $("#saco").val();
		var falda = $("#falda").val();
		var categoria = $("#categoria").val();
		var patron = $("#patron").val();
                if($("#staff").prop('checked') == true){
                    var staff = 1;
                }else{
                    var staff = 0;
                }
    
    	if( tipo_user == null || tipo_user == 0) {
			msg = msg+"<br>-<b>Tipo de Usuario</b>";
		}
		
		if( tipo_user == 1 && coordinador == 0) {
			msg = msg+"<br>-<b>Coordinador</b>";
		}
		
		if (num_empleado.length < 2 || num_empleado == null || /^\s+$/.test(num_empleado)) {
			msg = msg+"<br>-<b>Número de empleado</b>";
		}
		
		if (nombre.length < 2 || nombre == null || /^\s+$/.test(nombre)) {
			msg = msg+"<br>-<b>Nombre</b>";
		}
		
		if (apellidos.length < 2 || apellidos == null || /^\s+$/.test(apellidos)) {
			msg = msg+"<br>-<b>Apellidos</b>";
		}
				
		if( region == null) {
			msg = msg+"<br>-<b>Region</b>";
		}
		
		if( ciudad == null) {
			msg = msg+"<br>-<b>Ciudad</b>";
		}
		
		if( canal == null) {
			msg = msg+"<br>-<b>Canal</b>";
		}
		
		if( cliente == null) {
			msg = msg+"<br>-<b>Cliente</b>";
		}
		
		var filter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
		if(filter.test(mail)){
			//
		}else{
			msg = msg+"<br>-Correo electrónico<b></b>";
		}

		
		if (pass.length < 4 || pass == null || /^\s+$/.test(pass)) {
			msg = msg+"<br>-<b>Contraseña</b>";
		}
		
		if ($("#passc").val() != pass) {
			msg = msg+"<br>-<b>Las contraseñas no coinciden</b>";
		}
		
		if( categoria == null) {
			msg = msg+"<br>-<b>Categoria</b>";
		}
		
		if( patron == null || patron == 0) {
			msg = msg+"<br>-<b>Patron</b>";
		}
		
		
		
		

		if (msg.length > 5) {
			$.gritter.add({
					title: 'Agregar Usuario',
					text: 'Favor de completar la información de los siguientes campos: '+ msg,
					class_name: 'danger'
			});
			return;
		}else{
			
	        $.ajax({
	            url: "learners_controller/add_learner",
	            data: {username: mail, password: pass, learner_type_id: tipo_user, num_employed: num_empleado, name: nombre, last_name: apellidos, cellular: celular, email: mail, phone: telefono, coordinador: coordinador, fecha_ingreso: ingreso, patron: patron, staff: staff},
	            type: "POST",
	            async: 'true',
	            beforeSend: function() {
	                //console.log('Procesando, espere por favor...');
	            },
	            success: function(learner_id) {
	            	//console.log(learner_id);
	            	if (learner_id == 'exist') {
	            		$.gritter.add({
					        title: 'Agregar Usuario',
					        text: 'El usuario que ingreso ya existe.',
					        class_name: 'danger'
					    });
	            		return;	
	            	}
	                if (learner_id > 0) {
	                	
	                    $.ajax({
						    data:  {learner_id: learner_id, playera: playera, pantalon: pantalon, saco: saco, falda: falda},
							url: "learners_controller/insert_learner_info",
							type:  'post',
							success:  function (response_info) {
								//console.log('info'+response_info);
						        if(response_info < 1){alert("no inserto info de tallas");}
							}
						});
						
						$.each(region,function(index_reg,contenido_reg){//----- each region
				        	var regi = contenido_reg;
						        $.ajax({
								    data:  {region_id: regi, learner_id: learner_id},
									url: "learners_controller/insert_learner_region",
									type:  'post',
									success:  function (response_reg) {
										//console.log('reg'+response_reg);
								         if(response_reg < 1){alert("no inserto region");}
									}
								});
						});
						
						$.ajax({
						    data:  {learner_id: learner_id, ciudad_id: ciudad},
							url: "learners_controller/insert_learner_ciudad",
							type:  'post',
							success:  function (response_ciu) {
								//console.log('ciu:'+response_ciu);
						        if(response_ciu < 1){alert("no inserto info de tallas");}
							}
						});
						
						$.each(canal,function(index_cana,contenido_cana){//----- each canal
				        	var cana = contenido_cana;
						        $.ajax({
								    data:  {canal_id: cana, learner_id: learner_id},
									url: "learners_controller/insert_learner_canal",
									type:  'post',
									success:  function (response_cana) {
										//console.log('cana:'+response_cana);
								         if(response_cana < 1){alert("no inserto canal");}
									}
								});
						});
						
						$.each(cliente,function(index_cli,contenido_cli){//----- each cliente
				        	var cli = contenido_cli;
						        $.ajax({
								    data:  {cliente_id: cli, learner_id: learner_id},
									url: "learners_controller/insert_learner_cliente",
									type:  'post',
									success:  function (response_cli) {
										//console.log('clien:'+response_cli);
								         if(response_cli < 1){alert("no inserto cliente");}
									}
								});
						});
						
						$.each(categoria,function(index_cate,contenido_cate){//----- each categoria
				        	var cate = contenido_cate;
						        $.ajax({
								    data:  {category_id: cate, learner_id: learner_id},
									url: "learners_controller/insert_learner_categoria",
									type:  'post',
									success:  function (response_cate) {
										//console.log('cate: '+response_cate);
								         if(response_cate < 1){alert("no inserto categoria");}
									}
								});
						});
						
	                    $.gritter.add({
					        title: 'Agregar Usuario',
					        text: 'Se añadio correctamente el usuario.',
					        class_name: 'success'
					      });
	                    setTimeout ("window.location.href = 'usuarios';", 2000);//
	                } else {
	                    $.gritter.add({
					        title: 'Agregar Usuario',
					        text: 'Ocurrio un error y no se pudo añadir el usuario, intentelo nuevamente.',
					        class_name: 'danger'
					      });
	                }
	            },
	            error: function(x, e) {
	                $.gritter.add({
					        title: 'Agregar Usuario',
					        text: 'Ocurrio un error y no se pudo añadir el usuario, intentelo nuevamente.' + e.messager,
					        class_name: 'danger'
					});
	            }
	        });
	  }
    
}

function tipo_learner(tipo){
	if(tipo == 1){
		$("#tip_us").removeClass('col-lg-12');
		$("#tip_us").addClass('col-lg-6');
		$("#coordinador_select").fadeIn(400);
	}else{
		$("#coordinador_select").fadeOut(400);
		$("#tip_us").removeClass('col-lg-6');
		$("#tip_us").addClass('col-lg-12');
	}
}
	
function tipo_user_learner(tipo){
	if(tipo == 1){
		$("#div_coordinador").show();
	}else{
		$("#div_coordinador").hide();
	}
}









// --------------------------------------------------------------------------------------------------------------------
// -------------------------------------------------- AGRGAR Learner --------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------



function status_learner(id, status){
	$.ajax({
            url: "learners_controller/update_status",
            data: {id: id, status: status},
            type: "POST",
            async: 'true',
            beforeSend: function() {
                //console.log('Procesando, espere por favor...');
            },
            success: function(response) {
                if (response == 1) {
                    $.gritter.add({
				        title: 'Cambiar Status de Usuario',
				        text: 'Se modifico correctamente el usuario.',
				        class_name: 'success'
				      });
                    setTimeout ("window.location.reload();", 2300);//setTimeout("loadSection('user_controller','agasajo');", 2000);
                } else {
                    $.gritter.add({
				        title: 'Cambiar Status de Usuario',
				        text: 'Ocurrio un error y no se pudo modificar el usuario, intentelo nuevamente.',
				        class_name: 'danger'
				      });
                }
            },
            error: function(x, e) {
                $.gritter.add({
				        title: 'Cambiar Status de Usuario',
				        text: 'Ocurrio un error y no se pudo modificar el usuario, intentelo nuevamente.' + e.messager,
				        class_name: 'danger'
				});
            }
        });
}




function edit_learner(id) {
	
    $.ajax({
        url: "learners_controller/get_learner",
        data: {id: id},
        type: "POST",
        async: 'true',
        beforeSend: function() {
            //console.log('Procesando, espere por favor...');
        },
        success: function(response) {
			var myresponse = response.replace("[","");
			myresponse = myresponse.replace("]","");

			var dat = jQuery.parseJSON(myresponse);

				$("#tipo_user").val(dat.learner_type_id);
				$("#coordinador").val(dat.coordinador_id);

				if(dat.learner_type_id=="1"){
					$("#div_coordinador").show();
				}else{
					$("#div_coordinador").hide();
				}
				$("#numero_empleado").val(dat.num_employed);
				$("#learner_id").val(id);
				$("#nombre").val(dat.name);
				$("#apellido").val(dat.last_name);
				$("#celular").val(dat.cellular);

				$("#ingreso").val(dat.fecha_ingreso);
				$("#telefono").val(dat.phone);
				$("#mail").val(dat.email);
				$("#playera").val(dat.talla_playera);
				$("#pantalon").val(dat.talla_pantalon);
				$("#saco").val(dat.talla_saco);
				$("#falda").val(dat.talla_falda);
				$("#patron").val(dat.patron_id);

				//registro
			if(dat.regiones_id){
				var regiones_ids = dat.regiones_id.split(",");
				$("#region").select2("val",regiones_ids);
			}
				//clientes
			if(dat.clientes_id){
				var clientes_ids = dat.clientes_id.split(",");
				$("#cliente").select2("val",clientes_ids);
			}
				//ciudades
			if(dat.ciudades_id){
				var ciudades_ids = dat.ciudades_id.split(",");
				$("#ciudad").select2("val",ciudades_ids);
			}

				//categorias
			if(dat.categorias_id){
				var categorias_ids = dat.categorias_id.split(",");
				$("#categoria_").select2("val",categorias_ids);
			}
				//canales
			if(dat.canales_id){
				var canales_ids = dat.canales_id.split(",");
				$("#canal").select2("val",canales_ids);
			}





			$("#ModalEditUser").modal('show');
        },
        error: function(x, e) {
            $.gritter.add({
			        title: 'Editar Usuario',
			        text: 'Ocurrio un error y no se pudo editar el usuario, intentelo nuevamente.' + e.messager,
			        class_name: 'danger'
			});
        }
    });

}

function delete_learner(id) {
	$( "#learner_id_delete" ).val( id );
    $("#ModalDeleteLearner").modal('show');   
}

function reset_pass_learner(id) {
	$( "#learner_id_reset" ).val( id );
    $("#ModalResetLearner").modal('show');   
}



// --------------------------------------------------------------------------------------------------------------------
// -------------------------------------------------- EDITAR USUARIO --------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------



function UpdateLearner() {//Agregar usuario
   //if (validarNombreAdd() & validaUsernameAdd() & validarTipoAdd() & validarTipoCat() & validarPatron()) {

		var id = $("#learner_id").val();
		var tipo_user = $("#tipo_usuario").val();
		var coordinador = $("#coordinador").val();
		var num_empleado = $("#numero_empleado").val();
		var nombre = $("#nombre").val();
		var apellidos = $("#apellido").val();
		var region = $("#region").val();

		var cliente = $("#cliente").val();
		var ciudad = $("#ciudad").val();
		var canal = $("#canal").val();
		var categoria = $("#categoria_").val();

		var celular = $("#celular").val();
		var patron = $("#patron").val();

		var ingreso = $("#ingreso").val();
		var telefono = $("#telefono").val();
		var mail = $("#mail").val();
		var playera = $("#playera").val();
		var pantalon = $("#pantalon").val();
		var saco = $("#saco").val();
		var falda = $("#falda").val();

		var company = Math.floor( (Math.random()*100) + 1 );
		
		if(region == null || ciudad == null || canal == null || cliente == null || categoria == null){
			$.gritter.add({
				title: 'Campos Obligatorios',
				text: 'Favor de seleccionar Región, Ciudad, Canal, Cliente y Categoría.',
				class_name: 'danger'
			});
			return;
		}


        $.ajax({
            url: "learners_controller/update_learner",
            data: {learner_id:id, company_id:company, learner_type_id: tipo_user, num_employed: num_empleado, name: nombre, last_name: apellidos, cellular: celular, coordinador: coordinador, patron: patron, email: mail, phone: telefono, fecha_ingreso: ingreso},
            type: "POST",
            type: "POST",
            async: 'true',
            beforeSend: function() {
                //console.log('Procesando, espere por favor...');
            },
            success: function(response) {
				console.log(response);
                if (response == 1) {

				/*console.log("region: "+ region+" --- ciudad: "+ ciudad+" --- canal: "+canal+" --- cliente: "+cliente+" --- categoria: "+categoria);
				return;*/


					$.ajax({
						    data:  {learner_id: id},
							url: "learners_controller/clean_info",
							type:  'post',
							success:  function (response_info) {
								console.log('removing prev info: '+response_info);

								$.ajax({
								    data:  {learner_id: id, playera: playera, pantalon: pantalon, saco: saco, falda: falda},
									url: "learners_controller/insert_learner_info",
									type:  'post',
									success:  function (response_info) {
										//console.log('info'+response_info);
								        if(response_info < 1){alert("no inserto info de tallas");}
									}
								});
								
								
								
								$.each(region,function(index_reg,contenido_reg){//----- each region
						        	var regi = contenido_reg;
		
								        $.ajax({
										    data:  {region_id: regi, learner_id: id},
											url: "learners_controller/insert_learner_region",
											type:  'post',
											success:  function (response_reg) {
												console.log('reg'+response_reg);
										         if(response_reg < 1){alert("no inserto region");}
											}
										});
								});
								
		
								$.each(ciudad,function(index_ciudad,contenido_ciudad){//----- each ciudad
						        	var ciuda = contenido_ciudad;
								        $.ajax({
										    data:  {ciudad_id: ciuda, learner_id: id},
											url: "learners_controller/insert_learner_ciudad",
											type:  'post',
											success:  function (response_ciudad) {
												//console.log('cana:'+response_cana);
										         if(response_ciudad < 1){alert("no inserto ciudad");}
											}
										});
								});
		
								
								$.each(canal,function(index_cana,contenido_cana){//----- each canal
						        	var cana = contenido_cana;
								        $.ajax({
										    data:  {canal_id: cana, learner_id: id},
											url: "learners_controller/insert_learner_canal",
											type:  'post',
											success:  function (response_cana) {
												//console.log('cana:'+response_cana);
										         if(response_cana < 1){alert("no inserto canal");}
											}
										});
								});
								
								$.each(cliente,function(index_cli,contenido_cli){//----- each cliente
						        	var cli = contenido_cli;
								        $.ajax({
										    data:  {cliente_id: cli, learner_id: id},
											url: "learners_controller/insert_learner_cliente",
											type:  'post',
											success:  function (response_cli) {
												//console.log('clien:'+response_cli);
										         if(response_cli < 1){alert("no inserto cliente");}
											}
										});
								});
								
								$.each(categoria,function(index_cate,contenido_cate){//----- each categoria
						        	var cate = contenido_cate;
								        $.ajax({
										    data:  {category_id: cate, learner_id: id},
											url: "learners_controller/insert_learner_categoria",
											type:  'post',
											success:  function (response_cate) {
												//console.log('cate: '+response_cate);
										         if(response_cate < 1){alert("no inserto categoria");}
											}
										});
								});

							}
						});


//return;

                	$("#ModalEditUser").modal('hide');
                    $.gritter.add({
				        title: 'Editar Usuario',
				        text: 'Se modifico correctamente el usuario.',
				        class_name: 'success'
				      });
                    setTimeout ("window.location.reload();", 2300);
                } else {
                    $.gritter.add({
				        title: 'Editar Usuario',
				        text: 'Ocurrio un error y no se pudo modificar el usuario, intentelo nuevamente.',
				        class_name: 'danger'
				      });
                }
            },
            error: function(x, e) {
                $.gritter.add({
				        title: 'Editar Usuario',
				        text: 'Ocurrio un error y no se pudo modificar el usuario, intentelo nuevamente.' + e.messager,
				        class_name: 'danger'
				});
            }
        });
    /*} else {
        return false;
    }*/
}


function deleteLearner(id){
	$.ajax({
            url: "learners_controller/delete_user",
            data: {id: id},
            type: "POST",
            async: 'true',
            beforeSend: function() {
                //console.log('Procesando, espere por favor...');
            },
            success: function(response) {
                if (response == 1) {
                	$("#DeleteUser").modal('hide');
                    $.gritter.add({
				        title: 'Eliminar Usuario',
				        text: 'Se elimino correctamente el usuario.',
				        class_name: 'success'
				      });
                    setTimeout ("window.location.reload();", 2300);
                } else if (response == 2){
                    $.gritter.add({
				        title: 'Eliminar Usuario',
				        text: 'El usuario está asignado como coordinador de otro usuario y no se pudo eliminar.',
				        class_name: 'danger'
				      });
                }else {
                    $.gritter.add({
				        title: 'Eliminar Usuario',
				        text: 'Ocurrio un error y no se pudo eliminar el usuario, intentelo nuevamente.',
				        class_name: 'danger'
				      });
                }
            },
            error: function(x, e) {
                $.gritter.add({
				        title: 'Eliminar Usuario',
				        text: 'Ocurrio un error y no se pudo eliminar el usuario, intentelo nuevamente.' + e.messager,
				        class_name: 'danger'
				});
            }
        });
}

function ResetPassLearner(id){
	$.ajax({
            url: "learners_controller/reset_pass_user",
            data: {id: id},
            type: "POST",
            async: 'true',
            beforeSend: function() {
                //console.log('Procesando, espere por favor...');
            },
            success: function(response) {
            	console.log(response);
            	var obj = response.split("-");
            	
                if (obj[1] == 'si') {
                    $("#btnresetlear").fadeOut(500);
                    $("#texto_reset_p").html('<h4>Se cambio la contraseña correctamente, se le mando correo al usuario notificandole el cambio.<br><br>La nueva contraseña es : <b style="  font-weight: bold;">'+obj[0]+'</b></h4>');
                    setTimeout ("window.location.reload();", 5000);
                }else {
                    $.gritter.add({
				        title: 'Resetear Contraseña de Usuario',
				        text: 'Ocurrio un error y no se pudo resetear la contraseña del usuario, intentelo nuevamente.',
				        class_name: 'danger'
				      });
                }
            },
            error: function(x, e) {
                $.gritter.add({
				        title: 'Resetear Contraseña de Usuario',
				        text: 'Ocurrio un error y no se pudo resetear la contraseña del usuario, intentelo nuevamente.' + e.messager,
				        class_name: 'danger'
				});
            }
        });
}

// --------------------------------------------------------------------------------------------------------------------
// -------------------------------------------------- EDITAR USUARIO --------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------
