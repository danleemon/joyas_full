function AddNews() {//Agregar usuario
    if (validarTituloNoticia() & validarTipoNoticia()) {
    	$('textarea[name="textarea"]').html($('#summernote').code());
        $("#frmAddNoticia").submit();
    } else {
        return;
    }
}

function validarTituloNoticia() {
    if ($("#titulo").val().length < 2 || $("#titulo").val() == null || /^\s+$/.test($("#titulo").val())) {
        $("#titulo").addClass("error");
        return false;
    } else {
        $("#titulo").removeClass("error");
        return true;
    }
}

function validarTipoNoticia(){
		tipnot = document.getElementById("tipo").selectedIndex;
		if( tipnot == null || tipnot == 0 ) {
			$("#tipo").addClass("error");
			return false;
		}else{
			$("#tipo").removeClass("error");
			return true;
		}
}


function status_new(id, status){
	$.ajax({
            url: "news_controller/update_status",
            data: {id: id, status: status},
            type: "POST",
            async: 'true',
            beforeSend: function() {
                //console.log('Procesando, espere por favor...');
            },
            success: function(response) {
                if (response == 1) {
                    $.gritter.add({
				        title: 'Cambiar Status de Noticias',
				        text: 'Se modifico correctamente la Noticia.',
				        class_name: 'success'
				      });
                    setTimeout ("window.location.reload();", 2300);//setTimeout("loadSection('user_controller','agasajo');", 2000);
                } else {
                    $.gritter.add({
				        title: 'Cambiar Status de Noticias',
				        text: 'Ocurrio un error y no se pudo modificar la Noticia, intentelo nuevamente.',
				        class_name: 'danger'
				      });
                }
            },
            error: function(x, e) {
                $.gritter.add({
				        title: 'Cambiar Status de Noticias',
				        text: 'Ocurrio un error y no se pudo modificar la Noticia, intentelo nuevamente.' + e.messager,
				        class_name: 'danger'
				});
            }
        });
}


function edit_new(id) {
        $.ajax({
            url: "news_controller/get_new",
            data: {id: id},
            type: "POST",
            async: 'true',
            beforeSend: function() {
                //console.log('Procesando, espere por favor...');
            },
            success: function(response) {
                //console.log(response);
				var dat = jQuery.parseJSON(response);
				$("#new_id").val(id);
				$("#titulo").val(dat.titulo);
				$("#tipo").val(dat.tipo);
                                $("#categoria").val(dat.catid);
				$("#public_"+dat.publica).prop('checked', true);
				//console.log(dat.contenido);
				//$('textarea[name="textarea"]').html(dat.contenido);
				$('#summernote').code(dat.contenido);
				
				$("#ModalEditNew").modal('show');
            },
            error: function(x, e) {
                $.gritter.add({
				        title: 'Editar Curso',
				        text: 'Ocurrio un error, intentelo nuevamente.' + e.messager,
				        class_name: 'danger'
				});
            }
        });
}


function UpdateNew() {//Agregar usuario
   if (validarTituloNoticia() & validarTipoNoticia()) {
    	$('textarea[name="textarea"]').html($('#summernote').code());
        $.ajax({
            url: "news_controller/update_new",
            data: $("#frmUpdNoticia").serialize(),
            type: "POST",
            async: 'true',
            beforeSend: function() {
                //console.log('Procesando, espere por favor...');
            },
            success: function(response) {
                if (response == 1) {
                	$("#ModalEditComu").modal('hide');
                    $.gritter.add({
				        title: 'Editar Noticia',
				        text: 'Se modifico correctamente la Noticia.',
				        class_name: 'success'
				      });
                    setTimeout ("window.location.reload();", 2300);//setTimeout("loadSection('user_controller','agasajo');", 2000);
                } else {
                    $.gritter.add({
				        title: 'Editar Noticia',
				        text: 'Ocurrio un error y no se pudo modificarla Noticia, intentelo nuevamente.',
				        class_name: 'danger'
				      });
                }
            },
            error: function(x, e) {
                $.gritter.add({
				        title: 'Editar Noticia',
				        text: 'Ocurrio un error y no se pudo modificar la Noticia, intentelo nuevamente.' + e.messager,
				        class_name: 'danger'
				});
            }
        });
    } else {
        return false;
    }
}


function newAddCate(){
    if ($("#cate").val().length < 2 || $("#cate").val() == null || /^\s+$/.test($("#cate").val())) {
        $.gritter.add({
            title: 'Agregar categoria',
            text: 'Favor de agregar el nombre de la nueva categoria',
            class_name: 'danger'
	});
        return;
    } 
    
    $.ajax({
            url: "news_controller/categoryAdd",
            data: {cate: $("#cate").val()},
            type: "POST",
            async: 'true',
            beforeSend: function() {
                //console.log('Procesando, espere por favor...');
            },
            success: function(response) {
                if (response == 1) {
                	$("#ModalEditComu").modal('hide');
                    $.gritter.add({
				        title: 'Agregar categoria',
				        text: 'Se agrego correctamente la categoria.',
				        class_name: 'success'
				      });
                    setTimeout ("window.location.reload();", 2300);//setTimeout("loadSection('user_controller','agasajo');", 2000);
                } else {
                    $.gritter.add({
				        title: 'Agregar categoria',
				        text: 'Ocurrio un error y no se pudo agregar la categoria, intentelo nuevamente.',
				        class_name: 'danger'
				      });
                }
            },
            error: function(x, e) {
                $.gritter.add({
				        title: 'Agregar categoria',
				        text: 'Ocurrio un error y no se pudo agregar la categoria, intentelo nuevamente.' + e.messager,
				        class_name: 'danger'
				});
            }
    });
    
}



function statusCate(id, status){
	$.ajax({
            url: "news_controller/update_status_cate",
            data: {id: id, status: status},
            type: "POST",
            async: 'true',
            success: function(response) {
                if (response == 1) {
                    $.gritter.add({
				        title: 'Cambiar Status de categoria',
				        text: 'Se modifico correctamente la categoria.',
				        class_name: 'success'
				      });
                    setTimeout ("window.location.reload();", 2300);//setTimeout("loadSection('user_controller','agasajo');", 2000);
                } else {
                    $.gritter.add({
				        title: 'Cambiar Status de categoria',
				        text: 'Ocurrio un error y no se pudo modificar la categoria, intentelo nuevamente.',
				        class_name: 'danger'
				      });
                }
            },
            error: function(x, e) {
                $.gritter.add({
				        title: 'Cambiar Status de categoria',
				        text: 'Ocurrio un error y no se pudo modificar la categoria, intentelo nuevamente.' + e.messager,
				        class_name: 'danger'
				});
            }
        });
}


function editCate(id) {
        $.ajax({
            url: "news_controller/getCate",
            data: {id: id},
            type: "POST",
            async: 'true',
            success: function(response) {
                console.log(response);
				var dat = jQuery.parseJSON(response);
				$("#cate_id").val(id);
				$("#categoriaUpdate").val(dat.categoria);
				
				$("#ModalEditNew").modal('show');
            },
            error: function(x, e) {
                $.gritter.add({
				        title: 'Editar categoria',
				        text: 'Ocurrio un error, intentelo nuevamente.' + e.messager,
				        class_name: 'danger'
				});
            }
        });
}


function UpdateCate() {//Agregar usuario
    if ($("#categoriaUpdate").val().length < 2 || $("#categoriaUpdate").val() == null || /^\s+$/.test($("#categoriaUpdate").val())) {
        $.gritter.add({
            title: 'Editar categoria',
            text: 'Favor de agregar el nombre de la categoria',
            class_name: 'danger'
	});
        return;
    } 
        $.ajax({
            url: "news_controller/categoryUpdate",
            data: {id: $("#cate_id").val(), cate: $("#categoriaUpdate").val()},
            type: "POST",
            async: 'true',
            success: function(response) {
                if (response == 1) {
                	$("#ModalEditComu").modal('hide');
                    $.gritter.add({
				        title: 'Editar categoria',
				        text: 'Se modifico correctamente la categoria.',
				        class_name: 'success'
				      });
                    setTimeout ("window.location.reload();", 2300);//setTimeout("loadSection('user_controller','agasajo');", 2000);
                } else {
                    $.gritter.add({
				        title: 'Editar categoria',
				        text: 'Ocurrio un error y no se pudo modificar la categoria, intentelo nuevamente.',
				        class_name: 'danger'
				      });
                }
            },
            error: function(x, e) {
                $.gritter.add({
				        title: 'Editar Noticia',
				        text: 'Ocurrio un error y no se pudo modificar la Noticia, intentelo nuevamente.' + e.messager,
				        class_name: 'danger'
				});
            }
        });
    
}







