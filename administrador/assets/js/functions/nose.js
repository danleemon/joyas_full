var serverLms =  location.host + "/";
var server = location.host + "/admin/";

function learner_status(learner_id, status)
{
    $.ajax({
        url: server + "learners_controller/update_status",
        data: {learner_id: learner_id, status: status},
        type: "POST",
        async: 'true',
        beforeSend: function() {
            console.log('Procesando en learners, espere por favor...');
        },
        success: function(response) {
            loadSection('learners_controller/edit_learners_view', 'page-wrapper');
        },
        error: function(x, e) {
            alert("Ocurrio un error: " + e.messager);
        }
    });
}
function user_editar(id_user)
{
    var nombre_edit = $("#nombre_edit_user_" + id_user).val();
    var username_edit = $("#username_edit_user_" + id_user).val();
    if (nombre_edit.length < 2 || nombre_edit == null || /^\s+$/.test(nombre_edit)) {
        $("#nombre_edit_user_" + id_user).addClass("error");
        return false;
    }
    if (username_edit.length < 2 || username_edit == null || /^\s+$/.test(username_edit)) {
        $("#username_edit_user_" + id_user).addClass("error");
        return false;
    }
    $.ajax({
        url: server + "user_controller/update_user",
        data: {id_user: id_user, nombre: nombre_edit, username: username_edit},
        type: "POST",
        async: 'true',
        beforeSend: function() {
            console.log('Procesando, espere por favor...');
        },
        success: function(response) {
            if (response == 1) {
                $("#datos_" + id_user).html('<h4 class="text-info">Los datos fueron modificados correctamente</h4>');
                $("#boton_" + id_user).html('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
                setTimeout("loadSection('user_controller/get_user','page-content');", 2000);
            }
        },
        error: function(x, e) {
            alert("Ocurrio un error: " + e.messager);
        }
    });
}
function learner_editar(learner_id)
{
    var learner_id = $("#learner_id").val();
    var nombre_edit = $("#nombre_edit_user").val();
    var last_name_edit = $("#last_name_edit_user").val();
    var username_edit = $("#username_edit_user").val();
    var cellular_edit = $("#cellular_edit_user").val();
    var phone_edit = $("#phone_edit_user").val();
    var email_edit = $("#email_edit_user").val();
    var date_birth_edit = $("#date_birth_edit_user").val();
    var address_edit = $("#address_edit_user").val();
    var pincode = $('#pincode').val();
    var puesto = $('#puesto').val();
    //alert(nombre_edit+username_edit+phone_edit+cellular_edit+email_edit);
    if (nombre_edit.length < 2 || nombre_edit == null || /^\s+$/.test(nombre_edit)) {
        $("#nombre_edit_user").addClass("error");
        return false;
    }
    if (username_edit.length < 2 || username_edit == null || /^\s+$/.test(username_edit)) {
        $("#username_edit_user").addClass("error");
        return false;
    }
    
    console.log("Id ", learner_id);
    console.log("Nombre ", nombre_edit);
    console.log("Apellido", last_name_edit);
    console.log("Username", username_edit);
    console.log("Celular", cellular_edit);
    console.log("Teléfono", phone_edit);
    console.log("Email", email_edit);
    console.log("Fecha nac", date_birth_edit);
    console.log("Dir", address_edit);
    console.log("Pincode", pincode);
    console.log("Puesto", puesto);
    
    //*
    var url = "/admin/learners_controller/update_learner";
    console.log('URL: ' + url);
    $.ajax({
        url: url,
        data: { learner_id: learner_id, 
                name: nombre_edit, 
                username: username_edit, 
                cellular: cellular_edit, 
                phone: phone_edit, 
                email: email_edit, 
                last_name: last_name_edit, 
                date_birth: date_birth_edit, 
                address: address_edit,
                puesto: puesto
        },
        type: "POST",
        async: 'true',
        beforeSend: function() {
            console.log('Procesando, espere por favor...');
        },
        success: function(response) {
            console.log(response);
            if (response == '1') {
                //$("#datos_").html('<h4 class="text-info">Los datos fueron modificados correctamente</h4>');
                $.notify("Los datos fueron modificados correctamente", "success");
                $("#boton_").html('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
                setTimeout("loadSection('learners_controller/edit_learners_view','page-content');", 2000);
            }
            else {
                alert(response);
            }
        },
        error: function(x, e) {
            alert("Ocurrio un error: " + e.message);
        }
    });
    // */
}
function reset_pass(id_user)
{
    $.ajax({
        url: "user_controller/reset_pass",
        data: {id_user: id_user},
        type: "POST",
        async: 'true',
        beforeSend: function() {
            console.log('Procesando, espere por favor...');
        },
        success: function(response) {
            console.log(response);
            $("#passNew_" + id_user).html('<h4>La nueva contraseña es:</h4> <h3 class="text-warning orange">' + response + '</h3>');
        },
        error: function(x, e) {
            alert("Ocurrio un error: " + e.messager);
        }
    });
}
function reset_pass_learner(id_user)
{
    $.ajax({
        url: "learners_controller/reset_pass",
        data: {learner_id: id_user},
        type: "POST",
        async: 'true',
        beforeSend: function() {
            console.log('Procesando, espere por favor...');
        },
        success: function(response) {
            console.log(response);
            $("#passNew").html('<h4>La nueva contraseña es:</h4> <h3 class="text-warning orange">' + response + '</h3>');
        },
        error: function(x, e) {
            alert("Ocurrio un error: " + e.messager);
        }
    });
}
// -------------------------------------------------- EDITAR USUARIO --------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------

// --------------------------------------------------------------------------------------------------------------------
// -------------------------------------------------- EDITAR ARCHIVO --------------------------------------------------
function file_status(id_file, status)
{
    $.ajax({
        url: server + "files_controller/update_status",
        data: {id_file: id_file, status: status},
        type: "POST",
        async: 'true',
        beforeSend: function() {
            console.log('Procesando, espere por favor...');
        },
        success: function(response) {
            console.log(response);
            loadSection('files_controller', 'page-content');
        },
        error: function(x, e) {
            alert("Ocurrio un error: " + e.messager);
        }
    });
}
function file_eliminar(id_file)
{
    $.ajax({
        url: server + "files_controller/eliminar_file",
        data: {id_file: id_file},
        type: "POST",
        async: 'true',
        beforeSend: function() {
            console.log('Procesando, espere por favor...');
        },
        success: function(response) {
            loadSection('files_controller', 'page-content');
        },
        error: function(x, e) {
            alert("Ocurrio un error: " + e.messager);
        }
    });
}
function file_editar(id_file)
{
    var nombre_edit = $("#nombre_edit_file_" + id_file).val();
    var tipo_file = $("#tipo_edit_file_" + id_file).val();
    if (nombre_edit.length < 2 || nombre_edit == null || /^\s+$/.test(nombre_edit)) {
        $("#nombre_edit_file_" + id_user).addClass("error");
        return false;
    }
    $.ajax({
        url: server + "files_controller/update_file",
        data: {id_file: id_file, nombre: nombre_edit, category: tipo_file},
        type: "POST",
        async: 'true',
        beforeSend: function() {
            console.log('Procesando, espere por favor...');
        },
        success: function(response) {
            if (response == 1) {
                $("#datos_" + id_file).html('<h4 class="text-info">Los datos fueron modificados correctamente</h4>');
                $("#boton_" + id_file).html('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
                setTimeout("loadSection('files_controller','page-content');", 2000);
            }
        },
        error: function(x, e) {
            alert("Ocurrio un error: " + e.messager);
        }
    });
}
// -------------------------------------------------- EDITAR ARCHIVO --------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------
function showPicture(img)
{
    $("#dvTrans").html('<br><br><img src="' + serverLms + img + '"/>')
    $("#dvTrans").show(300);
}

function FormAgregarfile2() {//Agregar usuario
    var nombre = $("#nombre_file").val();
    var categoria = $("#tipo_file").val();
    var archivo = $("#id-input-file-2").val();

    console.log("nombre: " + nombre + " / categoria: " + categoria + " / archivo: " + archivo);
    var data = $("#FormAddFile").serialize();
    console.log("DATA:" + data);
    var realdata = $("#FormAddFile").serialize();
    console.log("realdata: " + realdata);
    if (true) {
        $.ajax({
            url: server + "files_controller/add_file",
            data: realdata,
            type: "POST",
            async: 'true',
            beforeSend: function() {
                console.log('Procesando archivos, espere por favor...');
            },
            success: function(response) {
                if (response == 1) {
                    $('#DivAddUser').html('<h1 class="text-success">Se añadio correctamente el archivo</h1>');
                    //setTimeout ("loadSection('user_controller/get_user','page-content');", 2000);
                }
                else {
                    $('#DivAddUser').html('<h1 class="text-danger">Ocurrio un error y no se pudo añadir el archivo, intentelo nuevamente.</h1>');
                }
            },
            error: function(x, e) {
                alert("Ocurrio un error:" + e.messager);
            }
        });
    }
    else {
        return false;
    }
}
