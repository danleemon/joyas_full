$(document).ready(function(){
	$("#nombre").blur(validarNombrePro);
	$("#nombre").keyup(validarNombrePro);
	$("#descripcion").blur(validarDescProg);
	$("#descripcion").keyup(validarDescProg);
	$("#fecha_inicio").blur(validarIniPro);
	$("#fecha_inicio").keyup(validarIniPro);
	$("#fecha_fin").blur(validarFinPro);
	$("#fecha_fin").keyup(validarFinPro);
});	

function AddPrograma() {//Agregar usuario
    if (validarNombrePro() & validarDescProg() & validarIniPro() & validarFinPro()) {
        $.ajax({
            url: "programs_controller/add_program",
            data: $("#frmAddPrograma").serialize(),
            type: "POST",
            async: 'true',
            beforeSend: function() {
                //console.log('Procesando, espere por favor...');
            },
            success: function(response) {
                if (response == 1) {
                    $.gritter.add({
				        title: 'Agregar Programa',
				        text: 'Se añadio correctamente el programa.',
				        class_name: 'success'
				      });
                    setTimeout ("window.location.reload();", 2300);
                } else {
                    $.gritter.add({
				        title: 'Agregar Programa',
				        text: 'Ocurrio un error y no se pudo añadir el programa, intentelo nuevamente.',
				        class_name: 'danger'
				      });
                }
            },
            error: function(x, e) {
                $.gritter.add({
				        title: 'Agregar Programa',
				        text: 'Ocurrio un error y no se pudo añadir el programa, intentelo nuevamente.' + e.messager,
				        class_name: 'danger'
				});
            }
        });
    } else {
        return false;
    }
}



function validarNombrePro() {
    if ($("#nombre").val().length < 2 || $("#nombre").val() == null || /^\s+$/.test($("#nombre").val())) {
        $("#nombre").addClass("error");
        return false;
    } else {
        $("#nombre").removeClass("error");
        return true;
    }
}

function validarDescProg() {
    if ($("#descripcion").val().length < 2 || $("#descripcion").val() == null || /^\s+$/.test($("#descripcion").val())) {
        $("#descripcion").addClass("error");
        return false;
    } else {
        $("#descripcion").removeClass("error");
        return true;
    }
}

function validarIniPro() {
    if ($("#fecha_inicio").val().length < 2 || $("#fecha_inicio").val() == null || /^\s+$/.test($("#fecha_inicio").val())) {
        $("#fecha_inicio").addClass("error");
        return false;
    } else {
        $("#fecha_inicio").removeClass("error");
        return true;
    }
}

function validarFinPro() {
    if ($("#fecha_fin").val().length < 2 || $("#fecha_fin").val() == null || /^\s+$/.test($("#fecha_fin").val())) {
        $("#fecha_fin").addClass("error");
        return false;
    } else {
        $("#fecha_fin").removeClass("error");
        return true;
    }
}





function edit_program(id) {
        $.ajax({
            url: "programs_controller/get_program",
            data: {id: id},
            type: "POST",
            async: 'true',
            beforeSend: function() {
                //console.log('Procesando, espere por favor...');
            },
            success: function(response) {
                //console.log(response);
				var dat = jQuery.parseJSON(response);
				$("#program_id_upd").val(id);
				$("#nombre").val(dat.nombre);
				$("#descripcion").val(dat.descripcion);
				$("#nivel").val(dat.nivel);
				$("#fecha_inicio").val(dat.fecha_inicio);
				$("#fecha_fin").val(dat.fecha_fin);
				
				$("#ModalEditProgram").modal('show');
            },
            error: function(x, e) {
                $.gritter.add({
				        title: 'Editar Programa',
				        text: 'Ocurrio un error, intentelo nuevamente.' + e.messager,
				        class_name: 'danger'
				});
            }
        });
}

function UpdateProgram() {//Agregar usuario
   if (validarNombrePro() & validarDescProg() & validarIniPro() & validarFinPro()) {
        $.ajax({
            url: "programs_controller/update_program",
            data: $("#frmUpdPrograma").serialize(),
            type: "POST",
            async: 'true',
            beforeSend: function() {
                //console.log('Procesando, espere por favor...');
            },
            success: function(response) {
                if (response == 1) {
                	$("#ModalEditComu").modal('hide');
                    $.gritter.add({
				        title: 'Editar Programa',
				        text: 'Se modifico correctamente el Programa.',
				        class_name: 'success'
				      });
                    setTimeout ("window.location.reload();", 2300);//setTimeout("loadSection('user_controller','agasajo');", 2000);
                } else {
                    $.gritter.add({
				        title: 'Editar Programa',
				        text: 'Ocurrio un error y no se pudo modificar el Programa, intentelo nuevamente.',
				        class_name: 'danger'
				      });
                }
            },
            error: function(x, e) {
                $.gritter.add({
				        title: 'Editar Programa',
				        text: 'Ocurrio un error y no se pudo modificar el Programa, intentelo nuevamente.' + e.messager,
				        class_name: 'danger'
				});
            }
        });
    } else {
        return false;
    }
}



function status_program(id, status){
	$.ajax({
            url: "programs_controller/update_status",
            data: {id: id, status: status},
            type: "POST",
            async: 'true',
            beforeSend: function() {
                //console.log('Procesando, espere por favor...');
            },
            success: function(response) {
                if (response == 1) {
                    $.gritter.add({
				        title: 'Cambiar Status de Programa',
				        text: 'Se modifico correctamente el Programa.',
				        class_name: 'success'
				      });
                    setTimeout ("window.location.reload();", 2300);//setTimeout("loadSection('user_controller','agasajo');", 2000);
                } else {
                    $.gritter.add({
				        title: 'Cambiar Status de Programa',
				        text: 'Ocurrio un error y no se pudo modificar el Programa, intentelo nuevamente.',
				        class_name: 'danger'
				      });
                }
            },
            error: function(x, e) {
                $.gritter.add({
				        title: 'Cambiar Status de Programa',
				        text: 'Ocurrio un error y no se pudo modificar el Programa, intentelo nuevamente.' + e.messager,
				        class_name: 'danger'
				});
            }
        });
}


function edit_program_courses(id) {
        $.ajax({
            url: "programs_controller/get_cursos",
            data: {id: id},
            type: "POST",
            async: 'true',
            success: function(response) {
                //console.log(response);
                if(response == 0){
                	$("#list1").html('<div class="dd-empty"></div>');
                }else{
                	$("#list1").html('<ol class="dd-list buscad">'+response+'</ol>');	
                }
				$("#program_id_courses").val(id);
            }
        });
        
        $.ajax({
            url: "programs_controller/get_cursos_program",
            data: {id: id},
            type: "POST",
            async: 'true',
            success: function(response) {
                //console.log(response);
				if(response == 0){
                	$("#list2").html('<div class="dd-empty"></div>');
                }else{
                	$("#list2").html('<ol class="dd-list">'+response+'<ol class="dd-list">');	
                }
            }
        });
        
        
		$("#CursosProgram").modal('show');
}

function UpdateCursosProgram(id){
	var agreg = $('#list2').nestable('serialize');
	$.ajax({//eliminar cursos del programa
            url: "programs_controller/delete_cursos_program",
            data: {id: id},
            type: "POST",
            async: 'true',
            success: function(response) {
                //console.log(response);
            }
    });
    var ids = '';
    $.each(agreg,function(index,contenido){//----- each clientes
        ids = ids+','+contenido.id;
	});
    setTimeout ("AddCursosProgram("+id+",'"+ids+"');", 2000);
    
}


function AddCursosProgram(id,ids){
    ids = ids.substr(1);
    var cuantos = ids.split(',');
    var si = 0;
    //console.log("ids: "+ids+" -- cuantos: "+cuantos.length+ " -- si: "+si);
    
    for(i=0;i<cuantos.length;i++){
        $.ajax({
                    url: "programs_controller/add_program_course",
                    data: {program_id: id, course_id: cuantos[i], position: i},
                    type: "POST",
                    async: 'false',
                    success: function(response) {
                        /*console.log("inserto---> c:"+cuantos[i]+" po:"+i+" - pr:"+id+" - r:"+response);
                        if(response == 1){
                            si = si + 1;
                            console.log("siii"+si);
                        }*/
                    }

        });
        
            si = si + 1;
        
    }
    
                //console.log(si+"-"+cuantos.length);
                if(si == cuantos.length){
                        $.gritter.add({
                            title: 'Agregar cursos Programa',
                            text: 'Se agregaron correctamente los cursos al programa.',
                            class_name: 'success'
                        });
                }
                
    
        
	setTimeout ("window.location.reload();", 2000);
}