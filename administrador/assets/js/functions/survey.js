function loadSection(controller, divSel)//Controlador,Div en el que se despliega la vista
{
    $.ajax({
        url: controller,
        async: 'true',
        cache: false,
        contentType: "application/x-www-form-urlencoded",
        dataType: "html",
        error: function(object, error, anotherObject) {
            alert('Mensaje: ' + object.statusText + 'Status: ' + object.status);
        },
        global: true,
        ifModified: false,
        processData: true,
        success: function(result) {
            //console.log(result);
            $('#' + divSel).html(result);
            
        },
        timeout: 30000,
        type: "GET"
    });
}

function addSurvey(){
        if (valNombreSurveyAdd() & valDateSurveyAdd()){
            var name_surv = $("#nombre_surv").val();
            $.ajax({
                url: "survey_controller/save_survey",
                data: $("#frmSurveyAdd").serialize(),
                type: "POST",
                async: 'true',
                beforeSend: function() {
                    //console.log('Procesando, espere por favor...');
                },
                success: function(response) {
                    if (response > 0) {
                        $.gritter.add({
                            title: 'Agregar evaluación',
                            text: 'Favor de agregar las preguntas y respuestas de la evaluación.',
                            class_name: 'success'
                        });
                        //setTimeout ("window.location.reload();", 2300);//setTimeout("loadSection('user_controller','agasajo');", 2000);
                        loadSection("survey_controller/addQuestions/"+response+'/'+name_surv, "agasajo");//pcont
                        
                    } else {
                        $.gritter.add({
                            title: 'Agregar evaluación',
                            text: 'Ocurrio un error y no se pudo agregar la evaluación, intentelo nuevamente.',
                            class_name: 'danger'
                        });
                    }
                },
                error: function(x, e) {
                    $.gritter.add({
                        title: 'Agregar evaluación',
                        text: 'Ocurrio un error y no se pudo agregar la evaluación, intentelo nuevamente.' + e.messager,
                        class_name: 'danger'
                    });
                }
            });
        }  
}




function valNombreSurveyAdd() {
    if ($("#nombre_surv").val().length < 2 || $("#nombre_surv").val() == null || /^\s+$/.test($("#nombre_surv").val())) {
        $("#nombre_surv").addClass("error");
        return false;
    } else {
        $("#nombre_surv").removeClass("error");
        return true;
    }
}

function valDateSurveyAdd() {
    if ($("#fecha_fin_surv").val().length < 2 || $("#fecha_fin_surv").val() == null || /^\s+$/.test($("#fecha_fin_surv").val())) {
        $("#fecha_fin_surv").addClass("error");
        return false;
    } else {
        $("#fecha_fin_surv").removeClass("error");
        return true;
    }
}


//  -------------------------------------


function addQuestionSurvey(){
    var cuantos = parseInt($("#CountQuestions").val())+1;
    //cuantos = parseInt(cuantos)+1;
    $("#CountQuestions").val(cuantos);
    $("#preguntasAdd").append('<div class="form-group">'+
                                        '<label style="font-size: 20px;" for="preg1" class="color-primary">Pregunta '+cuantos+'</label><input type="text" class="form-control" placeholder="Pregunta '+cuantos+'" id="preg'+cuantos+'" name="preg'+cuantos+'">'+
                                        '<label for="resp1">Respuestas</label>'+
                                        '<div class="input-group">'+
                                            '<input type="text" class="form-control" placeholder="Agregar Respuesta" id="resp'+cuantos+'" name="resp'+cuantos+'">'+
                                            '<span class="input-group-btn">'+
                                                '<button onclick="addAnswerQuestion('+cuantos+');" class="btn btn-primary" type="button"><i class="fa fa-plus"></i> Agregar</button>'+
                                            '</span>'+
                                        '</div>'+
                                        '<input type="hidden" name="CountAnswer'+cuantos+'" id="CountAnswer'+cuantos+'" value="0">'+
                                        '<input type="hidden" name="idAnswer'+cuantos+'" id="idAnswer'+cuantos+'" value="">'+
                                        '<input type="hidden" name="idAnswerCorrect'+cuantos+'" id="idAnswerCorrect'+cuantos+'" value="0">'+
                                        '<div id="Answer'+cuantos+'"></div>'+
                                    '</div>');
    
}

function addAnswerQuestion(idQuestion){
    if ($("#resp"+idQuestion).val().length < 1 || $("#resp"+idQuestion).val() == null || /^\s+$/.test($("#resp"+idQuestion).val())) {
        $.gritter.add({
                    title: 'Agregar Respuesta',
                    text: 'Favor de llenar el campo de la respuesta',
                    class_name: 'danger'
        });
        $("#resp"+idQuestion).focus();
        return;
    }
    var cuantos = parseInt($("#CountAnswer"+idQuestion).val())+1;
    $("#CountAnswer"+idQuestion).val(cuantos);
    var resp = $("#resp"+idQuestion).val();
    var ids = $("#idAnswer"+idQuestion).val();
    ids = ids+','+cuantos;
    $("#idAnswer"+idQuestion).val(ids);
    
    $("#Answer"+idQuestion).append('<span id="AnsQues'+idQuestion+'_'+cuantos+'" class="badge badge-primary"><input type="hidden" name="idAnswer'+idQuestion+'_'+cuantos+'" id="idAnswer'+idQuestion+'_'+cuantos+'" value="'+resp+'">'+resp+'<span><a style="cursor: pointer;font-weight: bold; color: #000; margin-left: 10px;" title="Eliminar respuesta" onclick="deleteAnswerQuestion('+idQuestion+','+cuantos+');">X</a></span><span class="answ_correct_'+idQuestion+'"><a style="cursor: pointer;font-weight: bold; color: #DBF6D3; margin-left: 5px;" title="Respuesta correcta" onclick="AnswerCorrectQuestion('+idQuestion+','+cuantos+');"><i class="fa fa-thumbs-up pull-right"></i></a></span></span>');
    $("#resp"+idQuestion).val('');
    $("#resp"+idQuestion).focus();
}

function AnswerCorrectQuestion(idQuestion,idAnswer){
    var ids_resp = $("#idAnswer"+idQuestion).val();
    var ids = ids_resp.split(',');
    for(i=1;i<ids.length;i++){
        $("#AnsQues"+idQuestion+'_'+ids[i]).removeClass('badge-success');
        $("#AnsQues"+idQuestion+'_'+ids[i]).addClass('badge-primary');
    }
    
    
    //$(".answ_correct_"+idQuestion).fadeOut(500);
    $("#idAnswerCorrect"+idQuestion).val(idAnswer);
    
    //CORRECT
    $("#AnsQues"+idQuestion+'_'+idAnswer).removeClass('badge-primary');
    $("#AnsQues"+idQuestion+'_'+idAnswer).addClass('badge-success');
}


function deleteAnswerQuestion(idQuestion,idAnswer){
    var ids = $("#idAnswer"+idQuestion).val();
    ids = ids.replace(","+idAnswer, "");
    $("#idAnswer"+idQuestion).val(ids);
    $('#AnsQues'+idQuestion+'_'+idAnswer).fadeOut(500);
    $('#AnsQues'+idQuestion+'_'+idAnswer).html('');
    if(idAnswer == $("#idAnswerCorrect"+idQuestion).val()){
        $("#idAnswerCorrect"+idQuestion).val('0');
    }
}

function addSurveyQuestions(){
    var cuantas = $("#CountQuestions").val();
    
    var msg = '';
    for(i=1;i<=cuantas;i++){
        if($("#idAnswerCorrect"+i).val() == 0){
            msg = msg+"Seleccionar la respuesta correcta de la pregunta "+i+"<br>";
        }
        
        if ($("#preg"+i).val().length < 1 || $("#preg"+i).val() == null || /^\s+$/.test($("#preg"+i).val())) {
            msg = msg+"Agregar la pregunta "+i+"<br>";
        }
    }
    
    if(msg.length > 3){
        $.gritter.add({
                    title: 'Agregar evaluación',
                    text: 'Favor de: <br> '+msg,
                    class_name: 'danger'
        });
        return;
    }
    
    $.ajax({
            url: "survey_controller/save_questions",
            data: $("#frmSurveyAddQuestions").serialize(),
            type: "POST",
            async: 'true',
            beforeSend: function() {
                //console.log('Procesando, espere por favor...');
            },
            success: function(response) {
                //console.log(response);
                    if (response == 1) {
                        $.gritter.add({
                            title: 'Agregar evaluación',
                            text: 'Se agrego la evaluación correctamente.',
                            class_name: 'success'
                        });
                        setTimeout ("window.location.reload();", 2300);
                        
                    } else {
                        $.gritter.add({
                            title: 'Agregar evaluación',
                            text: 'Ocurrio un error y no se pudieron agregar las preguntas de la evaluación, intentelo nuevamente.',
                            class_name: 'danger'
                        });
                    }
            },
            error: function(x, e) {
                    $.gritter.add({
                        title: 'Agregar evaluación',
                        text: 'Ocurrio un error y no se pudieron agregar las preguntas de la evaluación, intentelo nuevamente.' + e.messager,
                        class_name: 'danger'
                    });
            }
    });
}




function statusSurvey(id, status){
	$.ajax({
            url: "survey_controller/update_status",
            data: {id: id, status: status},
            type: "POST",
            async: 'true',
            beforeSend: function() {
                //console.log('Procesando, espere por favor...');
            },
            success: function(response) {
                if (response == 1) {
                    $.gritter.add({
				        title: 'Cambiar Status de Evaluación',
				        text: 'Se modifico correctamente la Evaluación.',
				        class_name: 'success'
				      });
                    setTimeout ("window.location.reload();", 2300);//setTimeout("loadSection('user_controller','agasajo');", 2000);
                } else {
                    $.gritter.add({
				        title: 'Cambiar Status de Evaluación',
				        text: 'Ocurrio un error y no se pudo modificar la Evaluación, intentelo nuevamente.',
				        class_name: 'danger'
				      });
                }
            },
            error: function(x, e) {
                $.gritter.add({
				        title: 'Cambiar Status de Evaluación',
				        text: 'Ocurrio un error y no se pudo modificar la Evaluación, intentelo nuevamente.' + e.messager,
				        class_name: 'danger'
				});
            }
        });
}


function editSurvey(id) {
        $.ajax({
            url: "survey_controller/get_survey",
            data: {id: id},
            type: "POST",
            async: 'true',
            beforeSend: function() {
                //console.log('Procesando, espere por favor...');
            },
            success: function(response) {
                //console.log(response);
		var dat = jQuery.parseJSON(response);
		$("#survey_id_upd").val(id);
		$("#nombre_surv").val(dat.title);
		$("#desc_surv").val(dat.description);
                $("#fecha_fin_surv").val(dat.dateEnd);
                $("#intentos").val(dat.attempts);
                $("#aprov").val(dat.aprove_score);
				$("#ModalEditSurvey").modal('show');
		
            },
            error: function(x, e) {
                $.gritter.add({
                    title: 'Editar Evaluación',
                    text: 'Ocurrio un error, intentelo nuevamente.' + e.messager,
                    class_name: 'danger'
		});
            }
        });
}

function UpdateSurvey() {
        $.ajax({
            url: "survey_controller/update_survey",
            data: $("#frmSurveyUpdate").serialize(),
            type: "POST",
            async: 'true',
            beforeSend: function() {
                //console.log('Procesando, espere por favor...');
            },
            success: function(response) {
                if (response == 1) {
                    $("#ModalEditSurvey").modal('hide');
                    $.gritter.add({
                        title: 'Editar Evaluación',
			text: 'Se modifico correctamente la Evaluación.',
			class_name: 'success'
                    });
                    setTimeout ("window.location.reload();", 2300);//setTimeout("loadSection('user_controller','agasajo');", 2000); 
                } else {
                    $.gritter.add({
			title: 'Editar Evaluación',
			text: 'Ocurrio un error y no se pudo modificar la Evaluación, intentelo nuevamente.',
			class_name: 'danger'
                    });
                }
            },
            error: function(x, e) {
                $.gritter.add({
				        title: 'Editar Evaluación',
				        text: 'Ocurrio un error y no se pudo modificar la Evaluación, intentelo nuevamente.' + e.messager,
				        class_name: 'danger'
				});
            }
        });
}









function editQuestionsSurvey(id){
    $.ajax({
            url: "survey_controller/get_questions_survey",
            data: {id: id},
            type: "POST",
            async: 'true',
            beforeSend: function() {
                //console.log('Procesando, espere por favor...');
            },
            success: function(response) {
                //console.log(response);
                //traer todas las preguntas y respuestas para editarlas
                $("#QuestionsEditSurvey").html(response);
                $("#UpdateQuestions").modal('show');
            },
            error: function(x, e) {
                $.gritter.add({
                    title: 'Editar Evaluación',
                    text: 'Ocurrio un error, intentelo nuevamente.' + e.messager,
                    class_name: 'danger'
		});
            }
    });
}


function UpdateSubmitQuestions(){
    var cuantas = $("#CountQuestions").val();
    
    var msg = '';
    for(i=1;i<=cuantas;i++){
        if($("#idAnswerCorrect"+i).val() == 0){
            msg = msg+"Seleccionar la respuesta correcta de la pregunta "+i+"<br>";
        }
        
        if ($("#preg"+i).val().length < 2 || $("#preg"+i).val() == null || /^\s+$/.test($("#preg"+i).val())) {
            msg = msg+"Agregar la pregunta "+i+"<br>";
        }
    }
    
    if(msg.length > 3){
        $.gritter.add({
                    title: 'Agregar evaluación',
                    text: 'Favor de: <br> '+msg,
                    class_name: 'danger'
        });
        return;
    }
    
    var idsQuestionDelete = $("#questions_ids_delete").val();
    var idEval = $("#EvalId").val();
    
    $.ajax({
            url: "../survey_controller/delete_for_edit",
            data: {answers: idsQuestionDelete, questions: idEval},
            type: "POST",
            async: 'true',
            beforeSend: function() {
                //console.log("borrandoooo");
            },
            success: function(response) {
                //console.log("borro: "+response);
                $.ajax({
                        url: "../survey_controller/save_questions",
                        data: $("#frmSurveyAddQuestions").serialize(),
                        type: "POST",
                        async: 'true',
                        beforeSend: function() {
                            //console.log("agregandooooo");
                        },
                        success: function(response) {
                            //console.log(response);
                                if (response == 1) {
                                    $.gritter.add({
                                        title: 'Editar evaluación',
                                        text: 'Se agrego la evaluación correctamente.',
                                        class_name: 'success'
                                    });
                                    setTimeout ("window.location = '../evaluacion';", 2300);

                                } else {
                                    $.gritter.add({
                                        title: 'Editar evaluación',
                                        text: 'Ocurrio un error y no se pudieron agregar las preguntas de la evaluación, intentelo nuevamente.',
                                        class_name: 'danger'
                                    });
                                }
                        },
                        error: function(x, e) {
                                $.gritter.add({
                                    title: 'Editar evaluación',
                                    text: 'Ocurrio un error y no se pudieron agregar las preguntas de la evaluación, intentelo nuevamente.' + e.messager,
                                    class_name: 'danger'
                                });
                        }
                });
            },
            error: function(x, e) {
                $.gritter.add({
                    title: 'Editar Evaluación',
                    text: 'Ocurrio un error, intentelo nuevamente.' + e.messager,
                    class_name: 'danger'
		});
            }
    });
    
    //console.log("eliminar respuestas de las preguntas "+idsQuestionDelete+" y las preguntas de la evaluacion: "+idEval);
}