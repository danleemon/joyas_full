var serverLms =  location.host + "/";
var server = location.host + "/admin/";
// --------------------------------------------------------------------------------------------------------------------
// -------------------------------------------------- AGRGAR USUARIO --------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------

$(document).ready(function(){
	/*$("#nombre").blur(validarNombreAdd);
	$("#nombre").keyup(validarNombreAdd);
	$("#username").blur(validaUsernameAdd);
	$("#username").keyup(validaUsernameAdd);
	$("#pass").blur(validarPassAdd);
	$("#pass").keyup(validarPassAdd);
	$("#tipo_user").blur(validarTipoAdd);
	$("#tipo_user").change(validarTipoAdd);
	$("#categoria").blur(validarTipoCat);
	$("#categoria").change(validarTipoCat);*/
});	


function tip_us(tipo){
	/*if(tipo == 2){
		$("#g_cate").fadeIn(400);
	}else{
		$("#g_cate").fadeOut(400);
	}*/
}
	

function validarTipoAdd(){
		cu = document.getElementById("tipo_user").selectedIndex;
		if( cu == null || cu == 0 ) {
			$("#tipo_user").addClass("error");
                        //console.log("Valida no");
			return false;
		}else{
			$("#tipo_user").removeClass("error");
                        //console.log("Valida si");
			return true;
		}
}

function validarTipoCat(){
		tu = document.getElementById("tipo_user").selectedIndex;
		ca = document.getElementById("categoria").selectedIndex;
		if( tu == 3){
			return true;
		}else{
			if( tu != 1 &&  ca == 0 ) {
				$("#categoria").addClass("error");
	                        console.log("Valida no");
				return false;
			}else{
				$("#categoria").removeClass("error");
	                        console.log("Valida si");
				return true;
			}
		}
}

function validarPatron(){
		pa = document.getElementById("patron").selectedIndex;
		if( pa == null || pa== 0 ) {
			$("#patron").addClass("error");
			return false;
		}else{
			$("#patron").removeClass("error");
			return true;
		}
}


function validarNombreAdd() {
    if ($("#nombre").val().length < 2 || $("#nombre").val() == null || /^\s+$/.test($("#nombre").val())) {
        
        $("#nombre").addClass("error");
        return false;
        
    } else {
        
        $("#nombre").removeClass("error");
        return true;
    }
}

function validaUsernameAdd() {
    if ($("#username").val().length < 2 || $("#username").val() == null || /^\s+$/.test($("#username").val())) {
        $("#username").addClass("error");
        
        return false;
    } else {
        
        $("#username").removeClass("error");
        return true;
    }
}

function validarPassAdd() {
    if ($("#pass").val().length < 2 || $("#pass").val() == null || /^\s+$/.test($("#pass").val())) {
        $("#pass").addClass("error");
        //console.log("Valida no");
        return false;
    } else {
        //console.log("Valida si");
        $("#pass").removeClass("error");
        return true;
    }
}
// --------------------------------------------------------------------------------------------------------------------
// -------------------------------------------------- AGRGAR USUARIO --------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------






// --------------------------------------------------------------------------------------------------------------------
// -------------------------------------------------- EDITAR USUARIO --------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------
function edit_user(id) {

        $.ajax({
            url: "user_controller/get_user",
            data: {id: id},
            type: "POST",
            async: 'true',
            success: function(response) {
				var dat = jQuery.parseJSON(response);
				$("#user_id").val(id);
				$("#edit_nombre").val(dat.nombre);
                $("#edit_username").val(dat.username);
                $("#edit_paterno").val(dat.apellido_paterno);
				$("#edit_materno").val(dat.apellido_materno);
                $("#edit_email").val(dat.email);
                $("#edit_empresa").val(dat.empresa);
                $("#edit_pass").val("");
                $("#confirm_pass").val("");

					 if( dat.permiso_adm_add=='1' ){
	               $("#permiso_adm_add").prop('checked',true);
					 }else{
                	$("#permiso_adm_add").prop('checked',false);
					 }

					 if( dat.permiso_adm_check=='1' ){
	               $("#permiso_adm_check").prop('checked',true);
					 }else{
                	$("#permiso_adm_check").prop('checked',false);
					 }
					 if( dat.permiso_cap_add_ind=='1' ){
	               $("#permiso_cap_add_ind").prop('checked',true);
					 }else{
                	$("#permiso_cap_add_ind").prop('checked',false);
					 }
					 if( dat.permiso_cap_add_mass=='1' ){
	               $("#permiso_cap_add_mass").prop('checked',true);
					 }else{
                	$("#permiso_cap_add_mass").prop('checked',false);
					 }
					 if( dat.permiso_cap_check=='1' ){
	               $("#permiso_cap_check").prop('checked',true);
					 }else{
                	$("#permiso_cap_check").prop('checked',false);
					 }
					 if( dat.permiso_prom_cat=='1' ){
	               $("#permiso_prom_cat").prop('checked',true);
					 }else{
                	$("#permiso_prom_cat").prop('checked',false);
					 }
					 if( dat.permiso_prom_add_ind=='1' ){
	               $("#permiso_prom_add_ind").prop('checked',true);
					 }else{
                	$("#permiso_prom_add_ind").prop('checked',false);
					 }
					 if( dat.permiso_prom_add_mass=='1' ){
	               $("#permiso_prom_add_mass").prop('checked',true);
					 }else{
                	$("#permiso_prom_add_mass").prop('checked',false);
					 }
					 if( dat.permiso_prom_check=='1' ){
	               $("#permiso_prom_check").prop('checked',true);
					 }else{
                	$("#permiso_prom_check").prop('checked',false);
					 }
					 if( dat.permiso_prom_pres=='1' ){
	               $("#permiso_prom_pres").prop('checked',true);
					 }else{
                	$("#permiso_prom_pres").prop('checked',false);
					 }
					 if( dat.permiso_prom_ace=='1' ){
	               $("#permiso_prom_ace").prop('checked',true);
					 }else{
                	$("#permiso_prom_ace").prop('checked',false);
					 }
					 if( dat.permiso_prom_esc=='1' ){
	               $("#permiso_prom_esc").prop('checked',true);
					 }else{
                	$("#permiso_prom_esc").prop('checked',false);
					 }
					 if( dat.permiso_prom_por=='1' ){
	               $("#permiso_prom_por").prop('checked',true);
					 }else{
                	$("#permiso_prom_por").prop('checked',false);
					 }
					 if( dat.permiso_prod_con=='1' ){
	               $("#permiso_prod_con").prop('checked',true);
					 }else{
                	$("#permiso_prod_con").prop('checked',false);
					 }
					 if( dat.permiso_prod_com=='1' ){
	               $("#permiso_prod_com").prop('checked',true);
					 }else{
                	$("#permiso_prod_com").prop('checked',false);
					 }
					 if( dat.permiso_prod_hist=='1' ){
	               $("#permiso_prod_hist").prop('checked',true);
					 }else{
                	$("#permiso_prod_hist").prop('checked',false);
					 }
					 if( dat.permiso_prod_nom=='1' ){
	               $("#permiso_prod_nom").prop('checked',true);
					 }else{
                	$("#permiso_prod_nom").prop('checked',false);
					 }
					 if( dat.permiso_prod_bit=='1' ){
	               $("#permiso_prod_bit").prop('checked',true);
					 }else{
                	$("#permiso_prod_bit").prop('checked',false);
					 }
					 if( dat.permiso_rec_add=='1' ){
	               $("#permiso_rec_add").prop('checked',true);
					 }else{
                	$("#permiso_rec_add").prop('checked',false);
					 }
					 if( dat.permiso_rec_check=='1' ){
	               $("#permiso_rec_check").prop('checked',true);
					 }else{
                	$("#permiso_rec_check").prop('checked',false);
					 }
					 if( dat.permiso_ana_head=='1' ){
	               $("#permiso_ana_head").prop('checked',true);
					 }else{
                	$("#permiso_ana_head").prop('checked',false);
					 }
					 if( dat.permiso_ana_rep=='1' ){
	               $("#permiso_ana_rep").prop('checked',true);
					 }else{
                	$("#permiso_ana_rep").prop('checked',false);
					 }
					 if( dat.permiso_atc_age=='1' ){
	               $("#permiso_atc_age").prop('checked',true);
					 }else{
                	$("#permiso_atc_age").prop('checked',false);
					 }
					 if( dat.permiso_atc_tick=='1' ){
	               $("#permiso_atc_tick").prop('checked',true);
					 }else{
                	$("#permiso_atc_tick").prop('checked',false);
					 }
					 if( dat.permiso_atc_for=='1' ){
	               $("#permiso_atc_for").prop('checked',true);
					 }else{
                	$("#permiso_atc_for").prop('checked',false);
					 }
					 if( dat.permiso_cpt_cur_add=='1' ){
	               $("#permiso_cpt_cur_add").prop('checked',true);
					 }else{
                	$("#permiso_cpt_cur_add").prop('checked',false);
					 }
					 if( dat.permiso_cpt_cur_check=='1' ){
	               $("#permiso_cpt_cur_check").prop('checked',true);
					 }else{
                	$("#permiso_cpt_cur_check").prop('checked',false);
					 }
					 if( dat.permiso_cpt_mat_add=='1' ){
	               $("#permiso_cpt_mat_add").prop('checked',true);
					 }else{
                	$("#permiso_cpt_mat_add").prop('checked',false);
					 }
					 if( dat.permiso_cpt_mat_check=='1' ){
	               $("#permiso_cpt_mat_check").prop('checked',true);
					 }else{
                	$("#permiso_cpt_mat_check").prop('checked',false);
					 }
					 if( dat.permiso_jer_add=='1' ){
	               $("#permiso_jer_add").prop('checked',true);
					 }else{
                	$("#permiso_jer_add").prop('checked',false);
					 }
					 if( dat.permiso_jer_check=='1' ){
	               $("#permiso_jer_check").prop('checked',true);
					 }else{
                	$("#permiso_jer_check").prop('checked',false);
					 }
					 if( dat.permiso_com_add=='1' ){
	               $("#permiso_com_add").prop('checked',true);
					 }else{
                	$("#permiso_com_add").prop('checked',false);
					 }
					 if( dat.permiso_com_check=='1' ){
	               $("#permiso_com_check").prop('checked',true);
					 }else{
                	$("#permiso_com_check").prop('checked',false);
					 }




				$("#ModalEditUser").modal('show');
            },
            error: function(x, e) {
                $.gritter.add({
				        title: 'Editar Administrador',
				        text: 'Ocurrio un error y no se pueden recuperar los datos del administrador, intentelo más tarde.',
				        class_name: 'danger'
				});
            }
        });
}


function UpdateUser() {//Agregar usuario
   //if (validarNombreAdd() & validaUsernameAdd() & validarTipoAdd() & validarTipoCat() & validarPatron()) {


		var nombre_ = $("#edit_nombre").val();
		var apellido_paterno_ = $("#edit_paterno").val();
		var apellido_materno_ = $("#edit_materno").val();
		var email_ = $("#edit_email").val();
		var empresa_ = $("#edit_empresa").val();
		var username_ = $("#edit_username").val();

		var permisos = 0;

		$("#edit_nombre").removeClass('error');
		$("#edit_paterno").removeClass('error');
		$("#edit_materno").removeClass('error');
		$("#edit_email").removeClass('error');
		$("#edit_empresa").removeClass('error');
		$("#edit_username").removeClass('error');
        $("#confirm_pass").removeClass('error');



		if( nombre_=="" || apellido_paterno_=="" || apellido_materno_=="" || email_=="" || username_=="" || empresa_==""){
			$.gritter.add({
				title: 'Agregar Usuario',
				text: 'Por favor introduzca toda la información que se solicita (*)',
				class_name: 'danger'
			});


			if( nombre_=="" ){
				$("#edit_nombre").addClass('error');
			}
			if( apellido_paterno_=="" ){
				$("#edit_paterno").addClass('error');
			}
			if( apellido_materno_=="" ){
				$("#edit_materno").addClass('error');
			}
			if( email_=="" ){
				$("#edit_email").addClass('error');
			}
			if( username_=="" ){
				$("#edit_username").addClass('error');
			}
			if( empresa_=="" ){
				$("#edit_empresa").addClass('error');
			}

		}else{

					if( $("#permiso_adm_add").prop('checked') ){
	               $("#hidden_adm_add").val(1);
	               permisos++;
					 }else{
                	$("#hidden_adm_add").val(0);
					 }

					 if( $("#permiso_adm_check").prop('checked') ){
	               $("#hidden_adm_check").val(1);
	               permisos++;
					 }else{
                	$("#hidden_adm_check").val(0);
					 }
					 if( $("#permiso_cap_add_ind").prop('checked') ){
	               $("#hidden_cap_add_ind").val(1);
	               permisos++;
					 }else{
                	$("#hidden_cap_add_ind").val(0);
					 }
					 if( $("#permiso_cap_add_mass").prop('checked') ){
	               $("#hidden_cap_add_mass").val(1);
	               permisos++;
					 }else{
                	$("#hidden_cap_add_mass").val(0);
					 }
					 if( $("#permiso_cap_check").prop('checked') ){
	               $("#hidden_cap_check").val(1);
	               permisos++;
					 }else{
                	$("#hidden_cap_check").val(0);
					 }
					 if( $("#permiso_prom_cat").prop('checked') ){
	               $("#hidden_prom_cat").val(1);
	               permisos++;
					 }else{
                	$("#hidden_prom_cat").val(0);
					 }
					 if( $("#permiso_prom_add_ind").prop('checked') ){
	               $("#hidden_prom_add_ind").val(1);
					 }else{
                	$("#hidden_prom_add_ind").val(0);
					 }
					 if( $("#permiso_prom_add_mass").prop('checked') ){
	               $("#hidden_prom_add_mass").val(1);
	               permisos++;
					 }else{
                	$("#hidden_prom_add_mass").val(0);
					 }
					 if( $("#permiso_prom_check").prop('checked') ){
	               $("#hidden_prom_check").val(1);
	               permisos++;
					 }else{
                	$("#hidden_prom_check").val(0);
					 }
					 if( $("#permiso_prom_pres").prop('checked') ){
	               $("#hidden_prom_pres").val(1);
	               permisos++;
					 }else{
                	$("#hidden_prom_pres").val(0);
					 }
					 if( $("#permiso_prom_ace").prop('checked') ){
	               $("#hidden_prom_ace").val(1);
	               permisos++;
					 }else{
                	$("#hidden_prom_ace").val(0);
					 }
					 if( $("#permiso_prom_esc").prop('checked') ){
	               $("#hidden_prom_esc").val(1);
	               permisos++;
					 }else{
                	$("#hidden_prom_esc").val(0);
					 }
					 if( $("#permiso_prom_por").prop('checked') ){
	               $("#hidden_prom_por").val(1);
	               permisos++;
					 }else{
                	$("#hidden_prom_por").val(0);
					 }
					 if( $("#permiso_prod_con").prop('checked') ){
	               $("#hidden_prod_con").val(1);
	               permisos++;
					 }else{
                	$("#hidden_prod_con").val(0);
					 }
					 if( $("#permiso_prod_com").prop('checked') ){
	               $("#hidden_prod_com").val(1);
	               permisos++;
					 }else{
                	$("#hidden_prod_com").val(0);
					 }
					 if( $("#permiso_prod_hist").prop('checked') ){
	               $("#hidden_prod_hist").val(1);
	               permisos++;
					 }else{
                	$("#hidden_prod_hist").val(0);
					 }
					 if( $("#permiso_prod_nom").prop('checked') ){
	               $("#hidden_prod_nom").val(1);
	               permisos++;
					 }else{
                	$("#hidden_prod_nom").val(0);
					 }
					 if( $("#permiso_prod_bit").prop('checked') ){
	               $("#hidden_prod_bit").val(1);
	               permisos++;
					 }else{
                	$("#hidden_prod_bit").val(0);
					 }
					 if( $("#permiso_rec_add").prop('checked') ){
	               $("#hidden_rec_add").val(1);
	               permisos++;
					 }else{
                	$("#hidden_rec_add").val(0);
					 }
					 if( $("#permiso_rec_check").prop('checked') ){
	               $("#hidden_rec_check").val(1);
	               permisos++;
					 }else{
                	$("#hidden_rec_check").val(0);
					 }
					 if( $("#permiso_ana_head").prop('checked') ){
	               $("#hidden_ana_head").val(1);
	               permisos++;
					 }else{
                	$("#hidden_ana_head").val(0);
					 }
					 if( $("#permiso_ana_rep").prop('checked') ){
	               $("#hidden_ana_rep").val(1);
	               permisos++;
					 }else{
                	$("#hidden_ana_rep").val(0);
					 }
					 if( $("#permiso_atc_age").prop('checked') ){
	               $("#hidden_atc_age").val(1);
	               permisos++;
					 }else{
                	$("#hidden_atc_age").val(0);
					 }
					 if( $("#permiso_atc_tick").prop('checked') ){
	               $("#hidden_atc_tick").val(1);
	               permisos++;
					 }else{
                	$("#hidden_atc_tick").val(0);
					 }
					 if( $("#permiso_atc_for").prop('checked') ){
	               $("#hidden_atc_for").val(1);
	               permisos++;
					 }else{
                	$("#hidden_atc_for").val(0);
					 }
					 if( $("#permiso_cpt_cur_add").prop('checked') ){
	               $("#hidden_cpt_cur_add").val(1);
	               permisos++;
					 }else{
                	$("#hidden_cpt_cur_add").val(0);
					 }
					 if( $("#permiso_cpt_cur_check").prop('checked') ){
	               $("#hidden_cpt_cur_check").val(1);
	               permisos++;
					 }else{
                	$("#hidden_cpt_cur_check").val(0);
					 }
					 if( $("#permiso_cpt_mat_add").prop('checked') ){
	               $("#hidden_cpt_mat_add").val(1);
	               permisos++;
					 }else{
                	$("#hidden_cpt_mat_add").val(0);
					 }
					 if( $("#permiso_cpt_mat_check").prop('checked') ){
	               $("#hidden_cpt_mat_check").val(1);
	               permisos++;
					 }else{
                	$("#hidden_cpt_mat_check").val(0);
					 }
					 if( $("#permiso_jer_add").prop('checked') ){
	               $("#hidden_jer_add").val(1);
	               permisos++;
					 }else{
                	$("#hidden_jer_add").val(0);
					 }
					 if( $("#permiso_jer_check").prop('checked') ){
	               $("#hidden_jer_check").val(1);
	               permisos++;
					 }else{
                	$("#hidden_jer_check").val(0);
					 }
					 if( $("#permiso_com_add").prop('checked') ){
	               $("#hidden_com_add").val(1);
	               permisos++;
					 }else{
                	$("#hidden_com_add").val(0);
					 }
					 if( $("#permiso_com_check").prop('checked') ){
	               $("#hidden_com_check").val(1);
	               permisos++;
					 }else{
                	$("#hidden_com_check").val(0);
					 }

				if(permisos==0){
					$.gritter.add({
						title: 'Nuevo Administrador',
						text: 'Debe seleccionar al menos un permiso.',
						class_name: 'danger'
					});
				}else{
					nuevo_pass = $("#edit_pass").val();
					conf_pass = $("#confirm_pass").val();
					pass_pass = true;

					if( nuevo_pass!="" ){
						//Si se escribe un password checar confirmacion
						if( nuevo_pass!=conf_pass ){
							$.gritter.add({
								title: 'Editar Administrador',
								text: 'Los passwords no coinciden, por favor verifique.',
								class_name: 'danger'
							});
							$("#confirm_pass").addClass('error');
							pass_pass=false;
						}
					}

					if( pass_pass ){
						$.ajax({
							url: "user_controller/update_user",
							data: $("#FormUpdateUser").serialize(),
							type: "POST",
							async: 'true',
							success: function(response) {
								if (response == 1) {
									$("#ModalEditUser").modal('hide');
								    $.gritter.add({
										title: 'Editar Administrador',
										text: 'Se modifico correctamente el usuario.',
										class_name: 'success'
									  });
								    setTimeout ("window.location.reload();", 2000);
								} else {
								    $("#ModalEditUser").modal('hide');
								    $.gritter.add({
										title: 'Editar Administrador',
										text: 'No se realizó ningún cambio en los datos.',
										class_name: 'success'
									  });
								    setTimeout ("window.location.reload();", 2000);
								}
							},
							error: function(x, e) {
								$.gritter.add({
										title: 'Editar Usuario',
										text: 'Ocurrio un error y no se pudo modificar el usuario: ' + e.messager,
										class_name: 'danger'
								});
							}
						});
					}
				}

					

		}






    /*} else {
        
        return false;
    }*/
}


function status_user(id, status){
	$.ajax({
            url: "user_controller/update_status",
            data: {id: id, status: status},
            type: "POST",
            async: 'true',
            beforeSend: function() {
                //console.log('Procesando, espere por favor...');
            },
            success: function(response) {
                if (response == 1) {
                    $.gritter.add({
				        title: 'Cambiar Status de Usuario',
				        text: 'Se modifico correctamente el usuario.',
				        class_name: 'success'
				      });
                    setTimeout ("window.location.reload();", 2300);//setTimeout("loadSection('user_controller','agasajo');", 2000);
                } else {
                    $.gritter.add({
				        title: 'Cambiar Status de Usuario',
				        text: 'Ocurrio un error y no se pudo modificar el usuario, intentelo nuevamente.',
				        class_name: 'danger'
				      });
                }
            },
            error: function(x, e) {
                $.gritter.add({
				        title: 'Cambiar Status de Usuario',
				        text: 'Ocurrio un error y no se pudo modificar el usuario, intentelo nuevamente.' + e.messager,
				        class_name: 'danger'
				});
            }
        });
}


function delete_user(id){
	$.ajax({
            url: "user_controller/eliminar_user",
            data: {id: id},
            type: "POST",
            async: 'true',
            success: function(response) {
                if (response == 1) {
                	$("#DeleteUser").modal('hide');
                    $.gritter.add({
				        title: 'Eliminar Administrador',
				        text: 'Se eliminó correctamente el administrador.',
				        class_name: 'success'
				      });
                    setTimeout ("window.location.reload();", 2000);
                } else {
                    $.gritter.add({
				        title: 'Eliminar Administrador',
				        text: 'Ocurrio un error y no se pudo eliminar el administrador, intentelo más tarde.',
				        class_name: 'danger'
				      });
                }
            },
            error: function(x, e) {
                $.gritter.add({
				        title: 'Eliminar Administrador',
				        text: 'Ocurrio un error y no se pudo eliminar el administrador: ' + e.messager,
				        class_name: 'danger'
				});
            }
        });
}
// --------------------------------------------------------------------------------------------------------------------
// -------------------------------------------------- EDITAR USUARIO --------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------
